// Region Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getRegionsAction = () => (dispatch) => {
    dispatch({
        api: app.getRegionListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_REGION_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveRegionAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveRegionAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null
            }
        })
        dispatch(getRegionsAction())
        toast.success("Region saved successfully!");
    }).catch(err => {
        toast.error("Error saving region!");
    })
}

export const updateRegionAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateRegionAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null
            }
        })
        dispatch(getRegionsAction())
        toast.success("Region updated successfully!");
    }).catch(err => {
        toast.error("Error updating region!");
    })
}

export const deleteRegionAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteRegionAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null,

            }
        })
        dispatch(getRegionsAction())
        toast.success("Region deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting region!");
    })
}

export const patchRegionAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchRegionAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                regions: null,
            }
        })
        dispatch(getRegionsAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Region!");
    })
}
export const getRegionSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getRegionSearchAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_REGION_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })

}
// Region Action End