// Bank Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getBanksAction = () => (dispatch) => {
    dispatch({
        api: app.getBankListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BANK_SUCCESS,
            types.REQUEST_ERROR
        ],
    })
}

export const saveBankAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveBankAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                banks: null
            }
        })
        dispatch(getBanksAction())
        toast.success("Bank saved successfully!");
    }).catch(err => {
        toast.error("Error saving bank!");
    })
}

export const updateBankAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateBankAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                banks: null
            }
        })
        dispatch(getBanksAction())
        toast.success("Bank updated successfully!");
    }).catch(err => {
        toast.error("Error updating bank!");
    })
}

export const deleteBankAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteBankAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                banks: null,

            }
        })
        dispatch(getBanksAction())
        toast.success("Bank deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting bank!");
    })
}

export const patchBankAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchBankAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                banks: null,
            }
        })
        dispatch(getBanksAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Bank!");
    })
}
export const getBanksSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getBankSearchAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_BANK_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}
// Bank Action End