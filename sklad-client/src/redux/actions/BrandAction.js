// Brand Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getBrandsAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getBrandListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BRAND_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}

export const saveBrandAction = (payload) => (dispatch) => {
    console.log(payload)
    dispatch({
        api : app.saveBrandAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                brands: null
            }
        })
        dispatch(getBrandsAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Brand!");
    })
}

export const updateBrandAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateBrandAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                brands: null
            }
        })
        dispatch(getBrandsAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Brand!");
    })
}

export const deleteBrandAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteBrandAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                brands: null,
            }
        })
        dispatch(getBrandsAction())
        toast.success("Brand deleted successfully");
    }).catch(err => {
        toast.error("Error deleting Brand!");
    })
}

export const patchBrandAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchBrandAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload

    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                brands: null,
            }
        })
        dispatch(getBrandsAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Brand!");
    })
}


export const searchBrandsAction = (payload) => (dispatch) => {
    dispatch({
        api: app.searchBrandAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_BRAND_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}
// Brand Action End