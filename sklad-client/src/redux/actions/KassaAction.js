// Kassa Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getKassasAction = () => (dispatch) => {
    dispatch({
        api: app.getKassaListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_KASSA_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveKassaAction = (payload) => (dispatch) => {
    console.log(payload)
    dispatch({
        api : app.saveKassaAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassas: null
            }
        })
        dispatch(getKassasAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Kassa!");
    })
}

export const updateKassaAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateKassaAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassas: null
            }
        })
        dispatch(getKassasAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Kassa!");
    })
}

export const deleteKassaAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteKassaAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassas: null,
            }
        })
        dispatch(getKassasAction())
        toast.success("Kassa deleted successfully");
    }).catch(err => {
        toast.error("Error deleting Product Category!");
    })
}

export const patchKassaAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchKassaAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassas: null,
            }
        })
        dispatch(getKassasAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Kassa!");
    })
}
// Kassa Action End