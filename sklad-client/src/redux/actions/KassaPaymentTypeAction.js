// Kassa Payment Type Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getKassaPaymentTypesAction = () => (dispatch) => {
    dispatch({
        api: app.getKassaPaymentTypeListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_KASSA_PAYMENT_TYPE_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveKassaPaymentTypeAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveKassaPaymentTypeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassaPaymentTypes: null
            }
        })
        dispatch(getKassaPaymentTypesAction())
        toast.success("Kassa Payment Type saved successfully!");
    }).catch(err => {
        toast.error("Error saving Kassa Payment Type!");
    })
}

export const updateKassaPaymentTypeAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateKassaPaymentTypeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassaPaymentTypes: null
            }
        })
        dispatch(getKassaPaymentTypesAction())
        toast.success("Kassa Payment Type updated successfully!");
    }).catch(err => {
        toast.error("Error updating Kassa Payment Type!");
    })
}

export const deleteKassaPaymentTypeAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteKassaPaymentTypeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassaPaymentTypes: null,

            }
        })
        dispatch(getKassaPaymentTypesAction())
        toast.success("Kassa Payment Type deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Kassa Payment Type!");
    })
}

export const patchKassaPaymentTypeAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchKassaPaymentTypeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                kassaPaymentTypes: null,
            }
        })
        dispatch(getKassaPaymentTypesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Kassa Payment Type!");
    })
}
export const getKassaPaymentTypeSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getKassaPaymentTypeSearchAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_KASSA_PAYMENT_TYPE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })

}
// Kassa Payment Type Action End