// Measure Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getMeasuresAction = () => (dispatch) => {
    dispatch({
        api: app.getMeasureListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_MEASURE_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveMeasureAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveMeasureAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null
            }
        })
        dispatch(getMeasuresAction())
        toast.success("Measure saved successfully!");
    }).catch(err => {
        toast.error("Error saving measure!");
    })
}

export const updateMeasureAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateMeasureAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null
            }
        })
        dispatch(getMeasuresAction())
        toast.success("Measure updated successfully!");
    }).catch(err => {
        toast.error("Error updating measure!");
    })
}

export const deleteMeasureAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteMeasureAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null,

            }
        })
        dispatch(getMeasuresAction())
        toast.success("Measure deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting measure!");
    })
}

export const patchMeasureAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchMeasureAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                measures: null,
            }
        })
        dispatch(getMeasuresAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Measure!");
    })
}
export const getMeasureSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getMeasureSearchAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_MEASURE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })

}
// Measure Action End