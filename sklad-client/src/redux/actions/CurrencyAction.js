// Currency Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getCurrenciesAction = () => (dispatch) => {
    dispatch({
        api: app.getCurrencyListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CURRENCY_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveCurrencyAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveCurrencyAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                currencies: null
            }
        })
        dispatch(getCurrenciesAction())
        toast.success("Currency saved successfully!");
    }).catch(err => {
        toast.error("Error saving Currency!");
    })
}

export const updateCurrencyAction = (payload) => (dispatch) => {
    console.log(payload)
    dispatch({
        api : app.updateCurrencyAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                currencies: null
            }
        })
        dispatch(getCurrenciesAction())
        toast.success("Currency updated successfully!");
    }).catch(err => {
        toast.error("Error updating Currency!");
    })
}

export const deleteCurrencyAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteCurrencyAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                currencies: null,

            }
        })
        dispatch(getCurrenciesAction())
        toast.success("Currency deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Currency!");
    })
}
// Currency Action End