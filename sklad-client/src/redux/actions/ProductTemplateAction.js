// Product Template Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getProductTemplatesAction = (payload) => (dispatch) => {
    dispatch({
        api : app.getProductTemplateListAppApi,
        types : [
            types.REQUEST_START,
            types.REQUEST_GET_PRODUCT_TEMPLATE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}

export const saveProductTemplateAction = (payload) => (dispatch) => {
    console.log(payload)
    dispatch({
        api : app.saveProductTemplateAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productTemplates: null
            }
        })
        dispatch(getProductTemplatesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Product Template!");
    })
}

export const updateProductTemplateAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateProductTemplateAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productTemplates: null
            }
        })
        dispatch(getProductTemplatesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Product Template!");
    })
}

export const deleteProductTemplateAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteProductTemplateAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productTemplates: null,
            }
        })
        dispatch(getProductTemplatesAction())
        toast.success("Product Template deleted successfully");
    }).catch(err => {
        toast.error("Error deleting Product Template!");
    })
}

export const patchProductTemplateAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchProductTemplateAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productTemplates: null,
            }
        })
        dispatch(getProductTemplatesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Product Template!");
    })
}

export const getByWarehouseAction = (payload) => (dispatch) => {
    dispatch({
        api : app.getByWarehouseAppApi,
        types : [
            types.REQUEST_START,
            types.REQUEST_GET_BY_WAREHOUSE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}

export const getByNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.getAllProductTemplateByNameAppApi,
        types : [
            types.REQUEST_START,
            types.REQUEST_GET_PRODUCT_TEMPLATE_BY_NAME_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}

export const getAllAction = (payload) => (dispatch) => {
    dispatch({
        api : app.getAllProductTemplateAppApi,
        types : [
            types.REQUEST_START,
            types.REQUEST_GET_PRODUCT_TEMPLATE_ALL_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}

export const searchProductTemplateAction = (payload) => (dispatch) => {
    dispatch({
        api: app.searchProductTemplateAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_PRODUCT_TEMPLATE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}
