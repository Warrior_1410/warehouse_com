// Currency Name Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getCurrencyNamesAction = () => (dispatch) => {
    dispatch({
        api: app.getCurrencyNameListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CURRENCY_NAME_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveCurrencyNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveCurrencyNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                warehouses: null
            }
        })
        dispatch(getCurrencyNamesAction())
        toast.success("Currency Name saved successfully!");
    }).catch(err => {
        toast.error("Error saving currency name!");
    })
}

export const updateCurrencyNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateCurrencyNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                currencyNames: null
            }
        })
        dispatch(getCurrencyNamesAction())
        toast.success("Currency Name updated successfully!");
    }).catch(err => {
        toast.error("Error updating currency name!");
    })
}

export const deleteCurrencyNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteCurrencyNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                currencyNames: null,

            }
        })
        dispatch(getCurrencyNamesAction())
        toast.success("Currency Name deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting currency name!");
    })
}

export const patchCurrencyNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchCurrencyNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                currencyNames: null,
            }
        })
        dispatch(getCurrencyNamesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Currency Name!");
    })
}

export const getCurrencyNameSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getCurrencyNameSearchAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_CURRENCY_NAME_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })

}
// Currency Name Action End