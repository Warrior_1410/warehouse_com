import * as types from "../actionTypes/AppActionTypes";
import * as app from "../../api/AppApi";
import {toast} from "react-toastify";


// Product  Start
export const saveIncomeAction = (payload) => (dispatch) => {
    dispatch({
        api: app.saveIncomeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Income!");
    })
}

export const getIncomeAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getIncomeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_INCOME_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getIncomeByDateAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getIncomeByDateAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_INCOME_BY_DATE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}

export const getIncomeGetAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getIncomeOneAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_INCOME_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}


export const searchIncomeAction = (payload) => (dispatch) => {
    dispatch({
        api: app.searchIncomeAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_INCOME_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}

export const saveShipmentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.saveShipmentAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Shipment!");
    })
}
export const getShipmentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getShipmentAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SHIPMENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const searchShipmentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.searchShipmentAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_SHIPMENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}

export const getShipmentGetAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getShipmentOneAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SHIPMENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}

export const saveTransferAction = (payload) => (dispatch) => {
    dispatch({
        api: app.saveTransferAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Transfer!");
    })
}
export const getTransferAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getTransferAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_TRANSFER_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const searchTransferAction = (payload) => (dispatch) => {
    dispatch({
        api: app.searchTransferAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_TRANSFER_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getTransferGetAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getTransferOneAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_TRANSFER_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
// Product End

// Price Start
export const getPriceAction = () => (dispatch) => {
    dispatch({
        api: app.getAllPrice,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_PRICE_SUCCESS,
            types.REQUEST_ERROR
        ],

    })
}
export const getCountAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getCountAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_COUNT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
// Price End


// User Start
export const getAllUserAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getUserAllAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_USER_ALL_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getAgentAction = () => (dispatch) => {
    dispatch({
        api: app.getUserByRole,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AGENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: "agent"
    })
}
export const getSupplierAction = () => (dispatch) => {
    dispatch({
        api: app.getUserByRole,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SUPPLIER_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: "supplier"
    })
}
export const getClientAction = () => (dispatch) => {
    dispatch({
        api: app.getUserByRole,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CLIENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: "client"
    })
}
export const getUserAction = () => (dispatch) => {
    dispatch({
        api: app.getUserByRole,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_USER_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: "user"
    })
}
// User End


// Register Start
export const registerAction = (payload) => async (dispatch) => {
    dispatch({
        api: app.registerAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                allUser: null
            }
        })
        dispatch(getAllUserAction())
        toast.success("Royhatdan o'tdingiz");
    }).catch(err => {
        toast.error("Error!");
    })
}
// Register End


// Attachment Action Start
export const uploadFileAction = (payload) => async (dispatch) => {
    if (!payload || !(payload.type.substring(0, payload.type.indexOf("/")) === "image")) {
        toast.error("File must be img")
        return "";
    }
    let obj = new FormData();
    obj.append("file", payload)

    dispatch({
        api: app.uploadFileAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_ATTACHMENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: obj

    })
}


export const deleteFileAction = (payload) => async (dispatch) => {
    console.log(payload)
    dispatch({
        api: app.deleteFileAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_ATTACHMENT,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        console.log(res);
    }).catch(err => {
        toast.error("Error!");
    })
}
// Attachment Action End





