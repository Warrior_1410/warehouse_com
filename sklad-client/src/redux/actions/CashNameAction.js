// Cash Name Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getCashNamesAction = () => (dispatch) => {
    dispatch({
        api: app.getCashNameListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CASH_NAME_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
}

export const saveCashNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveCashNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                cashNames: null
            }
        })
        dispatch(getCashNamesAction())
        toast.success("Cash Name saved successfully!");
    }).catch(err => {
        toast.error("Error saving CashName!");
    })
}

export const updateCashNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateCashNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                cashNames: null
            }
        })
        dispatch(getCashNamesAction())
        toast.success("CashName updated successfully!");
    }).catch(err => {
        toast.error("Error updating CashName!");
    })
}

export const deleteCashNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteCashNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                cashNames: null,

            }
        })
        dispatch(getCashNamesAction())
        toast.success("CashName deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting CashName!");
    })
}

export const patchCashNameAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchCashNameAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                cashNames: null,
            }
        })
        dispatch(getCashNamesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Cash Name!");
    })
}
export const getCashNameSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getCashNameSearchAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_CASH_NAME_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })

}
// Cash Name Action End