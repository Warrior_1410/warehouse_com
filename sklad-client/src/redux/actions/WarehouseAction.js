// Warehouse Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getWarehousesAction = () => (dispatch) => {
    dispatch({
        api: app.getWarehouseListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_WAREHOUSE_SUCCESS,
            types.REQUEST_ERROR
        ],
    })
}

export const saveWarehouseAction = (payload) => (dispatch) => {
    dispatch({
        api : app.saveWarehouseAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                warehouses: null
            }
        })
        dispatch(getWarehousesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving warehouse!");
    })
}

export const updateWarehouseAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateWarehouseAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                warehouses: null
            }
        })
        dispatch(getWarehousesAction())
        toast.success("Warehouse updated successfully!");
    }).catch(err => {
        toast.error("Error updating warehouse!");
    })
}

export const deleteWarehouseAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteWarehouseAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                warehouses: null,

            }
        })
        dispatch(getWarehousesAction())
        toast.success("Warehouse deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting warehouse!");
    })
}

export const patchWarehouseAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchWarehouseAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                warehouses: null,
            }
        })
        dispatch(getWarehousesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Warehouse!");
    })
}

export const getWarehousesSearchAction = (payload) => (dispatch) => {
        dispatch({
            api: app.getWarehouseSearchAppApi,
            types: [
                types.REQUEST_START,
                types.REQUEST_SEARCH_WAREHOUSE_SUCCESS,
                types.REQUEST_ERROR
            ],
            data : payload
        })
}
// Warehouse Action End