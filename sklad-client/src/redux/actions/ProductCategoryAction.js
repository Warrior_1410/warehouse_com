// Product Category Action Start
import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";

export const getProductCategoriesAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getProductCategoryListAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_PRODUCT_CATEGORY_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}

export const saveProductCategoryAction = (payload) => (dispatch) => {
    console.log(payload)
    dispatch({
        api : app.saveProductCategoryAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productCategories: null,
                attachmentId :  ''
            }
        })
        dispatch(getProductCategoriesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error saving Product Category!");
    })
}

export const updateProductCategoryAction = (payload) => (dispatch) => {
    dispatch({
        api : app.updateProductCategoryAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productCategories: null
            }
        })
        dispatch(getProductCategoriesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Product Category!");
    })
}

export const deleteProductCategoryAction = (payload) => (dispatch) => {
    dispatch({
        api : app.deleteProductCategoryAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productCategories: null,
            }
        })
        dispatch(getProductCategoriesAction())
        toast.success("Product deleted successfully");
    }).catch(err => {
        toast.error("Error deleting Product Category!");
    })
}

export const patchProductCategoryAction = (payload) => (dispatch) => {
    dispatch({
        api : app.patchProductCategoryAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload,
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                productCategories: null,
            }
        })
        dispatch(getProductCategoriesAction())
        toast.success(res.payload.message);
    }).catch(err => {
        toast.error("Error updating Product Category!");
    })
}

export const getProductCategoriesSearchAction = (payload) => (dispatch) => {
    dispatch({
        api: app.searchProductCategoryAppApi,
        types: [
            types.REQUEST_START,
            types.REQUEST_SEARCH_PRODUCT_CATEGORY_SUCCESS,
            types.REQUEST_ERROR
        ],
        data : payload
    })
}
// Product Category Action End