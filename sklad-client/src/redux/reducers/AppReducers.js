import * as types from "../actionTypes/AppActionTypes";
import {createReducer} from "../../utils/StoreUtils";
import {toast} from "react-toastify";

const initState = {
    attachmentUrl : "http://localhost/api/attachment/",
    attachmentId: null,
    page : 1,
    size : 20,
    filterDate : '',
    totalElements : 0,
    totalPages : 0,

    clients: [],
    agents: [],
    suppliers: [],
    users: [],

    openFullView : false,
    loading: false,

    addModal: false,
    viewModal: false,
    editModal: false,
    deleteModal: false,
    statusModal: false,
    currentItem: '',
    currentObject: {},

    banks: [],
    warehouses: [],
    currencyNames: [],
    measures: [],

    regions: [],
    regionsSearch : [],

    cashNames: [],
    kassaPaymentTypes: [],
    currencies: [],

    productCategories: [],
    productCategoriesSearch: [],

    kassas: [],

    brands: [],
    brandsSearch: [],

    productTemplates: [],
    productTemplatesSearch: [],

    income: [],
    incomeOne: {},
    shipment: [],
    shipmentOne: {},
    transfer: [],
    transferOne: {},
    price: [],
    count: [],
    byWarehouse: [],
    allUser:[]
};

const reducers = {
    [types.REQUEST_ATTACHMENT](state) {
        state.attachmentId = null;
        // setTimeout(()=>(state.loading = true),1000)
    },
    [types.REQUEST_ATTACHMENT_SUCCESS](state, payload) {
        state.attachmentId = payload.payload
    },
    [types.REQUEST_START](state) {
        state.loading = true;
        // setTimeout(()=>(state.loading = true),1000)
    },
    [types.REQUEST_ERROR](state) {
        state.loading = false;
        // setTimeout(()=>(state.loading = false),1000)
    },
    [types.REQUEST_GET_BANK_SUCCESS](state, payload) {
        state.banks = null
        state.banks = payload.payload.object.object
    },
    [types.REQUEST_SEARCH_BANK_SUCCESS](state, payload) {
        state.banks = null
        state.banks = payload.payload
    },
    [types.REQUEST_GET_WAREHOUSE_SUCCESS](state, payload) {
        state.warehouses = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.warehouses = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_SEARCH_WAREHOUSE_SUCCESS](state, payload) {
        state.warehouses = null
        state.warehouses = payload.payload
    },
    [types.REQUEST_GET_CURRENCY_NAME_SUCCESS](state, payload) {
        state.currencyNames = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.currencyNames = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_SEARCH_CURRENCY_NAME_SUCCESS](state, payload) {
        state.currencyNames = null
        state.currencyNames = payload.payload
    },
    [types.REQUEST_GET_MEASURE_SUCCESS](state, payload) {
        state.measures = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.measures = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_SEARCH_MEASURE_SUCCESS](state, payload) {
        state.measures = null
        state.measures = payload.payload
    },
    [types.REQUEST_GET_CASH_NAME_SUCCESS](state, payload) {
        state.cashNames = null;
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.cashNames = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_SEARCH_CASH_NAME_SUCCESS](state, payload) {
        state.cashNames = null
        state.cashNames = payload.payload
    },
    [types.REQUEST_GET_KASSA_PAYMENT_TYPE_SUCCESS](state, payload) {
        state.kassaPaymentTypes = null;
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.kassaPaymentTypes = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_SEARCH_KASSA_PAYMENT_TYPE_SUCCESS](state, payload) {
        state.kassaPaymentTypes = null
        state.kassaPaymentTypes = payload.payload
    },
    [types.REQUEST_GET_REGION_SUCCESS](state, payload) {
        console.log(payload)
        let array = []
        state.regionsSearch = null
        state.regions = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.regions = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            payload.payload.object.object.forEach(item=>
                array.push({value:item.id, label:item.name})
            )
            state.regionsSearch  = array
        }
    },
    [types.REQUEST_SEARCH_REGION_SUCCESS](state, payload) {
        state.regions = null
        state.regions = payload.payload
    },
    [types.REQUEST_GET_CURRENCY_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.currencies = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_GET_PRODUCT_CATEGORY_SUCCESS](state, payload) {
        let array = []
        state.productCategories = null
        state.productCategoriesSearch = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.productCategories = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            payload.payload.object.object.forEach(item=>
                array.push({value:item.id, label:item.name})
            )
            state.productCategoriesSearch  = array

            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },
    [types.REQUEST_SEARCH_PRODUCT_CATEGORY_SUCCESS](state, payload) {
        state.productCategories = null
        state.productCategories = payload.payload
    },

    [types.REQUEST_KASSA_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.kassas = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_GET_BRAND_SUCCESS](state, payload) {
        console.log(payload)
        let array = []
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.brands = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            payload.payload.object.object.forEach(item=>
                array.push({value:item.id, label:item.name})
            )
            state.brandsSearch  = array

            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },
    [types.REQUEST_SEARCH_BRAND_SUCCESS](state, payload) {
        state.brands = null
        state.brands = payload.payload
    },


    [types.REQUEST_GET_PRODUCT_TEMPLATE_SUCCESS](state, payload) {
        let array = []
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.productTemplates = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            payload.payload.object.object.forEach(item=>
                array.push({value:item.id, label:item.name})
            )
            state.productTemplatesSearch  = array

            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },
    [types.REQUEST_SEARCH_PRODUCT_TEMPLATE_SUCCESS](state, payload) {
        state.productTemplates = null
        state.productTemplates = payload.payload
    },




    [types.REQUEST_GET_AGENT_SUCCESS](state, payload) {
        state.agents  = null
        let array = []
        payload.payload.forEach(item=>
            array.push({value:item.id, label:item.fullName})
        )
        state.agents  = array
    },
    [types.REQUEST_GET_CLIENT_SUCCESS](state, payload) {
        state.clients  = null
        let array = []
        payload.payload.forEach(item=>
            array.push({value:item.id, label:item.fullName})
        )
        state.clients  = array
    },
    [types.REQUEST_GET_SUPPLIER_SUCCESS](state, payload) {
        state.suppliers  = null
        let array = []
        payload.payload.forEach(item=>
            array.push({value:item.id, label:item.fullName})
        )
        state.suppliers  = array
    },
    [types.REQUEST_GET_USER_SUCCESS](state, payload) {
        state.users  = null
        let array = []
        payload.payload.forEach(item=>
            array.push({value:item.id, label:item.fullName})
        )
        state.users  = array
    },


    [types.REQUEST_GET_PRICE_SUCCESS](state, payload) {
        state.price = payload.payload
    },



    [types.REQUEST_GET_INCOME_SUCCESS](state, payload) {
        state.income = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.income = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }

    },
    [types.REQUEST_SEARCH_INCOME_SUCCESS](state,payload){
        state.income = null
        state.income = payload.payload
    },
    [types.REQUEST_INCOME_SUCCESS](state, payload) {
        state.incomeOne = payload.payload
    },

    [types.REQUEST_GET_SHIPMENT_SUCCESS](state, payload) {
        state.shipment = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.shipment = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },
    [types.REQUEST_SEARCH_SHIPMENT_SUCCESS](state,payload){
        state.shipment = null
        state.shipment = payload.payload
    },
    [types.REQUEST_SHIPMENT_SUCCESS](state, payload) {
        state.shipmentOne = payload.payload
    },

    [types.REQUEST_GET_TRANSFER_SUCCESS](state, payload) {
        state.transfer = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.transfer = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },

    [types.REQUEST_SEARCH_TRANSFER_SUCCESS](state,payload){
        state.transfer = null
        state.transfer = payload.payload
    },
    [types.REQUEST_TRANSFER_SUCCESS](state, payload) {
        state.transferOne = payload.payload
    },
    [types.REQUEST_GET_COUNT_SUCCESS](state, payload) {
        state.count = payload.payload
    },


    [types.REQUEST_GET_BY_WAREHOUSE_SUCCESS](state, payload) {
        state.byWarehouse = null
        state.byWarehouse = payload.payload
    },
    [types.REQUEST_GET_PRODUCT_TEMPLATE_BY_NAME_SUCCESS](state, payload) {
        state.byWarehouse = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.byWarehouse = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },
    [types.REQUEST_GET_PRODUCT_TEMPLATE_ALL_SUCCESS](state, payload) {
        state.byWarehouse = null
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.byWarehouse = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }

    },
    [types.REQUEST_GET_USER_ALL_SUCCESS](state,payload){
        if (payload && payload.payload && payload.payload.object && payload.payload.object.object) {
            state.allUser = payload.payload.object.object.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.object.page
            state.size = payload.payload.object.size
            state.totalElements = payload.payload.object.totalElements
            state.totalPages = payload.payload.object.totalPages
        }
    },

    [types.REQUEST_SUCCESS](state, payload) {

    },

    updateState(state, {payload}) {
        return {
            ...state,
            ...payload,
        };
    },
};
export default createReducer(initState, reducers);
