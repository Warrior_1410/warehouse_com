import React, {Component} from 'react';
import {Col, Row} from "reactstrap";
import {Filter, Search} from "../Icons";

class Header extends Component {
    render() {
        return (
            <div className={"p-5"}>
                <Row>
                    <Col md={6}>
                        <h3>{this.props.title}</h3>
                    </Col>
                    <Col md={6} className={"text-right"}>
                        <Row>
                            <Col>
                                <div className={"searchInputGroup"}>
                                    <input type="text" className="mainInput" placeholder={"Qidirish..."} onChange={this.props.search}/>
                                    <span className={"searchIcon"}>
                                        <Search />
                                    </span>
                                    <div className={"calendarIcon"}>
                                        <span onClick={this.props.calendar}>
                                            <Filter />
                                        </span>
                                        <div className={"filterModal"}>
                                            OK
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col>
                                <div className={"btnAdd"} onClick={this.props.addModal}>
                                    Qo'shish
                                </div>
                            </Col>
                        </Row>


                    </Col>
                </Row>
            </div>
        );
    }
}

Header.propTypes = {};

export default Header;