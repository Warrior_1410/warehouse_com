import React, {Component} from 'react';
import "./Content.css";
import Warehouse from "../../pages/adminPages/Warehouse";
import Bank from "../../pages/adminPages/Bank";
import CurrencyName from "../../pages/adminPages/CurrencyName";
import Measure from "../../pages/adminPages/Measure";
import Region from "../../pages/adminPages/Region";
import CashName from "../../pages/adminPages/CashName";
import KassaPaymentType from "../../pages/adminPages/KassaPaymentType";
import NotFound from "../../pages/NotFound";
import Currency from "../../pages/adminPages/Currency";
import ProductCategory from "../../pages/adminPages/ProductCategory";
import IncomeProduct from "../../pages/adminPages/Product/Income/IncomeProduct";
import Brand from "../../pages/adminPages/Brand";
import ProductTemplate from "../../pages/adminPages/ProductTemplate";
import Kassa from "../../pages/adminPages/Kassa";
import ShipmentProduct from "../../pages/adminPages/Product/Shipment/ShipmentProduct";
import TransferProduct from "../../pages/adminPages/Product/Transfer/TransferProduct";
import IncomeProductList from "../../pages/adminPages/Product/Income/IncomeProductList";
import IncomeGet from "../../pages/adminPages/Product/Income/IncomeGet";
import ShipmentGet from "../../pages/adminPages/Product/Shipment/ShipmentGet";
import Product from "../../pages/adminPages/Product/Product";
import TransferProductList from "../../pages/adminPages/Product/Transfer/TransferProductList";
import TransferGet from "../../pages/adminPages/Product/Transfer/TransferGet";
import AdminPage from "../../pages/adminPages/AdminPage";
import User from "../../pages/adminPages/User/User";

class Content extends Component {
    render() {
        const choosePage = () => {
            switch (this.props.page) {
                case "warehouse":
                    return <Warehouse/>
                case "bank":
                    return <Bank/>
                case "currencyName":
                    return <CurrencyName/>
                case "measure":
                    return <Measure/>
                case "region":
                    return <Region/>
                case "cashName":
                    return <CashName/>
                case "kassaPaymentType":
                    return <KassaPaymentType/>
                case "currency":
                    return <Currency/>
                case "productCategory":
                    return <ProductCategory/>
                case "product":
                    return <Product/>
                case "income":
                    return this.props.subPage ? <IncomeGet id={this.props.subPage}/> : <IncomeProduct/>;
                case "shipment":
                    return this.props.subPage ? <ShipmentGet id={this.props.subPage}/> : <ShipmentProduct/>;
                case "transfer":
                    return this.props.subPage ? <TransferGet id={this.props.subPage}/> : <TransferProduct/>;
                case "brand":
                    return <Brand/>
                case "productTemplate":
                    return <ProductTemplate/>
                case "kassa":
                    return <Kassa/>
                case "users":
                    return <User/>
                case "home":
                    return <AdminPage/>
                default:
                    return <NotFound/>
            }
        }
        return (
            <div className={"content"}>
                {choosePage()}
            </div>
        );
    }
}

Content.propTypes = {};

export default Content;
