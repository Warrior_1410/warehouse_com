import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import {connect} from "react-redux";

class AdminMenuLayout extends Component {
    render() {
        const {currentUser, isAdmin, isSuperadmin, showModal} = this.props;

        return (
            <div className={'admin-layout-menu'}>
                {isAdmin || isSuperadmin ?
                    <ul>
                        <Link to="/dashboard">
                            <div className={this.props.pathname === "dashboard" ? "active-menu-button" : "menu-button"}>
                                Dashboard
                            </div>
                        </Link>
                    </ul> : ""}
            </div>
        );
    }
}

AdminMenuLayout.propTypes = {};

export default connect(
    ({
         app: {showModal},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        showModal,
        isAdmin,
        isSuperAdmin,
        currentUser
    })
)(AdminMenuLayout);
