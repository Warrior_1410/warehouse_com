import React from 'react';
import './AdminLayoutSecondaryButtons.css';


const AdminLayoutSecondaryButtons = (props) => {
    return (
        <button className={"secondaryButtonAdminLayout"}>
            {props.imgUrl}
        </button>
    );
}

export default AdminLayoutSecondaryButtons;