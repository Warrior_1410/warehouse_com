import React, {Component, useState} from 'react';
import 'simplebar/dist/simplebar.min.css';
import './AdminLayout.scss';
import {AdminLayoutButtons} from "./AdminLayoutButtons/AdminLayoutButtons";
import WorkflowMini from "./Logo/WorkflowMini";
import Content from "../Content/Content";
import {Link} from "react-router-dom";
import {AdminLayoutMiniButtons} from "./AdminLayoutMiniButtons/AdminLayoutMiniButtons";
import {Container, Row, Col} from "reactstrap";
import {connect} from "react-redux";
import {Notification} from "../Icons";

class AdminLayout extends React.Component {
    state = {
        active: true
    }

    render() {
        const {currentUser} = this.props
        return (
            <div className={"mainContent"}>
                <div className={"admin-ws"}>
                    <div className={"container mx-auto"}>
                            <Link to={"/admin/home"} className={"logotip"}>
                                {/*<WorkflowMini/>*/}
                                <h2>
                                    <font className={"text-primary"}>G</font>
                                    <font className={"text-danger"}>o</font>
                                    <font className={"text-warning"}>o</font>
                                    <font className={"text-primary"}>g</font>
                                    <font className={"text-success"}>l</font>
                                    <font className={"text-danger"}>e</font>
                                </h2>
                            </Link>
                            <AdminLayoutButtons
                                active={this.props.page === "product"}
                                layoutActive={!this.state.active}
                                btnText={"Mahsulot"}
                                class={this.state.active}
                                link={"/admin/product"}
                            />

                            <AdminLayoutButtons
                                active={this.props.page === "income"}
                                layoutActive={!this.state.active}
                                btnText={"Kirim"}
                                class={this.state.active}
                                link={"/admin/income"}
                            />
                            <AdminLayoutButtons
                                active={this.props.page === "shipment"}
                                layoutActive={!this.state.active}
                                btnText={"Chiqim"}
                                class={this.state.active}
                                link={"/admin/shipment"}
                            />
                            <AdminLayoutButtons
                                active={this.props.page === "transfer"}
                                layoutActive={!this.state.active}
                                btnText={"Ko'chirish"}
                                class={this.state.active}
                                link={"/admin/transfer"}
                            />

                            <AdminLayoutButtons
                                active={this.props.page === "users"}
                                layoutActive={!this.state.active}
                                btnText={"User"}
                                class={this.state.active}
                                link={"/admin/users"}
                            />
                            <button className={"btnAdminLayoutButton descr"}>
                                Qo'shimcha

                                <div className={"dropdownSimple"}>
                                    <Container>
                                        <Row>
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "productCategory"}
                                                layoutActive={!this.state.active}
                                                btnText={"Mahsulot turi"}
                                                class={this.state.active}
                                                link={"/admin/productCategory"}
                                            />
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "brand"}
                                                layoutActive={!this.state.active}
                                                btnText={"Brand"}
                                                class={this.state.active}
                                                link={"/admin/brand"}
                                            />
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "productTemplate"}
                                                layoutActive={!this.state.active}
                                                btnText={"Shablon"}
                                                class={this.state.active}
                                                link={"/admin/productTemplate"}
                                            />
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "warehouse"}
                                                layoutActive={!this.state.active}
                                                btnText={"Ombor"}
                                                class={this.state.active}
                                                link={"/admin/warehouse"}
                                            />
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "bank"}
                                                layoutActive={!this.state.active}
                                                btnText={"Bank"}
                                                class={this.state.active}
                                                link={"/admin/bank"}
                                            />
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "currencyName"}
                                                layoutActive={!this.state.active}
                                                btnText={"Valyuta nomi"}
                                                class={this.state.active}
                                                link={"/admin/currencyName"}
                                            />

                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "measure"}
                                                layoutActive={!this.state.active}
                                                btnText={"O'lchov"}
                                                class={this.state.active}
                                                link={"/admin/measure"}
                                            />
                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "region"}
                                                layoutActive={!this.state.active}
                                                btnText={"Region"}
                                                class={this.state.active}
                                                link={"/admin/region"}
                                            />

                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "cashName"}
                                                layoutActive={!this.state.active}
                                                btnText={"Tolov turi"}
                                                class={this.state.active}
                                                link={"/admin/cashName"}
                                            />

                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "kassaPaymentType"}
                                                layoutActive={!this.state.active}
                                                btnText={"Toluv qabul turi"}
                                                class={this.state.active}
                                                link={"/admin/kassaPaymentType"}
                                            />

                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "currency"}
                                                layoutActive={!this.state.active}
                                                btnText={"Valyuta"}
                                                class={this.state.active}
                                                link={"/admin/currency"}
                                            />

                                            <AdminLayoutMiniButtons
                                                active={this.props.page === "kassa"}
                                                layoutActive={!this.state.active}
                                                btnText={"Kassa"}
                                                class={this.state.active}
                                                link={"/admin/kassa"}
                                            />
                                        </Row>
                                    </Container>
                                </div>
                            </button>

                        <span className={"notificationButton"}>
                            <Notification />
                        </span>
                        <div className={"userPanel"}>
                            <img src={""} alt={""}/>
                        </div>

                    </div>
                </div>

                <Content page={this.props.page} subPage={this.props.subPage}/>
            </div>
        );
    }
}

AdminLayout.propTypes = {};


export default connect(
    ({
         app: {},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        currentUser
    })
)(AdminLayout);