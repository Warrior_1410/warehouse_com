import React from 'react';
import './AdminLayoutButtons.css';
import {Link} from "react-router-dom";


export const AdminLayoutButtons = (props) => {
    return (
        <div>
            <span id={props.tooltip}>
                <Link to={props.link}>
                    <button className={'btnAdminLayoutButton' + (props.active ? " activeButton" : "")} id={props.btnText}>
                        {/*{props.imgUrl}*/}
                        {/*<br/>*/}
                        {props.btnText}
                    </button>
                </Link>
            </span>
        </div>
    );
}

