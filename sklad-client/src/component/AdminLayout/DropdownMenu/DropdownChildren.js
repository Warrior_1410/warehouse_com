import React from 'react';

const DropdownChildren = (props) => {
    return (
            <button className={props.dropChildrenStyle}>
                {props.dropdownText}
            </button>
    );
}

export default DropdownChildren;