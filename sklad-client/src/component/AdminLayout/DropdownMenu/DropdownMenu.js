import React, {useState} from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import DropdownButton from "./DropdownButton";
import './DropdownMenu.css';
import DropdownChildren from "./DropdownChildren";

const DropdownMenu = (props) => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <DropdownButton clicked={toggle} style={{ marginBottom: '1rem' }} activeDropDown={"dropdownStyle"}/>
            <Collapse isOpen={isOpen}>
                <Card className={"cardStyle"}>
                    <CardBody>
                        <DropdownChildren dropChildrenStyle={"dropChildren"} dropdownText={"Bla bla bla"}/>
                        <DropdownChildren dropChildrenStyle={"dropChildren"} dropdownText={"Bla bla bla"}/>
                        <DropdownChildren dropChildrenStyle={"dropChildren"} dropdownText={"Bla bla bla"}/>
                        <DropdownChildren dropChildrenStyle={"dropChildren"} dropdownText={"Bla bla bla"}/>
                    </CardBody>
                </Card>
            </Collapse>
        </div>
    );
}

export default DropdownMenu;