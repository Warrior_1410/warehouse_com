import React from 'react';

const DropdownButton = (props) => {
    return(
        <button onClick={props.clicked} className={props.activeDropDown}>
            Click me
        </button>
    );
}

export default DropdownButton;