import React from 'react';
import './AdminLayoutMiniButtons.css';
import {Link} from "react-router-dom";

export const AdminLayoutMiniButtons = (props) => {
    return (
        <div className={"col-2"}>
            <span id={props.tooltip}>
                <Link to={props.link}>
                    <button className={props.active ? "btnAdminLayoutMiniButtonActive" : "btnAdminLayoutMiniButton"} id={props.btnText}>
                        {props.btnText}
                    </button>
                </Link>
            </span>
        </div>
    );
}

