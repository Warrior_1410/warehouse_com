import React, {Component} from 'react';
import PropTypes from 'prop-types';

class OpenButton extends Component {
    render() {
        return (
            <svg width="64" height="64" viewBox="0 0 64 64" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <g filter="url(#filter0_d)">
                    <path d="M2 0H32C48.5685 0 62 13.4315 62 30C62 46.5685 48.5685 60 32 60H2V0Z"
                          fill="white"/>
                </g>
                <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M28.131 27.0833L32.3929 21.9975L30.7143 20L23.5714 28.5L30.7143 37L32.4048 35.0025L28.131 29.9167H45V27.0833H28.131ZM20 37H22.3809V20H20V37Z"
                      fill="#6F6F76"/>
                <defs>
                    <filter id="filter0_d" x="0" y="0" width="64" height="64"
                            filterUnits="userSpaceOnUse"
                            color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix"
                                       values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                        <feOffset dy="2"/>
                        <feGaussianBlur stdDeviation="1"/>
                        <feColorMatrix type="matrix"
                                       values="0 0 0 0 0.933333 0 0 0 0 0.933333 0 0 0 0 0.933333 0 0 0 0.2 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow"
                                 result="shape"/>
                    </filter>
                </defs>
            </svg>
        );
    }
}

OpenButton.propTypes = {};

export default OpenButton;
