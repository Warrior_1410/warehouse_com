import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter} from "reactstrap";

class StatusModal extends Component {
    render() {
        const {isOpen,close,update} = this.props

        return (
            <div>
                <Modal isOpen={isOpen}>
                    <ModalBody className={"mx-auto"}>
                        <h4>Rostdan ham o'zgatirmoqchimisiz?</h4>
                    </ModalBody>
                    <ModalFooter>
                        <Button outline onClick={close}>Bekor qilish</Button>
                        <Button color="primary" onClick={update}>O'zgartirish</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

StatusModal.propTypes = {};

export default StatusModal;