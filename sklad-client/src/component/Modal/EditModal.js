import React, {Component} from 'react';
import rectangle from "../../resources/images/rectangle.png";
import {X} from "../Icons";
import {AvForm} from "availity-reactstrap-validation";
import {Button, Modal, ModalFooter} from "reactstrap";

class EditModal extends Component {
    render() {
        const {isOpen,toggle,children,close,onValidSubmit} = this.props
        return (
            <div>
                <Modal isOpen={isOpen} toggle={toggle} className={"EditModal m-0 p-0"}>
                    <img src={rectangle} alt="" className={"rectangle"} onClick={close}/>
                    <span className={"closeBtn"} onClick={close}>
                        <X />
                    </span>
                    <AvForm className={"text-left col-10"} onValidSubmit={onValidSubmit}>
                        <h4>Tahrirlash</h4>
                        <hr />
                        {children}
                        <ModalFooter className={"mt-3"}>
                            <Button outline onClick={close} type={"button"}>Bekor qilish</Button>
                            <Button color="primary">Saqlash</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>
            </div>
        );
    }
}

EditModal.propTypes = {};

export default EditModal;