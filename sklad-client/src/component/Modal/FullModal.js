import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Modal, ModalFooter} from "reactstrap";
import rectangle from "../../resources/images/rectangle.png";
import {X} from "../Icons";
import {AvForm} from "availity-reactstrap-validation";

class FullModal extends Component {
    render() {
        const {title,isOpen,toggle,children,close,onValidSubmit} = this.props
        return (
            <div>
                <Modal isOpen={isOpen} toggle={toggle} className={"FullModal"} size={"lg"}>
                    <img src={rectangle} alt="" className={"rectangle"} onClick={close}/>
                    <span className={"closeBtn"} onClick={close}>
                        <X />
                    </span>
                    <AvForm className={"col-10"} onValidSubmit={onValidSubmit}>
                        <h4>{title}</h4>
                        <hr />
                        {children}
                        <ModalFooter className={"mt-3"}>
                            <Button outline onClick={close} type={"button"}>Bekor qilish</Button>
                            <Button color="primary">Saqlash</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>
            </div>
        );
    }
}

FullModal.propTypes = {};

export default FullModal;