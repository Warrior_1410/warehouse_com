import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter,Row,Col} from "reactstrap";
import {X} from "../Icons";
import editImg from "../../resources/images/editImg.png";

class DeleteModal extends Component {
    render() {
        const {isOpen,close,del} = this.props
        return (
            <div>
                <Modal isOpen={isOpen} >
                    <ModalBody className={"mx-auto"}>
                        <Row>
                            <Col className={"text-right"}>
                                <div onClick={close}>
                                    <X />
                                </div>
                            </Col>
                        </Row>
                        <img src={editImg} className={"editImg"} alt=""/>
                        <h4 className={"title"}>Rostdan ham o'chirmoqchimisiz?</h4>
                    </ModalBody>
                    <ModalFooter>
                        <Button outline color="secondary" onClick={close}>Bekor
                            qilish</Button>
                        <Button color="danger" onClick={del}>O'chirish</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

DeleteModal.propTypes = {};

export default DeleteModal;