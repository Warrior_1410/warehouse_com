import React from "react";
import {Route, Switch} from "react-router-dom";
import store from "../redux";
import {Provider} from "react-redux";
import PublicRoute from "../utils/PublicRoute";
import PrivateRoute from "../utils/PrivateRoute";
import HomePage from "../pages/HomePage";
import AdminHome from "../pages/adminPages/AdminPages";
import NotFound from "../pages/NotFound";
import Login from "../pages/Login/Login";
import Register from "../pages/Register/Register";
import Card from "../pages/Card";

const App = () => {
  return (
      <Provider store={store}>
        <Switch>
          <PublicRoute exact path="/card" component={Card}/>
          <PublicRoute exact path="/" component={Login}/>
          <PublicRoute exact path="/login" component={Login}/>
          <PrivateRoute exact path="/admin/:page" component={AdminHome}/>
          <PrivateRoute exact path="/admin/:page/:subPage" component={AdminHome}/>
          <Route component={NotFound}/>
        </Switch>
      </Provider>
  );
}

export default App;