import HttpClient from "../utils/HttpClient";
import {api} from './api'


export const getTest = () => {
    return HttpClient.doGet(api.test);
}

export const saveTestAppApi = (data) => {
    return HttpClient.doPost(api.test, data);
}

export const updateTestAppApi = (data) => {
    return HttpClient.doPut(api.test + "/" + data.id, data);
}

export const deleteTestAppApi = (data) => {
    return HttpClient.doDelete(api.test + "/" + data.id)
}

// Product Start
export const saveIncomeAppApi = (data) => {
    return HttpClient.doPost(api.income, data);
}
export const getIncomeAppApi = (data) => {
    return HttpClient.doGet(api.income + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""));
}
export const getIncomeByDateAppApi = (data) => {
    return HttpClient.doGet(api.income + "/byDate" + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size + "&date=" + data.filterDate : ""));
}

export const searchIncomeAppApi = (data) => {
    return HttpClient.doGet(api.income + "/search/" + data);
}
export const getIncomeOneAppApi = (data) => {
    return HttpClient.doGet(api.income + "/" + data);
}
export const saveShipmentAppApi = (data) => {
    return HttpClient.doPost(api.shipment, data);
}
export const getShipmentAppApi = (data) => {
    return HttpClient.doGet(api.shipment + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""));
}
export const searchShipmentAppApi = (data) => {
    return HttpClient.doGet(api.shipment + "/search/" + data);
}
export const getShipmentOneAppApi = (data) => {
    return HttpClient.doGet(api.shipment + "/" + data);
}
export const saveTransferAppApi = (data) => {
    return HttpClient.doPost(api.transfer, data);
}
export const getTransferAppApi = (data) => {
    return HttpClient.doGet(api.transfer + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""));
}
export const searchTransferAppApi = (data) => {
    return HttpClient.doGet(api.transfer + "/search/" + data);
}
export const getTransferOneAppApi = (data) => {
    return HttpClient.doGet(api.transfer + "/" + data);
}
// Product End

// Price Start
export const getAllPrice = () => {
    return HttpClient.doGet(api.product + "/allPrice")
}
export const getCountAppApi = (data) => {
    return HttpClient.doGet(api.product + "/allCount/" + data);
}
export const getByWarehouseAppApi = (data) => {
    return HttpClient.doGet(api.productTemplate + "/byWarehouse/" + data);
}

export const getAllProductTemplateByNameAppApi = (data) => {
    return HttpClient.doGet(api.productTemplate + "/searchBy/" + data.name + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""))
}
export const getAllProductTemplateAppApi = (data) => {
    return HttpClient.doGet(api.productTemplate + "/all" + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""))
}
// Price End

// Register Start
export const registerAppApi = (data) => {
    return HttpClient.doPost(api.registerUser, data)
}
// Register End

// Attachment CRUD Start
export const uploadFileAppApi = (data) => {
    return HttpClient.doPost(api.attachment + "/upload", data);
};

export const getFileAppApi = (data) => {
    return HttpClient.doGet(api.attachment + "/" + data);
};

export const deleteFileAppApi = (data) => {
    console.log(data)
    return HttpClient.doDelete(api.attachment + "/" + data.name + "/" + data.id);
}

// Attachment CRUD End

// User Start
export const getUserByRole = (data) => {
    return HttpClient.doGet(api.byRole + data);
}
export const getUserAllAppApi = (data) => {
    return HttpClient.doGet(api.registerUser + "/all" + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""));
}
// User End

/* Bank CRUD Start */
export const getBankListAppApi = (data) => {
    return HttpClient.doGet(api.bank)
}
export const saveBankAppApi = (data) => {
    return HttpClient.doPost(api.bank, data);
}
export const updateBankAppApi = (data) => {
    return HttpClient.doPut(api.bank + "/" + data.id, data);
}
export const deleteBankAppApi = (data) => {
    return HttpClient.doDelete(api.bank + "/" + data);
}
export const patchBankAppApi = (data) => {
    return HttpClient.doPatch(api.bank + "/" + data);
}
export const getBankSearchAppApi = (data) => {
    return HttpClient.doGet(api.bank + "/search/" + data);
}
/* Bank CRUD End */

/* Warehouse CRUD Start */
export const getWarehouseListAppApi = (data) => {
    return HttpClient.doGet(api.warehouse);
}
export const saveWarehouseAppApi = (data) => {
    return HttpClient.doPost(api.warehouse, data);
}
export const updateWarehouseAppApi = (data) => {
    return HttpClient.doPut(api.warehouse + "/" + data.id, data);
}
export const deleteWarehouseAppApi = (data) => {
    return HttpClient.doDelete(api.warehouse + "/" + data);
}
export const patchWarehouseAppApi = (data) => {
    return HttpClient.doPatch(api.warehouse + "/" + data);
}
export const getWarehouseSearchAppApi = (data) => {
    return HttpClient.doGet(api.warehouse + "/search/" + data);
}
/* Warehouse CRUD End */

/* Currency Name CRUD Start */
export const getCurrencyNameListAppApi = () => {
    return HttpClient.doGet(api.currencyName)
}
export const saveCurrencyNameAppApi = (data) => {
    return HttpClient.doPost(api.currencyName, data);
}
export const updateCurrencyNameAppApi = (data) => {
    return HttpClient.doPut(api.currencyName + "/" + data.id, data);
}
export const deleteCurrencyNameAppApi = (data) => {
    return HttpClient.doDelete(api.currencyName + "/" + data);
}
export const patchCurrencyNameAppApi = (data) => {
    return HttpClient.doPatch(api.currencyName + "/" + data);
}
export const getCurrencyNameSearchAppApi = (data) => {
    return HttpClient.doGet(api.currencyName + "/search/" + data);
}
/* Currency Name CRUD End */


/* Measure CRUD Start */
export const getMeasureListAppApi = () => {
    return HttpClient.doGet(api.measure)
}
export const saveMeasureAppApi = (data) => {
    return HttpClient.doPost(api.measure, data);
}
export const updateMeasureAppApi = (data) => {
    return HttpClient.doPut(api.measure + "/" + data.id, data);
}
export const deleteMeasureAppApi = (data) => {
    return HttpClient.doDelete(api.measure + "/" + data);
}
export const patchMeasureAppApi = (data) => {
    return HttpClient.doPatch(api.measure + "/" + data);
}
export const getMeasureSearchAppApi = (data) => {
    return HttpClient.doGet(api.measure + "/search/" + data);
}
/* Measure CRUD End */


/* Region CRUD Start */
export const getRegionListAppApi = () => {
    return HttpClient.doGet(api.region)
}
export const saveRegionAppApi = (data) => {
    return HttpClient.doPost(api.region, data);
}
export const updateRegionAppApi = (data) => {
    return HttpClient.doPut(api.region + "/" + data.id, data);
}
export const deleteRegionAppApi = (data) => {
    return HttpClient.doDelete(api.region + "/" + data);
}
export const patchRegionAppApi = (data) => {
    return HttpClient.doPatch(api.region + "/" + data);
}
export const getRegionSearchAppApi = (data) => {
    return HttpClient.doGet(api.region + "/search/" + data);
}
/* Region CRUD End */


/* Region CRUD Start */
export const getCashNameListAppApi = () => {
    return HttpClient.doGet(api.cashName)
}
export const saveCashNameAppApi = (data) => {
    return HttpClient.doPost(api.cashName, data);
}
export const updateCashNameAppApi = (data) => {
    return HttpClient.doPut(api.cashName + "/" + data.id, data);
}
export const deleteCashNameAppApi = (data) => {
    return HttpClient.doDelete(api.cashName + "/" + data);
}
export const patchCashNameAppApi = (data) => {
    return HttpClient.doPatch(api.cashName + "/" + data);
}
export const getCashNameSearchAppApi = (data) => {
    return HttpClient.doGet(api.cashName + "/search/" + data);
}
/* Region CRUD End */


/* Region CRUD Start */
export const getKassaPaymentTypeListAppApi = () => {
    return HttpClient.doGet(api.kassaPaymentType)
}
export const saveKassaPaymentTypeAppApi = (data) => {
    return HttpClient.doPost(api.kassaPaymentType, data);
}
export const updateKassaPaymentTypeAppApi = (data) => {
    return HttpClient.doPut(api.kassaPaymentType + "/" + data.id, data);
}
export const deleteKassaPaymentTypeAppApi = (data) => {
    return HttpClient.doDelete(api.kassaPaymentType + "/" + data);
}
export const patchKassaPaymentTypeAppApi = (data) => {
    return HttpClient.doPatch(api.kassaPaymentType + "/" + data);
}
export const getKassaPaymentTypeSearchAppApi = (data) => {
    return HttpClient.doGet(api.kassaPaymentType + "/search/" + data);
}
/* Region CRUD End */

/* Currency CRUD Start */
export const getCurrencyListAppApi = () => {
    return HttpClient.doGet(api.currency)
}
export const saveCurrencyAppApi = (data) => {
    return HttpClient.doPost(api.currency, data);
}
export const updateCurrencyAppApi = (data) => {
    return HttpClient.doPut(api.currency + "/" + data.id, data);
}
export const deleteCurrencyAppApi = (data) => {
    return HttpClient.doDelete(api.currency + "/" + data);
}
export const patchCurrencyAppApi = (data) => {
    return HttpClient.doPatch(api.currency + "/" + data);
}
/* Currency CRUD End */

/* Product Category CRUD Start */
export const getProductCategoryListAppApi = (data) => {
    return HttpClient.doGet(api.productCategory + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""))
}
export const saveProductCategoryAppApi = (data) => {
    return HttpClient.doPost(api.productCategory, data);
}
export const updateProductCategoryAppApi = (data) => {
    return HttpClient.doPut(api.productCategory + "/" + data.id, data);
}
export const deleteProductCategoryAppApi = (data) => {
    return HttpClient.doDelete(api.productCategory + "/" + data);
}
export const patchProductCategoryAppApi = (data) => {
    return HttpClient.doPatch(api.productCategory + "/" + data.id, data);
}
export const searchProductCategoryAppApi = (data) => {
    return HttpClient.doGet(api.productCategory + "/search/" + data);
}
/* Product Category CRUD End */


/* Kassa CRUD Start */
export const getKassaListAppApi = () => {
    return HttpClient.doGet(api.kassa)
}
export const saveKassaAppApi = (data) => {
    return HttpClient.doPost(api.kassa, data);
}
export const updateKassaAppApi = (data) => {
    return HttpClient.doPut(api.kassa + "/" + data.id, data);
}
export const deleteKassaAppApi = (data) => {
    return HttpClient.doDelete(api.kassa + "/" + data);
}
export const patchKassaAppApi = (data) => {
    return HttpClient.doPatch(api.kassa + "/" + data);
}
/* Kassa CRUD End */


/* Brand CRUD Start */
export const getBrandListAppApi = (data) => {
    return HttpClient.doGet(api.brand + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""))
}
export const saveBrandAppApi = (data) => {
    return HttpClient.doPost(api.brand, data);
}
export const updateBrandAppApi = (data) => {
    return HttpClient.doPut(api.brand + "/" + data.id, data);
}
export const deleteBrandAppApi = (data) => {
    return HttpClient.doDelete(api.brand + "/" + data);
}
export const patchBrandAppApi = (data) => {
    return HttpClient.doPatch(api.brand + "/" + data.id, data);
}
export const searchBrandAppApi = (data) => {
    return HttpClient.doGet(api.brand + "/search/" + data);
}
/* Brand CRUD End */


/* Product Template Start */
export const getProductTemplateListAppApi = (data) => {
    return HttpClient.doGet(api.productTemplate + (data && data.page != null && data.size ? "?page=" + data.page
        + "&size=" + data.size : ""))
}
export const saveProductTemplateAppApi = (data) => {
    return HttpClient.doPost(api.productTemplate, data);
}
export const updateProductTemplateAppApi = (data) => {
    return HttpClient.doPut(api.productTemplate + "/" + data.id, data);
}
export const deleteProductTemplateAppApi = (data) => {
    return HttpClient.doDelete(api.productTemplate + "/" + data);
}
export const patchProductTemplateAppApi = (data) => {
    return HttpClient.doPatch(api.productTemplate + "/" + data.id, data);
}
export const searchProductTemplateAppApi = (data) => {
    return HttpClient.doGet(api.productTemplate + "/search/" + data);
}
/* Product Template End */