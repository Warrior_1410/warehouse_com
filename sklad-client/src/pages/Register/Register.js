import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './../Login/Login.css'
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {FormGroup, Label, Button, Input, ModalFooter, Modal} from "reactstrap";
import {connect} from "react-redux";
import {
    registerAction,
} from "../../redux/actions/AppActions";
import {toast} from "react-toastify";
import {getRegionsAction} from "../../redux/actions/RegionAction";

class Register extends Component {
    componentDidMount() {
        this.props.dispatch(getRegionsAction())
    }
    

    render() {
        const {regions} = this.props

        const handleRegister = async (e, v) => {
            console.log(v)
            if (v.regionId === "0")
            {
                toast.error("Region tanlash majburiy")
            }
            else {
                this.props.dispatch(registerAction(v))
            }
        };

        return (
            <div className="container">
                <div className={"row"}>
                    <div className={"col-6 offset-3"}>
                        <h1 className={"text-center"}>Register</h1>
                        <AvForm method={"POST"} onValidSubmit={handleRegister}>
                            <AvField name={"fullName"} label={"Full Name"} required />
                            <AvField name={"address"} label={"Address"} required />
                            <AvField name={"phoneNumber"} label={"Phone Number"} required />
                            <AvField name={"phoneNumber2"} label={"Phone Number 2"} required />
                            <AvField name={"password"} label={"Password"} required />
                            <AvField type={"select"} name={"roleName"} label={"Role Name"} required >
                                <option value={"CLIENT"}>CLIENT</option>
                                <option value={"USER"}>USER</option>
                                <option value={"SUPPLIER"}>SUPPLIER</option>
                                <option value={"AGENT"}>AGENT</option>
                            </AvField>
                            <AvField type={"select"} name={"regionId"} label={"Region"} required >
                                <option value={"0"} selected>Region tanlang</option>
                                {regions ? regions.map((item,i)=>
                                    <option value={item.id}>{item.name}</option>
                                )
                                    :
                                    <option value={"0"}>Malumot topilmadi</option>
                                }
                            </AvField>
                            <hr />
                            <Button color={"primary"} type={"submit"}>Register</Button>
                        </AvForm>
                    </div>
                </div>




            </div>
        );
    }
}

export default connect(
    ({
         app: {showModal,regions},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        showModal,
        isAdmin,
        isSuperAdmin,
        currentUser,
        regions
    })
)(Register);
