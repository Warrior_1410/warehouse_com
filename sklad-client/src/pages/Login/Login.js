import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import warehouse from "../../resources/images/warehous.png";
import './Login.css'
import * as authActions from "../../redux/actions/AuthActions";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {connect} from "react-redux";
import {EyeButton, EyeOffButton} from "../../component/Icons";

class Login extends Component {
    state = {
        variable: false
    }

    handleSignIn = async (e, v) => {
        this.props.dispatch(authActions.login({v, history: this.props.history}))
    };

    render() {
        const showHide = () => {
            this.setState({variable: !this.state.variable})
            let pwd = document.getElementById("pwd")

            this.state.variable ? pwd.type = "password" : pwd.type = "text"
        }

        return (
            <div className={"registration"}>
                <div className={"w-50 fl-l"}>
                    <img src={warehouse} alt={""} className={"h-100 w-100"}/>
                </div>
                <div className={"w-50 fl-l"}>
                    <h1 className={"text-center mt-15"}>Kirish</h1>
                    <AvForm className={"mx-auto col-8"} onValidSubmit={this.handleSignIn}>
                        <AvField name="phoneNumber" placeholder="+998 90 1234567" id="usr" label={"Telefon raqam"} className={"reg-input"}/>
                        <div className={"reg-pass"}>
                        <AvField type="password" name="password" placeholder="Parol" id={"pwd"} label={"Parol"} className={"reg-input"}/>

                        <button type={"button"} className={"btn reg-eye"} id={"reg-eye"} onClick={showHide}>
                            {this.state.variable ? <EyeButton /> : <EyeOffButton />}
                        </button>
                        </div>

                        <button className={"btn w-100 btn-primary reg-input"}>Kirish</button>
                    </AvForm>
                </div>
            </div>
        );
    }
}

export default connect(
    ({
         app: {showModal},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        showModal,
        isAdmin,
        isSuperAdmin,
        currentUser
    })
)(Login);
