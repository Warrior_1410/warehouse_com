import React, {Component} from 'react';
import {Container,Row,Col} from "reactstrap";
import Workflow from "../../component/AdminLayout/Logo/Workflow";

class AdminPage extends Component {
    render() {
        return (
            <div className={"container content pt-3"}>
                    <Row>
                        <Col className={"text-center"}>
                            <div className={"mx-auto"}><Workflow/></div>
                        </Col>
                    </Row>
                    <hr />
                    <Row>
                        <Col className={"text-center"}>
                            <h4>Workflow ombor boshqaruv tizimiga xush kelibsiz</h4>
                        </Col>
                    </Row>
            </div>
        );
    }
}

AdminPage.propTypes = {};

export default AdminPage;