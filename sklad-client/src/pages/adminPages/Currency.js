import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deleteCurrencyAction,
    getCurrenciesAction,
    saveCurrencyAction,
    updateCurrencyAction
} from "../../redux/actions/CurrencyAction";
import {Table} from "reactstrap";
import "./AdminPages.css";
import {AvField} from "availity-reactstrap-validation";
import {toast} from "react-toastify";
import {getCurrencyNamesAction} from "../../redux/actions/CurrencyNameAction";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import DeleteModal from "../../component/Modal/DeleteModal";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";

class Currency extends Component {

    componentDidMount() {
        this.props.dispatch(getCurrenciesAction())
        this.props.dispatch(getCurrencyNamesAction())

        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentId: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {
        const {
            addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            page, size, totalElements, totalPages, currencies, currencyNames
        } = this.props;

        const showHideAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }
        const showHideEditModal = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject: item,
                    editModal: !editModal
                }
            })
        }
        const showHideDeleteModal = (id) => {
            if (id) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: id,
                        deleteModal: !deleteModal
                    }
                })
            } else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const saveItem = (e, v) => {
            if (v.currencyNameId === 0) {
                toast.warning("Currency Name Tanlash Majburiy")
            } else {
                this.props.dispatch(saveCurrencyAction(v))

            }
        }
        const deleteItem = () => {
            this.props.dispatch(deleteCurrencyAction(currentItem))
        }

        const editItem = (e, v) => {
            if (v.currencyNameId === 0) {
                toast.warning("Currency Name Tanlash Majburiy")
            } else {
                this.props.dispatch(updateCurrencyAction(v))

            }
        }

        return (
            <div>
                <Header title={"Valyuta ro'yhati"} addModal={showHideAddModal}/>

                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Miqdori</th>
                        <th>Birlik</th>
                        <th>Valyuta nomi</th>
                        <th className={'text-center'}>Amal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {currencies ? currencies.map((item, i) =>
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{item.amount}</td>
                            <td>{item.unity}</td>
                            <td>{item.currencyName ? item.currencyName.name : ''}</td>
                            <td className={'text-center'}>
                                <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                    <EditButton/>
                                </div>
                                <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                    <DeleteButton/>
                                </div>
                            </td>
                        </tr>
                    ) : <tr>
                        <td colSpan={5}>Malumot topilmadi</td>
                    </tr>}
                    </tbody>
                </Table>

                <AddModal isOpen={addModal} toggle={showHideAddModal}
                          close={showHideAddModal} onValidSubmit={saveItem}>
                    <AvField type={"hidden"} name={"id"}/>
                    <span>Miqdori</span>
                    <AvField type="number" min="0" max='1000000000' name="amount" required/>
                    <span>Birlik</span>
                    <AvField type="number" min="0" max='1000000000' name="unity"/>
                    <span>Valyuta nomi</span>
                    <AvField type="select" name="currencyNameId" helpMessage="Tanlash majburiy" required>
                        <option selected value={"0"}>Valyuta nomini tanlang</option>
                        {currencyNames ? currencyNames.map((item, i) =>
                                <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>
                </AddModal>

                <EditModal isOpen={editModal} toggle={showHideEditModal}
                           close={showHideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"} defaultValue={currentObject ? currentObject.id : ''}/>

                    <span>Miqdori</span>
                    <AvField type="number" min="0" max='1000000000' name="amount"
                             defaultValue={currentObject ? currentObject.amount : ''} required/>

                    <span>Birlik</span>
                    <AvField type="number" min="0" max='1000000000' name="unity"
                             defaultValue={currentObject ? currentObject.unity : ''}/>

                    <span>Valyuta nomi</span>
                    <AvField type="select" name="currencyNameId" helpMessage="Tanlash majburiy" required>
                        <option selected
                                value={currentObject && currentObject.currencyName ? currentObject.currencyName.id : ''}>{currentObject && currentObject.currencyName ? currentObject.currencyName.name : 'Valyuta nomini tanlang'}</option>
                        {currencyNames ? currencyNames.map((item, i) =>
                                currentObject && currentObject.currencyName && currentObject.currencyName.id === item.id ? '' :
                                    <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>
            </div>
        );
    }
}


export default connect(
    ({
         app: {
             addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
             page, size, totalElements, totalPages, currencyNames, currencies
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        page, size, totalElements, totalPages,
        currencies,
        currencyNames,
    })
)(Currency);