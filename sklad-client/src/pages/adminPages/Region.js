import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deleteRegionAction,
    getRegionsAction, getRegionSearchAction, patchRegionAction,
    saveRegionAction,
    updateRegionAction
} from "../../redux/actions/RegionAction";
import {Table, Input} from "reactstrap";
import {AvField} from "availity-reactstrap-validation";
import Select from "react-select";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";

class Region extends Component {

    componentDidMount() {
        this.props.dispatch(getRegionsAction())
        console.clear()
    }

    state = {
        region: ''
    }


    render() {
        const {
            addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            page, size, totalElements, totalPages, regions, regionsSearch
        } = this.props;

        const showHideAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }
        const showHideEditModal = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject: item,
                    editModal: !editModal
                }
            })
        }
        const showHideDeleteModal = (id) => {
            if (id) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: id,
                        deleteModal: !deleteModal
                    }
                })
            } else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }
        const showHideStatusModal = (id) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem: id,
                    statusModal: !statusModal
                }
            })
            if (id === 0) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        regions: null
                    }
                })
                this.props.dispatch(getRegionsAction())
            }
        }

        const saveItem = (e, v) => {
            v.regionId = this.state.region
            this.props.dispatch(saveRegionAction(v))
        }

        const deleteItem = () => {
            this.props.dispatch(deleteRegionAction(currentItem))

        }

        const editItem = (e, v) => {
            v.regionId = this.state.region
            this.props.dispatch(updateRegionAction(v))
        }

        const updateStatus = () => {
            console.log(currentItem)
            this.props.dispatch(patchRegionAction(currentItem))
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getRegionsAction())
            } else {
                this.props.dispatch(getRegionSearchAction(e.target.value))

            }
        }

        const setRegion = (e) => {
            this.setState({region: e.value})
        }

        return (
            <div>

                <Header title={"Regionlar ro'yhati"} addModal={showHideAddModal} search={search}/>

                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nomi</th>
                        <th>Izoh</th>
                        <th>Region</th>
                        <th>Faol</th>
                        <th className={'text-center'}>Amal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {regions ? regions.map((item, i) =>
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{item.name}</td>
                            <td>{item.description}</td>
                            <td>{item.region ? item.region.name : ""}</td>
                            <td>
                                <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id)}
                                       checked={item.active}/>
                            </td>
                            <td className={'text-center'}>
                                <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                    <EditButton/>
                                </div>
                                <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                    <DeleteButton/>
                                </div>
                            </td>
                        </tr>
                    ) : <tr>
                        <td colSpan={5}>Malumot topilmadi</td>
                    </tr>}
                    </tbody>
                </Table>


                <AddModal isOpen={addModal} toggle={showHideAddModal}
                          close={showHideAddModal} onValidSubmit={saveItem}>
                    <span>Nomi</span>
                    <AvField type="search" name="name" required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"/>

                    <div className={"mt-4"}>
                        <span>Region tanlang</span>
                        <Select
                            defaultValue={"0"}
                            placeholder={"Regionni tanlang"}
                            onChange={setRegion}
                            isSearchable={true}
                            options={regionsSearch}
                            classNamePrefix={"select"}
                        />
                    </div>
                    <br/>
                    <AvField type="checkbox" name="active" label={"Faol"}/>
                </AddModal>

                <EditModal isOpen={editModal} toggle={showHideEditModal}
                           close={showHideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"}
                             defaultValue={currentObject ? currentObject.id : ''}/>
                    <span>Nomi</span>
                    <AvField type="search" name="name" defaultValue={currentObject ? currentObject.name : ''}
                             required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"
                             defaultValue={currentObject ? currentObject.description : ''}/>
                    <span>Region</span>
                    <div className={"mt-4"}>
                        <span>Region tanlang</span>
                        <Select
                            defaultValue={currentObject && currentObject.region ? {
                                value: currentObject.region.id,
                                label: currentObject.region.name
                            } : ''}
                            placeholder={"Regionni tanlang"}
                            onChange={setRegion}
                            isSearchable={true}
                            options={regionsSearch}
                            classNamePrefix={"select"}
                        />
                    </div>
                    <br/>
                    <AvField type="checkbox" name="active" label={"Faol"}
                             defaultChecked={currentObject ? currentObject.active : false}/>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>
            </div>
        );
    }
}


export default connect(
    ({
         app: {
             addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
             warehouses, page, size, totalElements, totalPages, regions, regionsSearch
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        warehouses, page, size, totalElements, totalPages,
        regions,
        regionsSearch
    })
)(Region);