import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deleteKassaAction, getKassasAction,
    saveKassaAction, updateKassaAction,
} from "../../redux/actions/KassaAction";

import {Table} from "reactstrap";
import "./AdminPages.css";
import {AvField} from "availity-reactstrap-validation";
import {getCashNamesAction} from "../../redux/actions/CashNameAction";
import {getKassaPaymentTypesAction} from "../../redux/actions/KassaPaymentTypeAction";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import DeleteModal from "../../component/Modal/DeleteModal";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";

class Kassa extends Component {
    componentDidMount() {
        this.props.dispatch(getKassasAction())
        this.props.dispatch(getKassaPaymentTypesAction())
        this.props.dispatch(getCashNamesAction())
        console.clear()
    }


    render() {
        const {
            addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            page, size, totalElements, totalPages, kassas, kassaPaymentTypes, cashNames
        } = this.props;


        const showHideAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }
        const showHideEditModal = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject: item,
                    editModal: !editModal
                }
            })
        }
        const showHideDeleteModal = (id) => {
            if (id) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: id,
                        deleteModal: !deleteModal
                    }
                })
            } else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const saveItem = (e, v) => {
            this.props.dispatch(saveKassaAction(v))
        }
        const deleteItem = () => {
            this.props.dispatch(deleteKassaAction(currentItem))
        }

        const editItem = (e, v) => {
            this.props.dispatch(updateKassaAction(v))
        }

        return (
            <div>
                <Header title={"Kassalar ro'yhati"} addModal={showHideAddModal}/>

                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>amount</th>
                        <th>unity</th>
                        <th>Jami</th>
                        <th>Kassa To'lov turi</th>
                        <th>To'lov vaqti</th>
                        <th>Izoh</th>
                        <th>Amal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {kassas ? kassas.map((item, i) =>
                        <tr key={i}>
                            <td>{i + 1}</td>
                            <td>{item.amount}</td>
                            <td>{item.unity}</td>
                            <td>{item.allSum}</td>
                            <td>{item.kassaPaymentType.name}</td>
                            <td>{item.paymentTime}</td>
                            <td>{item.description}</td>
                            <td className={'text-center'}>
                                <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                    <EditButton/>
                                </div>
                                <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                    <DeleteButton/>
                                </div>
                            </td>
                        </tr>
                    ) : <tr>
                        <td colSpan={5}>Malumot topilmadi</td>
                    </tr>}
                    <tr>
                    </tr>
                    </tbody>
                </Table>


                <AddModal isOpen={addModal} toggle={showHideAddModal} close={showHideAddModal} onValidSubmit={saveItem}>
                    <AvField type="select" name="cashNameId" label="To'lov turi" helpMessage="Tanlash majburiy">
                        <option selected value={"0"}>To'lov turini tanlang</option>
                        {cashNames ? cashNames.map((item, i) =>
                                <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>

                    <AvField type="number" name="amount" label={"Amount"} required/>
                    <AvField type="number" name="unity" label={"Unity"} required/>
                    <AvField type="number" name="allSum" label={"Jami"} required/>

                    <AvField type="select" name="kassaPaymentTypeId" label="Kassa To'lov turi"
                             helpMessage="Tanlash majburiy">
                        <option selected value={""}>Kassa To'lov turini tanlang</option>
                        {kassaPaymentTypes ? kassaPaymentTypes.map((item, i) =>
                                <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>

                    <AvField name="description" label={"Izoh"}/>
                </AddModal>

                <EditModal isOpen={editModal} toggle={showHideEditModal} close={showHideEditModal}
                           onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"}
                             defaultValue={currentObject ? currentObject.id : ''}/>

                    <AvField type="select" name="cashNameId" label="To'lov turi" helpMessage="Tanlash majburiy">
                        <option selected
                                value={currentObject && currentObject.cashName ? currentObject.cashName.id : ''}>{currentObject && currentObject.cashName ? currentObject.cashName.name : 'Tolov turini tanlang'}</option>
                        {cashNames ? cashNames.map((item, i) =>
                                currentObject && currentObject.cashName && currentObject.cashName.id === item.id ? ''
                                    : <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>

                    <AvField name="amount" label={"Amount"}
                             defaultValue={currentObject ? currentObject.amount : false}
                             required/>
                    <AvField name="unity" label={"Unity"}
                             defaultValue={currentObject ? currentObject.unity : false}
                             required/>
                    <AvField name="allSum" label={"Jami"}
                             defaultValue={currentObject ? currentObject.allSum : false}
                             required/>

                    <AvField type="select" name="kassaPaymentTypeId" label="Kassa To'lov turi"
                             helpMessage="Tanlash majburiy">
                        <option selected
                                value={currentObject && currentObject.kassaPaymentType ? currentObject.kassaPaymentType.id : ''}>{currentObject && currentObject.kassaPaymentType ? currentObject.kassaPaymentType.name : 'Kassa Tolov turini tanlang'}</option>
                        {kassaPaymentTypes ? kassaPaymentTypes.map((item, i) =>
                                currentObject && currentObject.kassaPaymentType && currentObject.kassaPaymentType.id === item.id ? '' :
                                    <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>

                    <AvField name="description" label={"Izoh"}
                             defaultValue={currentObject ? currentObject.description : false}/>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
             page, size, totalElements, totalPages,
             kassas, kassaPaymentTypes, cashNames
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        page, size, totalElements, totalPages,
        kassas,
        kassaPaymentTypes,
        cashNames,
    })
)(Kassa);