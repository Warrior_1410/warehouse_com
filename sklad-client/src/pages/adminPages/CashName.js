import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deleteCashNameAction,
    getCashNamesAction, getCashNameSearchAction,
    patchCashNameAction,
    saveCashNameAction,
    updateCashNameAction
} from "../../redux/actions/CashNameAction";
import {Table, Input} from "reactstrap";
import "./AdminPages.css";
import {AvField} from "availity-reactstrap-validation";
import {toast} from "react-toastify";
import {getCurrencyNamesAction} from "../../redux/actions/CurrencyNameAction";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";

class CashName extends Component {

    componentDidMount() {
        this.props.dispatch(getCashNamesAction())
        this.props.dispatch(getCurrencyNamesAction())

        console.clear()
    }


    render() {
        const {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            page, size, totalElements, totalPages,cashNames, currencyNames} = this.props;

        const showHideAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }
        const showHideEditModal = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject : item,
                    editModal: !editModal
                }
            })
        }
        const showHideDeleteModal = (id) => {
            if (id){
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : id,
                        deleteModal: !deleteModal
                    }
                })
            }else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const saveItem = (e, v) => {
            if (v.currencyNameId === 0) {
                toast.warning("Currency Tanlash Majburiy")
            } else {
                this.props.dispatch(saveCashNameAction(v))

            }
        }
        const deleteItem = () => {
            this.props.dispatch(deleteCashNameAction(currentItem))
        }

        const editItem = (e, v) => {
            if (v.currencyNameId === 0) {
                toast.warning("Currency Tanlash Majburiy")
            } else {
                this.props.dispatch(updateCashNameAction(v))
            }
        }
        const showHideStatusModal = (id) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem : id,
                    statusModal: !statusModal
                }
            })
            if (id === 0) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        cashNames: null
                    }
                })
                this.props.dispatch(getCashNamesAction())
            }
        }

        const updateStatus = () => {
            this.props.dispatch(patchCashNameAction(currentItem))
        }
        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getCashNamesAction())
            } else {
                this.props.dispatch(getCashNameSearchAction(e.target.value))

            }
        }

        return (
            <div>
                <Header title={"To'lov turi ro'yhati"} addModal={showHideAddModal} search={search} />
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Izoh</th>
                            <th>CurrencyName</th>
                            <th>Faol</th>
                            <th className={'text-center'}>Amal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {cashNames ? cashNames.map((item, i) =>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>{item.currencyName ? item.currencyName.name : ""}</td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id)} checked={item.active}/>
                                </td>
                                <td className={'text-center'}>
                                    <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                        <EditButton />
                                    </div>
                                    <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                        <DeleteButton />
                                    </div>
                                </td>
                            </tr>
                        ) : <tr>
                            <td colSpan={5}>Malumot topilmadi</td>
                        </tr>}
                        </tbody>
                    </Table>

                <AddModal isOpen={addModal} toggle={showHideAddModal}
                          close={showHideAddModal} onValidSubmit={saveItem}>
                    <span>Nomi</span>
                    <AvField type="search" name="name" required/>
                    <span>Izoh</span>
                    <AvField type="text" name="description"/>
                    <span>Valyuta nomi</span>
                    <AvField type="select" name="currencyNameId" helpMessage="Tanlash majburiy" required>
                        <option selected value={"0"}>Valyuta nomini tanlang</option>
                        {currencyNames ? currencyNames.map((item, i) =>
                                <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>
                    <AvField type="checkbox" name="active" label={"Faol"}/>
                </AddModal>

                <EditModal isOpen={editModal} toggle={showHideEditModal}
                           close={showHideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"} defaultValue={currentObject ? currentObject.id : ''}/>
                    <span>Nomi</span>
                    <AvField type="search" name="name" defaultValue={currentObject ? currentObject.name : ''} required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description" defaultValue={currentObject ? currentObject.description : ''}/>
                    <span>Valyuta nomi</span>
                    <AvField type="select" name="currencyNameId" helpMessage="Tanlash majburiy">
                        <option selected value={currentObject && currentObject.currencyName ? currentObject.currencyName.id : ''}>{currentObject && currentObject.currencyName ? currentObject.currencyName.name : 'Valyuta nomini tanlang'}</option>
                        {currencyNames ? currencyNames.map((item, i) =>
                                currentObject && currentObject.currencyName && currentObject.currencyName.id ===item.id ? '' :
                                    <option value={item.id}>{item.name}</option>
                            ) :
                            <option>Malumot topilmadi</option>}
                    </AvField>

                    <AvField type="checkbox" name="active" label={"Faol"} defaultChecked={currentObject ? currentObject.active : false}/>
                </EditModal>


                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>
            </div>
        );
    }
}


export default connect(
    ({
         app: {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
             page, size, totalElements, totalPages, currencyNames,cashNames},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        page, size, totalElements, totalPages,
        cashNames,
        currencyNames,
    })
)(CashName);