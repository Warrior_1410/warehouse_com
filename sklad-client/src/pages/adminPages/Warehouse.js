import React, {Component} from 'react';
import {Table,Input,} from "reactstrap";
import {connect} from "react-redux";
import {
    saveWarehouseAction,
    getWarehousesAction,
    deleteWarehouseAction,
    updateWarehouseAction,
    patchWarehouseAction, getWarehousesSearchAction
} from "../../redux/actions/WarehouseAction";
import {AvField} from 'availity-reactstrap-validation';
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";

class Warehouse extends Component {
    componentDidMount() {
        this.props.dispatch(getWarehousesAction())
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getWarehousesAction({page: (pageNumber - 1), size: this.props.size}))
    }

    render() {
        const {addModal, editModal, deleteModal,statusModal,currentItem, currentObject,
            warehouses,page,size,totalElements,totalPages,} = this.props;


        const showHideAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }

        const showHideEditModal = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject : item,
                    editModal: !editModal
                }
            })
        }
        const showHideDeleteModal = (id) => {
            if (id){
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : id,
                        deleteModal: !deleteModal
                    }
                })
            }else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const showHideStatusModal = (id) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem : id,
                    statusModal: !statusModal
                }
            })
            if (id === 0) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        warehouses: null
                    }
                })
                this.props.dispatch(getWarehousesAction())
            }
        }

        const saveItem = (e, v) => {
            this.props.dispatch(saveWarehouseAction(v))
        }
        const deleteItem = () => {
            this.props.dispatch(deleteWarehouseAction(currentItem))
        }

        const editItem = (e, v) => {
            this.props.dispatch(updateWarehouseAction(v))
        }

        const updateStatus = () => {
            this.props.dispatch(patchWarehouseAction(currentItem))
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getWarehousesAction())
            } else {
                this.props.dispatch(getWarehousesSearchAction(e.target.value))
            }
        }

        return (
            <div>
                <Header title={"Omborlar ro'yhati"} addModal={showHideAddModal} search={search} />

                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Izoh</th>
                            <th>Faol</th>
                            <th className={'text-center'}>Amal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {warehouses ? warehouses.map((item, i) =>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id)} checked={item.active}/>
                                </td>
                                <td className={'text-center'}>
                                    <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                        <EditButton className={"editButton"} />
                                    </div>
                                    <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                        <DeleteButton />
                                    </div>
                                </td>
                            </tr>
                        ) : <tr>
                            <td colSpan={5}>Malumot topilmadi</td>
                        </tr>}
                        <tr>
                        </tr>
                        </tbody>
                    </Table>
                

                <AddModal isOpen={addModal} toggle={showHideAddModal}
                          close={showHideAddModal} onValidSubmit={saveItem}>
                    <span>Nomi</span>
                    <AvField type="search" name="name" required/>

                    <span>Izoh</span>
                    <AvField type="text" name="description"/>

                    <AvField type="checkbox" name="active" label={"Faol"}/>
                </AddModal>

                <EditModal isOpen={editModal} toggle={showHideEditModal}
                           close={showHideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"}
                             defaultValue={currentObject ? currentObject.id : ''}/>
                    <span>Nomi</span>
                    <AvField type="search" name="name"
                             defaultValue={currentObject ? currentObject.name : ''}
                             required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"
                             defaultValue={currentObject ? currentObject.description : ''}/>

                    <AvField type="checkbox" name="active" label={"Faol"} defaultChecked={currentObject ? currentObject.active : false}/>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>

            </div>
        );
    }
}

Warehouse.propTypes = {};

export default connect(
    ({
         app: {addModal,viewModal,editModal, deleteModal,statusModal,currentItem, currentObject, warehouses,page,size,totalElements,totalPages},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal,
        viewModal,
        deleteModal,
        editModal,
        statusModal,
        currentObject,
        currentItem,
        warehouses,
        page,size,totalElements,totalPages
    })
)(Warehouse);
