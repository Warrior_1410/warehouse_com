import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deleteBrandAction, getBrandsAction, patchBrandAction,
    saveBrandAction, searchBrandsAction, updateBrandAction,
} from "../../redux/actions/BrandAction";

import {Table, Input} from "reactstrap";
import "./AdminPages.css";
import {AvField} from "availity-reactstrap-validation";
import {deleteFileAction, getSupplierAction, uploadFileAction} from "../../redux/actions/AppActions";
import uploadInputImg from "../../resources/images/uploadImg.png";
import {getWarehousesAction} from "../../redux/actions/WarehouseAction";
import Pagination from "react-js-pagination";
import Select from "react-select";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";
import EditModal from "../../component/Modal/EditModal";
import AddModal from "../../component/Modal/AddModal";

class Brand extends Component {
    componentDidMount() {
        this.props.dispatch(getBrandsAction({page: 0, size: this.props.size}))
        this.props.dispatch(getSupplierAction())
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getWarehousesAction({page: (pageNumber - 1), size: this.props.size}))
    }

    constructor(props) {
        super(props);
        this.state = {
            user: '',
            brandId: '',
            request: '',
            image : true
        }
    }

    render() {
        const {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            brands, attachmentId, currentUser, suppliers, attachmentUrl, page, size, totalElements, totalPages} = this.props;


        const uploadImg = (e) => {
            this.props.dispatch(uploadFileAction(e.target.files[0]))
        }

        const deleteImg = (id,isEdit) => {
            if (isEdit) {
                    this.props.dispatch(deleteFileAction({name: "byBrand", id: id}))
                    this.setState({image: false})
            }else {
                if (id === 0) {
                    this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
                }
                else {
                    this.props.dispatch(deleteFileAction({name: "byBrand", id: id}))
                }
            }
        }

        const showAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: true
                }
            })
        }
        const hideAddModal = () => {
            if (attachmentId) {
                this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: false
                }
            })
        }

        const showEditModal = (item) => {
            if (item.attachmentId){
                this.setState({image : true})
            }
            else {
                this.setState({image : false})
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject : item,
                    editModal: true
                }
            })

        }
        const hideEditModal = () => {
            if (attachmentId) {
                this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject : null,
                    editModal: false
                }
            })
        }

        const showHideDeleteModal = (id) => {
            if (id){
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : id,
                        deleteModal: !deleteModal
                    }
                })
            }else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const saveItem = (e, v) => {
            v.userId = this.state.user
            v.attachmentId = attachmentId
            this.props.dispatch(saveBrandAction(v))

            this.props.dispatch({
                type: "updateState",
                payload: {
                    attachmentId: null
                }
            })
        }
        const deleteItem = () => {
            this.props.dispatch(deleteBrandAction(currentItem))
        }

        const editItem = (e, v) => {
            v.userId = this.state.user
            if(attachmentId){
                v.attachmentId = attachmentId
            }else {
                v.attachmentId = currentObject.attachmentId
            }

            this.props.dispatch(updateBrandAction(v))

            this.props.dispatch({
                type: "updateState",
                payload: {
                    attachmentId: null
                }
            })
        }
        const showHideStatusModal = (id, name) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem : {id: id, request: name}
                }
            })
            if (id === 0) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        brands: null
                    }
                })
                this.props.dispatch(getBrandsAction())
            }
        }

        const updateStatus = () => {
            this.props.dispatch(patchBrandAction(currentItem))
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getBrandsAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(searchBrandsAction(e.target.value))

            }
        }

        const setUser = (e) => {
            this.setState({user: e.value})
        }

        return (
            <div>
                <Header title={"Brendlarlar ro'yhati"} addModal={showAddModal} search={search} />

                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Izoh</th>
                            <th>Yetkazib beruvchi</th>
                            <th>Faol</th>
                            <th>Mobil</th>
                            <th className={'text-center'}>Amal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {brands ? brands.map((item, i) =>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>{item.supplierUser ? item.supplierUser.fullName : ''}</td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id, "active")}
                                            checked={item.active}/>
                                </td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id, "mobile")}
                                            checked={item.mobile}/>
                                </td>
                                <td className={'text-center'}>
                                    <div className={"editButton"} onClick={() => showEditModal(item)}>
                                        <EditButton />
                                    </div>
                                    <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                        <DeleteButton />
                                    </div>
                                </td>
                            </tr>
                        ) : <tr>
                            <td colSpan={5}>Malumot topilmadi</td>
                        </tr>}
                        </tbody>
                    </Table>
                    <Pagination
                        activePage={page + 1}
                        itemsCountPerPage={size}
                        totalItemsCount={totalElements}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                        linkClass="page-link"
                    />

                <AddModal isOpen={addModal} toggle={hideAddModal} close={hideAddModal} onValidSubmit={saveItem}>
                    <span>Nomi</span>
                    <AvField type="search" name="name" required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"/>
                    <AvField type="checkbox" name="active" label={"Faol"}/>
                    <AvField type="checkbox" name="mobile" label={"Mobil"}/>
                    <div className={"fileUpdateGroup mx-auto mt-4"}>
                        <div className="file-input">
                            <input type="file" id="file" name={"attachmentId"} onChange={uploadImg} className="file"/>
                            <label htmlFor="file"><img src={uploadInputImg}/> &nbsp; Rasmni yuklang</label>
                            {attachmentId ?
                                <div className={"imgPosition"}>
                                    <span onClick={() => deleteImg(0,false)} className={"removeBtn"}>
                                            <DeleteButton/>
                                        </span>
                                    <img src={attachmentUrl + attachmentId} className={"img-thumbnail"}/>
                                </div>
                                :
                                ""}
                        </div>
                    </div>
                    <div className={"mt-5"}>
                        <span>Yetkazib beruvchi</span>
                        <Select
                            defaultValue={"0"}
                            placeholder={"Yetkazib beruvchini tanlang"}
                            onChange={setUser}
                            isSearchable={true}
                            options={suppliers}
                            classNamePrefix={"select"}
                        />
                    </div>
                </AddModal>

                <EditModal isOpen={editModal} toggle={hideEditModal} close={hideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"}
                             defaultValue={currentObject ? currentObject.id : ''}/>
                    <span>Nomi</span>
                    <AvField type="search" name="name" defaultValue={currentObject ? currentObject.name : ''}
                             required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"
                             defaultValue={currentObject ? currentObject.description : ''}/>

                    <AvField type="checkbox" name="active" label={"Faol"} defaultChecked={currentObject ? currentObject.active : false}/>
                    <AvField type="checkbox" name="mobile" label={"Mobil"} defaultChecked={currentObject ? currentObject.mobile : false}/>
                    <div className={"fileUpdateGroup mx-auto mt-4"}>
                        <div className="file-input">
                            <input type="file" id="file" name={"attachmentId"} onChange={uploadImg} className="file"/>
                            <label htmlFor="file"><img src={uploadInputImg}/> &nbsp; Rasmni yuklang</label>
                            {this.state.image ?
                                currentObject && currentObject.attachmentId ?
                                    <div className={"imgPosition text-right"}>
                                        <span onClick={() => deleteImg(currentObject.attachmentId,true)} className={"removeBtn"}>
                                            <DeleteButton/>
                                        </span>
                                        <img src={attachmentUrl + currentObject.attachmentId} className={"img-thumbnail"}/>
                                    </div> : ''
                                :
                                attachmentId ? <div className={"imgPosition text-right"}>
                                    <span onClick={() => deleteImg(0,false)} className={"removeBtn"}>
                                            <DeleteButton/>
                                    </span>
                                    <img src={attachmentUrl + attachmentId} className={"img-thumbnail"}/>
                                </div> : ""}
                        </div>
                    </div>
                    <div className={"mt-4"}>
                        <span>Yetkazib beruvchi</span>
                        <Select
                            defaultValue={currentObject && currentObject.supplierUser ? {value:currentObject.supplierUser.id,label:currentObject.supplierUser.fullName} : ''}
                            placeholder={"Yetkazib beruvchini tanlang"}
                            onChange={setUser}
                            isSearchable={true}
                            options={suppliers}
                            classNamePrefix={"select"}
                        />
                    </div>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             suppliers,brands, attachmentId, page, size, totalElements, totalPages, attachmentUrl,
             addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        brands,
        attachmentId,
        suppliers,
        attachmentUrl,
        page, size, totalElements, totalPages
    })
)(Brand);