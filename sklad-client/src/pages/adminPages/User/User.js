import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAllUserAction, registerAction} from "../../../redux/actions/AppActions";
import Pagination from "react-js-pagination";
import {toast} from "react-toastify";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Modal, ModalFooter, Table,Row,Col} from "reactstrap";
import {getRegionsAction} from "../../../redux/actions/RegionAction";
import Header from "../../../component/Header/Header";
import {X} from "../../../component/Icons";

class User extends Component {
    componentDidMount() {
        this.props.dispatch(getAllUserAction({page: 0, size: this.props.size}))
        this.props.dispatch(getRegionsAction())
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getAllUserAction({page: (pageNumber - 1), size: this.props.size}))
    }

    state = {
        openModal: false
    }

    render() {
        const {regions, allUser, page, size, totalElements, totalPages} = this.props
        console.log(allUser)
        const showHideAddModal = () => {
            this.setState({openModal: !this.state.openModal})
        }

        const handleRegister = async (e, v) => {
            console.log(v)
            if (v.regionId === "0") {
                toast.error("Region tanlash majburiy")
            } else {
                this.props.dispatch(registerAction(v))
            }
        };

        return (
            <div>
                {this.state.openModal ?
                        <AvForm className={"text-left content container"} onValidSubmit={handleRegister}>
                            <img src={<X />} alt="" align={"right"} onClick={showHideAddModal}/>
                            <h4 className={"title"}>Foydalanuvchi qo'shish</h4>
                            <hr />
                            <Row>
                                <Col>
                                    <AvField name={"fullName"} label={"Full Name"} required/>
                                    <AvField name={"phoneNumber"} label={"Phone Number"} required/>
                                    <AvField name={"password"} label={"Parol"} required/>
                                </Col>
                                <Col>
                                    <AvField type={"select"} name={"roleName"} label={"Role Name"} required>
                                        <option selected>Lavozim</option>
                                        <option value={"CLIENT"}>CLIENT</option>
                                        <option value={"SUPPLIER"}>SUPPLIER</option>
                                        <option value={"AGENT"}>AGENT</option>
                                    </AvField>

                                    <AvField type={"select"} name={"regionId"} label={"Region"} required>
                                        <option value={"0"} selected>Region tanlang</option>
                                        {regions ? regions.map((item, i) =>
                                                <option value={item.id}>{item.name}</option>
                                            )
                                            :
                                            <option value={"0"}>Malumot topilmadi</option>
                                        }
                                    </AvField>
                                </Col>
                            </Row>

                            <ModalFooter className={"modal-footer mt-3"}>
                                <Button outline color="secondary" className={"buttonBack"} onClick={showHideAddModal}
                                        type={"button"}>Bekor qilish</Button>
                                <Button color="success" className={"buttonSave"} type={"submit"}>Saqlash</Button>
                            </ModalFooter>
                        </AvForm>
                    :
                    <div>
                        <Header title={"Foydalanuvchilar ro'yhati"} addModal={showHideAddModal} />
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ism / Sharif</th>
                            <th>Telefon raqam</th>
                            <th>Telefon raqam 2</th>
                            <th>Manzil</th>
                            <th>Hudud</th>
                            <th>Bank</th>
                        </tr>
                        </thead>
                        <tbody>
                        {allUser ? allUser.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.fullName}</td>
                                <td>{item.phoneNumber}</td>
                                <td>{item.phoneNumber2}</td>
                                <td>{item.address}</td>
                                <td>{item.region ? item.region.name : ''}</td>
                                <td>{item.bank ? item.bank.name : ''}</td>
                            </tr>
                        ) : ''}
                        </tbody>
                    </Table>
                    <Row>
                        <Col md={5}></Col>
                        <Col>
                            <Pagination
                                activePage={page + 1}
                                itemsCountPerPage={size}
                                totalItemsCount={totalElements}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Col>
                    </Row>
                </div>
                }



            </div>
        );
    }
}

User.propTypes = {};

export default connect(
    ({
         app: {regions, allUser, page, size, totalElements, totalPages},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        regions, allUser, page, size, totalElements, totalPages
    })
)(User);