import React, {Component} from 'react';
import {
    Row,
    Col,
} from "reactstrap";
import {connect} from "react-redux";
import {X} from "../../component/Icons";

class ProductTemplateGet extends Component {
    render() {

        const {object,attachmentUrl,attachmentId,openFullView} = this.props;

        const openFullViewFun = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    openFullView: !openFullView
                }
            })
        }
        return (
            <div className={"container pt-5"}>
                <Row>
                    <Col md={11}>
                        <h3>Batafsil</h3>
                    </Col>
                    <Col md={1} className={"text-right"} onClick={openFullViewFun}>
                        <X />
                    </Col>
                </Row>
                <hr />
                <Row className={"pt-5"}>
                    <Col md={5}>
                        {object && object.attachmentId ?
                            <img className={"img-thumbnail"} src={attachmentUrl + object.attachmentId}/>
                            :
                            ""}
                    </Col>
                    <Col md={7}>
                        <Row>
                            <Col>
                                <span>Nomi</span>
                                <h4 className={"form-control"}>{object ? object.name : ''}</h4>
                                <span>Brend</span>
                                <h4 className={"form-control"}>{object && object.brand ? object.brand.name : ''}</h4>
                                <span>Mahsulot turi</span>
                                <h4 className={"form-control"}>{object && object.productCategory ? object.productCategory.name : ''}</h4>
                                <Row>
                                    <Col>
                                        <input type={"checkbox"} className={"custom-chechbox m-3"} checked={object ? object.active : false} readOnly />
                                        <span>Faol</span>
                                    </Col>
                                    <Col>
                                        <input type={"checkbox"} className={"custom-checkbox m-3"} checked={object ? object.mobile : false} readOnly />
                                        <span>Mobile</span>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Col>
                                        <span>Kod</span>
                                        <h4 className={"form-control"}>{object ? object.kod : ''}</h4>
                                    </Col>
                                    <Col>
                                        <span>BarCode</span>
                                        <h4 className={"form-control"}>{object ? object.barcode : ''}</h4>
                                    </Col>
                                </Row>
                                <span>O'lchov birligi</span>
                                <h4 className={"form-control"}>{object && object.measure ? object.measure.name : ''}</h4>
                                <span>Minimum miqdori</span>
                                <h4 className={"form-control"}>{object ? object.minCount : ''}</h4>

                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>

        );
    }
}

ProductTemplateGet.propTypes = {};

export default connect(
    ({
         app: {
                attachmentUrl,attachmentId,openFullView
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        attachmentUrl,attachmentId,openFullView
    })
)(ProductTemplateGet);
