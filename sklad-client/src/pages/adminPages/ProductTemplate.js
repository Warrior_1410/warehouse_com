import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    getProductTemplatesAction,
    saveProductTemplateAction,
    updateProductTemplateAction,
    deleteProductTemplateAction,
    patchProductTemplateAction, searchProductTemplateAction
} from "../../redux/actions/ProductTemplateAction";
import {Table, Button, Modal, ModalFooter, ModalBody, Row, Col, Input, Container} from "reactstrap";
import "./AdminPages.css";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {toast} from "react-toastify";
import {getMeasuresAction} from "../../redux/actions/MeasureAction";
import {getProductCategoriesAction} from "../../redux/actions/ProductCategoryAction";
import {getBrandsAction} from "../../redux/actions/BrandAction";
import {deleteFileAction, uploadFileAction} from "../../redux/actions/AppActions";
import {getWarehousesAction} from "../../redux/actions/WarehouseAction";
import Select from 'react-select';
import uploadInputImg from "../../resources/images/uploadImg.png";
import Pagination from "react-js-pagination";
import ProductTemplateGet from "./ProductTemplateGet";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton, EyeButton, X} from "../../component/Icons";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";
import FullModal from "../../component/Modal/FullModal";

class ProductTemplate extends Component {
    componentDidMount() {
        this.props.dispatch(getProductTemplatesAction({page: 0, size: this.props.size}))
        this.props.dispatch(getBrandsAction())
        this.props.dispatch(getProductCategoriesAction())
        this.props.dispatch(getMeasuresAction())

    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getWarehousesAction({page: (pageNumber - 1), size: this.props.size}))
    }

    constructor(props) {
        super(props);
        this.state = {
            productCategory: '',
            brand: '',
            productTemplateId: '',
            request: '',
            image: true
        }
    }

    render() {

        const {
            addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            attachmentId, productTemplates, brands, productCategories, measures, brandsSearch, productCategoriesSearch,
            page, size, totalElements, totalPages, attachmentUrl, openFullView
        } = this.props


        const uploadImg = (e) => {
            this.props.dispatch(uploadFileAction(e.target.files[0]))
        }
        const deleteImg = (id, isEdit) => {
            if (isEdit) {
                this.props.dispatch(deleteFileAction({name: "byTemplate", id: id}))
                this.setState({image: false})
            } else {
                if (id === 0) {
                    this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
                } else {
                    this.props.dispatch(deleteFileAction({name: "byTemplate", id: id}))
                }
            }
        }

        const showHideAddModal = () => {
            if (attachmentId) {
                this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }

        const showHideEditModal = (item) => {
            if (attachmentId) {
                this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
            }
            if (item) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentObject: item,
                        editModal: !editModal
                    }
                })
            } else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentObject: null,
                        editModal: !editModal
                    }
                })
            }
        }

        const AllFullView = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    openFullView: !openFullView
                }
            })
            if (item) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentObject: item
                    }
                })
            } else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentObject: item
                    }
                })
            }
        }

        const showHideDeleteModal = (id) => {
            if (id) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: id,
                        deleteModal: !deleteModal
                    }
                })
            } else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem: '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const saveItem = (e, v) => {
            v.brandId = this.state.brand
            v.productCategoryId = this.state.productCategory
            if (v.brandId === '' || v.productCategoryId === '' || v.measureId === '') {
                toast.error("Malumot to'ldirish majburiy")
            } else {

                v.attachmentId = attachmentId
                this.props.dispatch(saveProductTemplateAction(v))
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        attachmentId: null
                    }
                })
            }
        }
        const deleteItem = () => {
            this.props.dispatch(deleteProductTemplateAction(currentItem))
        }
        const editItem = (e, v) => {
            if (attachmentId) {
                v.attachmentId = attachmentId
            } else {
                v.attachmentId = currentObject.attachmentId
            }
            v.brandId = this.state.brand
            v.productCategoryId = this.state.productCategory
            this.props.dispatch(updateProductTemplateAction(v))
        }
        const showHideStatusModal = (id, name) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem: {id: id, request: name},
                    statusModal: !statusModal
                }
            })
            if (id === 0) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        productTemplates: null
                    }
                })
                this.props.dispatch(getProductTemplatesAction())
            }
        }
        const updateStatus = () => {
            this.props.dispatch(patchProductTemplateAction(currentItem))
        }
        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getProductTemplatesAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(searchProductTemplateAction(e.target.value))
            }
        }
        const setItem = (e, item) => {
            if (item === "brand") {
                this.setState({brand: e.value})
            } else if (item === "product") {
                this.setState({productCategory: e.value})
            }
        }

        return (
            <div>
                {openFullView ?
                    <ProductTemplateGet object={currentObject}/>
                    :
                    addModal ?
                        <Container className={"pt-5"} >
                                <AvForm onValidSubmit={saveItem}>
                                    <Row>
                                        <Col md={11}>
                                            <h3>Mahsulot shablonini qo'shish</h3>
                                        </Col>
                                        <Col md={1} className={"text-right"}>
                                            <span onClick={showHideAddModal}>
                                                <X />
                                            </span>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <div className="col-4">
                                            <span>Brend</span>
                                            <Select
                                                defaultValue={"0"}
                                                placeholder={"Brandni tanlang"}
                                                onChange={(e) => setItem(e, "brand")}
                                                isSearchable={true}
                                                options={brandsSearch}
                                                classNamePrefix={"select"}
                                            />
                                        </div>
                                        <div className="col-4">
                                            <span>O'lchov birligi</span>
                                            <AvField type="select" name="measureId">
                                                <option selected value={""}>O'lchovni ni tanlang</option>
                                                {measures ? measures.map((item, i) =>
                                                        <option value={item.id}>{item.name}</option>
                                                    ) :
                                                    <option>Malumot topilmadi</option>}
                                            </AvField>
                                        </div>
                                        <div className="col-4">
                                            <span>Mahsulot turi</span>
                                            <Select
                                                defaultValue={"0"}
                                                placeholder={"Mahsulot turini tanlang"}
                                                onChange={(e) => setItem(e, "product")}
                                                isSearchable={true}
                                                options={productCategoriesSearch}
                                                classNamePrefix={"select"}
                                            />
                                        </div>
                                    </Row>
                                    <Row>
                                        <div className="col-4 mt-4">
                                            <span>Kod</span>
                                            <AvField type="search" name="kod" required/>
                                            <div className={'pt-4'}>
                                                <span>BarCode</span>
                                                <AvField type="search" name="barcode" required/>
                                            </div>
                                            <AvField type="checkbox" name="active" label={"Faol"}/>
                                            <AvField type="checkbox" name="mobile" label={"Mobil"}/>

                                        </div>
                                        <div className="col-4 mt-4">
                                            <span>Nomi</span>
                                            <AvField type="search" name="name" className="form-control name"
                                                     required/>
                                            <div className={'pt-4'}>Minimum miqdori</div>
                                            <AvField type="search" name="minCount" className="form-control name"
                                                     required/>
                                        </div>
                                        <div className="col-4">
                                            <div className={"fileUpdateGroup mx-auto mt-4"}>
                                                <div className="file-input">
                                                    <input type="file" id="file" name={"attachmentId"}
                                                           onChange={uploadImg} className="file"/>
                                                    <label htmlFor="file"><img
                                                        src={uploadInputImg}/> &nbsp; Rasmni yuklang</label>
                                                    {attachmentId ?
                                                        <div className={"imgPosition"}>
                                                            <img src={<DeleteButton/>} onClick={() => deleteImg(0, false)}
                                                                 className={"removeBtn"}/>
                                                            <img src={attachmentUrl + attachmentId}
                                                                 className={"img-thumbnail"}/>
                                                        </div>
                                                        :
                                                        ""}
                                                </div>
                                            </div>
                                        </div>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Button color={"primary"} type={"submit"}>Saqlash</Button>
                                        </Col>
                                    </Row>
                                </AvForm>
                            </Container>
                        :
                        editModal ?
                            <Container className={"pt-5"}>
                                <AvForm onValidSubmit={editItem}>
                                    <AvField type="hidden" name="id" defaultValue={currentObject ? currentObject.id : ''} required/>
                                    <Row>
                                        <Col md={11}>
                                            <h3>Malumotlarni o'zgartirish</h3>
                                        </Col>
                                        <Col md={1} className={"text-right"}>
                                            <span onClick={showHideEditModal}>
                                                <X />
                                            </span>
                                        </Col>
                                    </Row>
                                    <hr/>
                                    <Row>
                                        <Col md={5}>
                                            <div className={"fileUpdateGroup w-75 mx-auto mt-4"}>
                                                <div className="file-input">
                                                    <input type="file" id="file" name={"attachmentId"} onChange={uploadImg}
                                                           className="file"/>
                                                    <label htmlFor="file"><img src={uploadInputImg}/> &nbsp; Rasmni
                                                        yuklang</label>
                                                    {this.state.image ?
                                                        currentObject && currentObject.attachmentId ?
                                                            <div className={"imgPosition text-right"}>
                                                                <span onClick={() => deleteImg(currentObject.attachmentId, true)} className={"removeBtn"}>
                                                                    <DeleteButton/>
                                                                </span>
                                                                <img src={attachmentUrl + currentObject.attachmentId} className={"img-thumbnail"}/>
                                                            </div> : ''
                                                        :
                                                        attachmentId ? <div className={"imgPosition text-right"}>
                                                            <span onClick={() => deleteImg(0, false)} className={"removeBtn"}>
                                                                <DeleteButton/>
                                                            </span>
                                                            <img src={attachmentUrl + attachmentId}
                                                                 className={"img-thumbnail"}/>
                                                        </div> : ""}
                                                </div>

                                            </div>
                                        </Col>
                                        <Col md={7}>
                                            <Row>
                                                <Col>
                                                    <span>Nomi</span>
                                                    <AvField type="search" name="name" className="form-control"
                                                             defaultValue={currentObject ? currentObject.name : ''}
                                                             required/>

                                                    <span>Brend</span>
                                                    <Select
                                                        defaultValue={currentObject && currentObject.brand ? {
                                                            value: currentObject.brand.id,
                                                            label: currentObject.brand.name
                                                        } : ''}
                                                        placeholder={"Brandni tanlang"}
                                                        onChange={(e) => setItem(e, "brand")}
                                                        isSearchable={true}
                                                        options={brandsSearch}
                                                        classNamePrefix={"select"}
                                                    />
                                                    <span className={"pt-2 mt-2 d-block"}>Mahsulot turi</span>
                                                    <Select
                                                        defaultValue={currentObject && currentObject.productCategory ? {
                                                            value: currentObject.productCategory.id,
                                                            label: currentObject.productCategory.name
                                                        } : ''}
                                                        placeholder={"Mahsulot turini tanlang"}
                                                        onChange={(e) => setItem(e, "product")}
                                                        isSearchable={true}
                                                        options={productCategoriesSearch}
                                                        classNamePrefix={"select"}
                                                    />
                                                </Col>

                                                <Col>
                                                    <Row>
                                                        <Col>
                                                            <span>Kod</span>
                                                            <AvField type="search" name="kod" defaultValue={currentObject ? currentObject.kod : ''} required/>
                                                        </Col>
                                                        <Col>
                                                            <span>Bar Code</span>
                                                            <AvField type="search" name="barcode" defaultValue={currentObject ? currentObject.barcode : ''} required/>
                                                        </Col>
                                                    </Row>


                                                    <span>O'lchov birligi</span>
                                                    <AvField type="select" name="measureId">
                                                        <option selected
                                                                value={currentObject && currentObject.measure ? currentObject.measure.id : ''}>{currentObject && currentObject.measure ? currentObject.measure.name : 'O`lchov birligini tanlang'}</option>
                                                        {measures ? measures.map((item, i) =>
                                                                currentObject && currentObject.measure && currentObject.measure.id === item.id ? '' :
                                                                    <option value={item.id}>{item.name}</option>
                                                            ) :
                                                            <option>Malumot topilmadi</option>}
                                                    </AvField>

                                                    <span>Minimum miqdori</span>
                                                    <AvField type="search" name="minCount" defaultValue={currentObject ? currentObject.minCount : ''} required/>


                                                </Col>

                                            </Row>
                                            <Row>
                                                <Col>
                                                    <Row>
                                                        <Col>
                                                            <AvField type="checkbox"
                                                                     name="active" label={"Faol"}
                                                                     defaultChecked={currentObject ? currentObject.active : false}/>
                                                        </Col>
                                                        <Col>
                                                            <AvField type="checkbox"
                                                                     name="mobile" label={"Mobil"}
                                                                     defaultChecked={currentObject ? currentObject.mobile : false}/>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col>
                                                    <Button color="success" className={"mx-auto"}>Saqlash</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </AvForm>
                            </Container>
                            :
                            <div>
                                <Header title={"Omborlar ro'yhati"} addModal={showHideAddModal} search={search}/>

                                <Table>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nomi</th>
                                        <th>Mahsulot turi</th>
                                        <th>Brend</th>
                                        <th>Faol</th>
                                        <th>Mobile</th>
                                        <th className={"text-center"}>Amal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {productTemplates ? productTemplates.map((item, i) =>
                                        <tr key={i}>
                                            <td>{i + 1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.productCategory ? item.productCategory.name : ''}</td>
                                            <td>{item.brand ? item.brand.name : ''}</td>
                                            <td>
                                                <Input type={"checkbox"}
                                                       onChange={() => showHideStatusModal(item.id, "active")}
                                                       checked={item.active}/>
                                            </td>
                                            <td>
                                                <Input type={"checkbox"}
                                                       onChange={() => showHideStatusModal(item.id, "mobile")}
                                                       checked={item.mobile}/>
                                            </td>
                                            <td className={'text-center'}>
                                                <div className={"viewButton"} onClick={() => AllFullView(item)}>
                                                    <EyeButton/>
                                                </div>
                                                <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                                    <EditButton/>
                                                </div>
                                                <div className={"deleteButton"}
                                                     onClick={() => showHideDeleteModal(item.id)}>
                                                    <DeleteButton/>
                                                </div>
                                            </td>
                                        </tr>
                                    ) : <tr>
                                        <td colSpan={5}>Malumot topilmadi</td>
                                    </tr>}
                                    </tbody>
                                </Table>
                                <Pagination
                                    activePage={page + 1}
                                    itemsCountPerPage={size}
                                    totalItemsCount={totalElements}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                    linkClass="page-link"
                                />

                            </div>
                }


                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             addModal, editModal, deleteModal, statusModal, currentItem, currentObject, openFullView,
             attachmentId, brandsSearch, productCategoriesSearch,
             productTemplates, brands, productCategories, measures, page, size, totalElements, totalPages, attachmentUrl
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject, openFullView,

        attachmentId,
        productTemplates,
        brands,
        productCategories,
        measures,
        brandsSearch,
        productCategoriesSearch,
        attachmentUrl,
        page, size, totalElements, totalPages
    })
)(ProductTemplate);
