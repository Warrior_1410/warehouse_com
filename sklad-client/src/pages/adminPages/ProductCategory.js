import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deleteProductCategoryAction,
    getProductCategoriesAction,
    getProductCategoriesSearchAction,
    patchProductCategoryAction,
    saveProductCategoryAction,
    updateProductCategoryAction,
} from "../../redux/actions/ProductCategoryAction";
import {Table, Input} from "reactstrap";
import {AvField} from "availity-reactstrap-validation";
import uploadInputImg from "../../resources/images/uploadImg.png"
import {deleteFileAction, uploadFileAction} from "../../redux/actions/AppActions";
import {getWarehousesAction} from "../../redux/actions/WarehouseAction";
import Pagination from "react-js-pagination";
import Select from "react-select";
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";

class ProductCategory extends Component {
    componentDidMount() {
        this.props.dispatch(getProductCategoriesAction({page: 0, size: this.props.size}))
        console.clear()
    }
    handlePageChange(pageNumber) {
        this.props.dispatch(getWarehousesAction({page: (pageNumber - 1), size: this.props.size}))
    }

    constructor(props) {
        super(props);
        this.state = {
            category : '',
            productCategoryId: '',
            request: '',
            image :true
        }
    }

    render() {
        const {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,productCategories, attachmentId,productCategoriesSearch,attachmentUrl, page,size,totalElements,totalPages} = this.props;


        const uploadImg = (e) => {
            this.props.dispatch(uploadFileAction(e.target.files[0]))
        }
        const deleteImg = (id,isEdit) => {
            if (isEdit) {
                this.props.dispatch(deleteFileAction({name: "byCategory", id: id}))
                this.setState({image: false})
            }else {
                if (id === 0) {
                    this.props.dispatch(deleteFileAction({name: "all", id: attachmentId}))
                }
                else {
                    this.props.dispatch(deleteFileAction({name: "byCategory", id: id}))
                }
            }
        }

        const showAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: true
                }
            })
        }
        const hideAddModal = () => {
            if (attachmentId){
                this.props.dispatch(deleteFileAction({name:"all",id:attachmentId}))
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: false
                }
            })
        }


        const showEditModal = (item) => {
            if (item.attachment.id){
                this.setState({image : true})
            }
            else {
                this.setState({image : false})
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject : item,
                    editModal: true
                }
            })
        }
        const hideEditModal = () => {
            if (attachmentId){
                this.props.dispatch(deleteFileAction({name:"all",id:attachmentId}))
            }
            this.props.dispatch({
                type: "updateState",
                payload: {
                    editModal: false,
                    currentObject: null
                }
            })
        }


        const showHideDeleteModal = (id) => {
            if (id){
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : id,
                        deleteModal: !deleteModal
                    }
                })
            }else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }


        const saveItem = (e, v) => {
            v.categoryId = this.state.category
            v.attachmentId = attachmentId
            this.props.dispatch(saveProductCategoryAction(v))
        }
        const deleteItem = () => {
            this.props.dispatch(deleteProductCategoryAction(currentItem))
        }

        const editItem = (e, v) => {
            if (attachmentId) {
                v.attachmentId = attachmentId
            }else {
                v.attachmentId = currentObject.attachment.id
            }
            this.props.dispatch(updateProductCategoryAction(v))
        }

        const showHideStatusModal = (id, name) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem : {id: id, request: name},
                    statusModal: !statusModal
                }
            })
            if (id === 0) {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        productCategories: null
                    }
                })
                this.props.dispatch(getProductCategoriesAction())
            }
        }

        const updateStatus = () => {
            this.props.dispatch(patchProductCategoryAction(currentItem))
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getProductCategoriesAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(getProductCategoriesSearchAction(e.target.value))

            }
        }

        const setCategory = (e) => {
            this.setState({category : e.value})
        }



        return (
            <div>
                <Header title={"Mahsulot turi ro'yhati"} addModal={showAddModal} search={search} />

                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Izoh</th>
                            <th>Mahsulot Turi</th>
                            <th>Faol</th>
                            <th>Mobil</th>
                            <th className={'text-center'}>Amal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {productCategories ? productCategories.map((item, i) =>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>
                                    {item.category ? item.category.name : ''}
                                </td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id,"active")} checked={item.active}/>
                                </td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id,"mobile")} checked={item.mobile}/>
                                </td>
                                <td className={'text-center'}>
                                    <div className={"editButton"} onClick={() => showEditModal(item)}>
                                        <EditButton />
                                    </div>
                                    <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                        <DeleteButton />
                                    </div>
                                </td>
                            </tr>
                        ) : <tr>
                            <td colSpan={5}>Malumot topilmadi</td>
                        </tr>}
                        </tbody>
                    </Table>
                    <Pagination
                        activePage={page + 1}
                        itemsCountPerPage={size}
                        totalItemsCount={totalElements}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                        linkClass="page-link"
                    />

                <AddModal isOpen={addModal} toggle={hideAddModal} close={hideAddModal} onValidSubmit={saveItem}>
                    <span>Nomi</span>
                    <AvField type="search" name="name" required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"/>

                        <AvField type="checkbox" name="active" label={"Faol"}/>
                        <AvField type="checkbox" name="mobile" label={"Mobil"}/>

                    <div className={"fileUpdateGroup mx-auto mt-4"}>
                        <div className="file-input">
                            <input type="file" id="file" name={"attachmentId"} onChange={uploadImg} className="file"/>
                            <label htmlFor="file"><img src={uploadInputImg}/> &nbsp; Rasmni yuklang</label>
                            {attachmentId ?
                                <div className={"imgPosition"}>
                                    <span onClick={() => deleteImg(0,false)} className={"removeBtn"}>
                                            <DeleteButton/>
                                        </span>
                                    <img src={attachmentUrl + attachmentId} className={"img-thumbnail"}/>
                                </div>
                                :
                                ""}
                        </div>
                    </div>
                    <div className={"mt-5"}>
                        <span>Mahsulot turi</span>
                        <Select
                            defaultValue={"0"}
                            placeholder={"Mahsulot turini tanlang"}
                            onChange={setCategory}
                            isSearchable={true}
                            options={productCategoriesSearch}
                            classNamePrefix={"select"}
                        />
                    </div>

                </AddModal>

                <EditModal isOpen={editModal} toggle={hideEditModal} close={hideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"} defaultValue={currentObject ? currentObject.id : ''}/>
                    <span>Nomi</span>
                    <AvField type="search" name="name" defaultValue={currentObject ? currentObject.name : ''} required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"
                             defaultValue={currentObject ? currentObject.description : ''}/>

                    <AvField type="checkbox" name="active" label={"Faol"}
                             defaultChecked={currentObject ? currentObject.active : false}/>
                    <AvField type="checkbox" name="mobile" label={"Mobil"}
                             defaultChecked={currentObject ? currentObject.mobile : false}/>

                    <AvField name={'attachmentId'} type='hidden' className={""}
                             defaultValue={currentObject && currentObject.attachmentId ? currentObject.attachmentId : ''}/>
                    <div className={"fileUpdateGroup mx-auto mt-4"}>
                        <div className="file-input">
                            <input type="file" id="file" name={"attachmentId"} onChange={uploadImg} className="file"/>
                            <label htmlFor="file"><img src={uploadInputImg}/> &nbsp; Rasmni yuklang</label>
                            {this.state.image ?
                                currentObject && currentObject.attachment && currentObject.attachment.id ?
                                    <div className={"imgPosition text-right"}>
                                        <span onClick={() => deleteImg(currentObject.attachmentId,true)} className={"removeBtn"}>
                                            <DeleteButton/>
                                        </span>
                                        <img src={attachmentUrl + currentObject.attachment.id} className={"img-thumbnail"}/>
                                    </div> : ''
                                :
                                attachmentId ? <div className={"imgPosition text-right"}>
                                    <span onClick={() => deleteImg(0,false)} className={"removeBtn"}>
                                            <DeleteButton/>
                                        </span>
                                    <img src={attachmentUrl + attachmentId} className={"img-thumbnail"}/>
                                </div> : ""}
                        </div>
                    </div>
                    <div className={"mt-5"}>
                        <span>Mahsulot turi</span>
                        <Select
                            defaultValue={currentObject && currentObject.category ? {value:currentObject.category.id,label:currentObject.category.name} : ''}
                            placeholder={"Mahsulot turini tanlang"}
                            onChange={setCategory}
                            isSearchable={true}
                            options={productCategoriesSearch}
                            classNamePrefix={"select"}
                        />
                    </div>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>
            </div>
        );
    }
}

export default connect(
    ({
         app: {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
             productCategories, attachmentId, productCategoriesSearch, page,size,totalElements,totalPages,attachmentUrl},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        productCategories,
        attachmentId,
        productCategoriesSearch,
        attachmentUrl,
        page,size,totalElements,totalPages
    })
)(ProductCategory);
