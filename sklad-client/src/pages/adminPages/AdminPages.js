import React, {Component} from 'react';
import AdminLayout from "../../component/AdminLayout/AdminLayout";
import {connect} from "react-redux";

class AdminHome extends Component {
    state = {
        dropdown:false
    }
    render() {
        const {loading} = this.props

        const changeDropDown = () => {
            this.setState({dropdown : !this.state.dropdown})
        }
        const hideDropDown = () => {
            if (this.state.dropdown){
                changeDropDown()
            }
        }
        return (
            <div onClick={hideDropDown}>
                {/*<Loading />*/}
                <AdminLayout
                    page={this.props.match.params.page}
                    subPage={this.props.match.params.subPage}
                    dropdown={this.state.dropdown}
                    changeDropdown={changeDropDown}
                    onClick={hideDropDown}
                />
            </div>

        );
    }
}

export default connect(
    ({
         app: {loading},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        loading,
    })
)(AdminHome)
