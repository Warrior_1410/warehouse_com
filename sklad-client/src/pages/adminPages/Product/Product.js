import React, {Component} from 'react';
import {Row, Table, Input, Container, Col, Label} from "reactstrap";
import {connect} from "react-redux";
import {
    getWarehousesAction,
} from "../../../redux/actions/WarehouseAction";
import {
    getAllAction, getByNameAction,
    getByWarehouseAction,
} from "../../../redux/actions/ProductTemplateAction";
import Pagination from "react-js-pagination";

class Product extends Component {
    componentDidMount() {
        this.props.dispatch(getWarehousesAction());
        this.props.dispatch(getAllAction({page: 0, size: this.props.size}));
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getAllAction({page: (pageNumber - 1), size: this.props.size}))
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentId: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false,
            statusModal: false
        }
    }

    render() {
        const {warehouses, byWarehouse,page, size, totalElements, totalPages} = this.props;

        const getProducts = (e) => {
            if (e.target.value === 0){
                this.props.dispatch(getAllAction({page: 0, size: this.props.size}))
            }else {
                this.props.dispatch(getByWarehouseAction(e.target.value))
            }
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getAllAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(getByNameAction({name:e.target.value,page: 0, size: this.props.size}))
            }
        }

        return (
            <div>
                <Container className={"pt-5"}>
                    <Row>
                        <Col>
                                <Input type={"select"} onChange={getProducts}>
                                    <option selected value={0}>Barcha omborlar</option>
                                    {warehouses ? warehouses.map((item, i) =>
                                        <option value={item.id}>{item.name}</option>
                                    ) : ''}
                                </Input>
                        </Col>
                        <Col>
                            <Input placeholder={"Qidirish..."} onChange={search}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Table className={"mt-5"}>
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nomi</td>
                                    <td>Soni</td>
                                    <td>Brend</td>
                                    <td>Mahsulot turi</td>
                                    <td>O'lchov birligi</td>
                                </tr>
                                </thead>

                                <tbody>
                                {byWarehouse ? byWarehouse.map((item, i) =>
                                        <tr key={i}>
                                            <td>{item.kod}</td>
                                            <td>{item.name}</td>
                                            <td>{item.count}</td>
                                            <td>{item.brand ? item.brand.name : ''}</td>
                                            <td>{item.productCategory ? item.productCategory.name : ''}</td>
                                            <td>{item.measure ? item.measure.name : ''}</td>
                                        </tr>
                                ) : ''}
                                </tbody>
                            </Table>
                            <Pagination
                                activePage={page + 1}
                                itemsCountPerPage={size}
                                totalItemsCount={totalElements}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

Product.propTypes = {};

export default connect(
    ({
         app: {warehouses, byWarehouse,page, size, totalElements, totalPages},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        warehouses,
        byWarehouse,
        page, size, totalElements, totalPages
    })
)(Product);
