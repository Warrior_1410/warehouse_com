import React, {Component} from 'react';
import {Input, Button, Container, Row, Col, Table} from "reactstrap";
import {
    getAgentAction, getCountAction,
    getSupplierAction, getTransferAction, saveTransferAction, searchTransferAction,
} from "../../../../redux/actions/AppActions";
import {connect} from "react-redux";
import {v4 as uuidv4} from "uuid";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {getWarehousesAction} from "../../../../redux/actions/WarehouseAction";
import {getProductTemplatesAction} from "../../../../redux/actions/ProductTemplateAction";
import TransferProductList from "./TransferProductList";
import Pagination from "react-js-pagination";
import Select from "react-select";
import Header from "../../../../component/Header/Header";
import {AddButton, DeleteButton, EditButton, X} from "../../../../component/Icons";

class TransferProduct extends Component {
    componentDidMount() {
        this.props.dispatch(getWarehousesAction());
        this.props.dispatch(getSupplierAction());
        this.props.dispatch(getAgentAction());
        this.props.dispatch(getProductTemplatesAction())
        this.props.dispatch(getTransferAction({page: 0, size: this.props.size}))
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getTransferAction({page: (pageNumber - 1), size: this.props.size}))
    }

    constructor(props) {
        super(props);
        this.state = {
            responsible : '',
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false,
            openCreatePage: false,
            inputs: [
                {
                    id: uuidv4(),
                    productTemplateId: '',
                    allCount: '',
                    count: '',
                    price: '',
                    allPrice: '',
                    retailPrice: '',
                    status: false
                }
            ]
        }
    }

    render() {

        const {warehouses, agents, transfer, productTemplates,productTemplatesSearch, count,page,size,totalElements,totalPages} = this.props;

        const getCountByWarehouse = (e) => {
            this.props.dispatch(getCountAction(e.target.value))
        }

        const addTr = () => {
            let inputs = this.state.inputs
            inputs.push({id: uuidv4(), status: false})
            this.setState({inputs})
        }

        const saveTr = (id) => {
            let name = document.getElementById("name/" + id).innerText
            let pId = document.getElementById("templateId/" + id).value

            let allCount = document.getElementById("allCount/" + id).value
            let count = document.getElementById("count/" + id).value
            let allPrice = document.getElementById("allPrice/" + id).value
            let price = document.getElementById("price/" + id).value

            let index = this.state.inputs.findIndex(index => (
                index.id === id
            ))
            let inputs = [...this.state.inputs]
            let tr = this.state.inputs[index]

            tr.productTemplateName = name
            tr.productTemplateId = pId
            tr.allCount = allCount
            tr.count = count
            tr.allPrice = allPrice
            tr.price = price
            tr.status = true

            inputs[index] = tr

            this.setState({inputs})

            let itemAllSum = 0;
            this.state.inputs.map(item => item ? itemAllSum = itemAllSum + item.allPrice * 1 : '')
            document.getElementById("allA").value = itemAllSum
        }
        const editTr = (id) => {
            let index = this.state.inputs.findIndex(index => (
                index.id === id
            ))
            let inputs = [...this.state.inputs]
            let tr = this.state.inputs[index]
            tr.status = false
            inputs[index] = tr
            this.setState({inputs})
        }

        const deleteTr = (index) => {
            let inputs = this.state.inputs
            inputs.splice(index, 1)
            this.setState({inputs})
        }

        const calc = (id, e) => {
            let son = document.getElementById("count/" + id).value
            document.getElementById("allPrice/" + id).value = son * e.target.value
        }

        const getCount = (e, id) => {
            if (e.value === 0) {
                console.log(e.value)
            } else {

                let index = this.state.inputs.findIndex(index => (
                    index.id === id
                ))
                let inputs = [...this.state.inputs]
                let tr = this.state.inputs[index]

                count.map(item => item.productTemplateId === e.value ? tr.allCount = item.count : '')

                inputs[index] = tr
                this.setState({inputs})

                document.getElementById(`templateId/${id}`).value = e.value
            }
        }


        const saveForm = (e, v) => {
            v.transferProductDto = this.state.inputs
            let b = 0;
            v.transferProductDto.map(item => item ? b = b + item.allPrice * 1 : '')
            v.allAmount = b

            v.userId = this.state.responsible
            this.props.dispatch(saveTransferAction(v))
        }
        const openCreatePageFun = () => {
            this.setState({openCreatePage: !this.state.openCreatePage})
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getTransferAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(searchTransferAction(e.target.value))

            }
        }

        const getRole = (e,role) => {
            if(role === "responsible"){
                this.setState({responsible : e.value})
            }else {

            }
        }

        return (
            <div>
                {this.state.openCreatePage ?
                    <Container className={"pt-5"}>
                        <h3 className={"text-center"}>Transfer royhati qo'shish</h3>
                        <span onClick={openCreatePageFun} className={"deleteButton"}><X /></span>
                        <AvForm method={"POST"} onValidSubmit={saveForm}>
                            <Row>
                                <Col md={3}>
                                    <AvField type={"number"} label={"Hujjat nomeri:"} className={"firstInput"} name={"contractNumber"}
                                             placeholder="e.g. 5562235"/>
                                </Col>
                                <Col md={3}>
                                    <AvField label={"Sana:"} type={"datetime-local"} className={"secondInput"}
                                             name={"contractTime"}/>
                                </Col>
                                <Col md={6}>
                                    <span>Javobgar shaxs</span>
                                    <Select
                                        defaultValue={"0"}
                                        placeholder={"Javobgar shaxsni tanlang"}
                                        onChange={(e) => getRole(e,"responsible")}
                                        isSearchable={true}
                                        options={agents}
                                        classNamePrefix={"select"}
                                        className={"mt-2"}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6}>
                                    <AvField label={"Olingan ombor:"} type="select" name="fromWarehouseId"
                                             id="exampleSelect" onChange={getCountByWarehouse}>
                                        <option selected>Omborni tanlang</option>
                                        {warehouses ? warehouses.map((item, i) =>
                                            <option value={item.id}>{item.name}</option>
                                        ) : ""}
                                    </AvField>
                                </Col>
                            </Row>


                            <div className="col-2">
                                <div className={"viewButton"} onClick={addTr}>
                                    <AddButton />
                                </div>
                            </div>

                            <Table className={"inComeTable mt-5"}>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Nomi</th>
                                    <th>Max miqdor</th>
                                    <th>son</th>
                                    <th>narx</th>
                                    <th>Summa</th>
                                    <th>Amal</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.inputs.map((item, i) =>
                                    item.status ?
                                        <tr key={item.id}>
                                            <th scope="row">{i + 1}</th>
                                            <td><Input type={"text"} value={item.productTemplateName} disabled/></td>
                                            <td><Input type={"text"} value={item.allCount} disabled/></td>
                                            <td><Input type={"number"} value={item.count} disabled/></td>
                                            <td><Input type={"number"} value={item.retailPrice} disabled/></td>
                                            <td><Input type={"number"} value={item.allPrice} disabled/></td>
                                            <td>
                                                <span className={"editButton"} onClick={() => editTr(item.id)}>
                                                    <EditButton />
                                                </span>
                                                <span className={"deleteButton"} onClick={() => deleteTr(i)}>
                                                    <DeleteButton />
                                                </span>
                                            </td>
                                        </tr>
                                        :
                                        <tr key={item.id}>
                                            <th scope="row">{i + 1}</th>
                                            <td>
                                                <Select
                                                    id={`name/${item.id}`}
                                                    defaultValue={{label: item.productTemplateName, value: item.productTemplateId}}
                                                    selectedValue={{label: item.productTemplateName, value: item.productTemplateId}}
                                                    onChange={(e) => getCount(e, item.id)}
                                                    placeholder={"Mahsulot"}
                                                    isSearchable={true}
                                                    options={productTemplatesSearch}
                                                    classNamePrefix={"select"}
                                                    className={"product_input"}
                                                />
                                                <input id={`templateId/${item.id}`} type={"hidden"} />
                                            </td>
                                            <td><Input id={`allCount/${item.id}`} type={"number"}
                                                       defaultValue={item.allCount}
                                                       disabled/>
                                            </td>
                                            <td><Input id={`count/${item.id}`} type={"number"}
                                                       defaultValue={item.count}/>
                                            </td>
                                            <td><Input id={`price/${item.id}`} type={"number"}
                                                       defaultValue={item.retailPrice}
                                                       onChange={(e) => calc(item.id, e)}/></td>
                                            <td><Input id={`allPrice/${item.id}`} type={"number"}
                                                       defaultValue={item.allPrice}
                                                       disabled/></td>
                                            <td>
                                                <span className={"viewButton"} onClick={() => saveTr(item.id)}>
                                                    <AddButton />
                                                </span>
                                                <span className={"deleteButton"} onClick={() => deleteTr(i)}>
                                                    <DeleteButton />
                                                </span>
                                            </td>
                                        </tr>
                                )}
                                <tr>
                                    <td colSpan={5}>&nbsp;</td>
                                    <td><Input id={"allA"} placeholder={"Jami summa :"} disabled/></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </Table>
                            <hr/>
                            <Row>
                                <Col>
                                    <AvField label={"Qo'yiladigan ombor:"} type="select" name="toWarehouseId"
                                             id="exampleSelect">
                                        <option selected>Omborni tanlang</option>
                                        {warehouses ? warehouses.map((item, i) =>
                                            <option value={item.id}>{item.name}</option>
                                        ) : ""}
                                    </AvField>
                                </Col>
                                <Col className={"pt-5"}>
                                    <AvField type={"checkbox"} name={"accepted"} label={"Qabul qilindi"} />
                                </Col>
                                <Col className={"pt-3"}>
                                    <Button className={"btnSave"} type={"submit"}>
                                        Saqlash
                                    </Button>
                                </Col>
                            </Row>
                        </AvForm>
                    </Container>
                    :
                    <Container>
                        <Header
                            title={"Ko'chirishlar ro'yhati"}
                            addModal={openCreatePageFun}
                            search={search}
                            calendar={""}
                        />
                        <TransferProductList/>
                        <Pagination
                            activePage={page + 1}
                            itemsCountPerPage={size}
                            totalItemsCount={totalElements}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                            linkClass="page-link"
                        />
                    </Container>
                }

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             showModal, suppliers, agents, openModal, warehouses, currentItem, count,productTemplatesSearch,
             editModal, deleteModal, productTemplates, currentObject, transfer,page,size,totalElements,totalPages
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        warehouses,
        currentObject,
        currentItem,
        suppliers,
        agents,
        productTemplates,
        count,
        productTemplatesSearch,
        transfer,page,size,totalElements,totalPages
    })
)(TransferProduct);
