import React, {Component} from 'react';
import {
    Button,
    Table,
} from "reactstrap";
import {connect} from "react-redux";
import {
    getTransferAction
} from "../../../../redux/actions/AppActions";
import "../Product.css";
import {Link} from "react-router-dom";
import Switch from "react-switch";
import {EyeButton} from "../../../../component/Icons";

class TransferProductList extends Component {
    componentDidMount() {
        this.props.dispatch(getTransferAction())
        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {

        let {transfer} = this.props;

        return (
            <Table>
                <thead>
                <tr>
                    <td>#</td>
                    <td>Shartnoma raqami</td>
                    <td>Olingan ombor</td>
                    <td>Qo'yilgan ombor</td>
                    <td>Javobgar shaxs</td>
                    <td>Summa</td>
                    <td>Qabul</td>
                    <td>Amal</td>
                </tr>
                </thead>
                <tbody>
                {transfer ? transfer.map((item, i) =>
                    <tr key={i}>
                        <td>{i+1}</td>
                        <td>{item.contractNumber}</td>
                        <td>{item.fromWarehouse.name}</td>
                        <td>{item.toWarehouse.name}</td>
                        <td>{item.user.fullName}</td>
                        <td>{item.allAmount}</td>
                        <td>
                            <Switch color={'red'}  className={"switch"} checked={item.accepted} disabled />
                        </td>
                        <td>
                            <Link to={"/admin/transfer/" + item.contractNumber}>
                                <div className={"viewButton"}>
                                    <EyeButton />
                                </div>
                            </Link>
                        </td>
                    </tr>
                ) : ''}
                </tbody>
            </Table>

        );
    }
}


export default connect(
    ({
         app: {
             openModal, currentItem, editModal, deleteModal, transfer
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        currentItem,
        transfer,
    })
)(TransferProductList);
