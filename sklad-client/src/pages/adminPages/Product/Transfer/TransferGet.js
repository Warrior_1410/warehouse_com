import React, {Component} from 'react';
import {
    Row,
    Table,
    Container,
    Col,
} from "reactstrap";
import {connect} from "react-redux";
import {
    getTransferGetAction
} from "../../../../redux/actions/AppActions";
import "../Product.css";
import {Link} from "react-router-dom";
import moment from "moment";
import {X} from "../../../../component/Icons";

class TransferGet extends Component {
    componentDidMount() {
        this.props.dispatch(getTransferGetAction(this.props.id))
        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {

        const {transferOne} = this.props;
        console.log(transferOne)
        return (
            <Container className={"pt-5"}>
                <Row>
                    <Col>
                        <Row>
                            <Col md={11}>
                                <h4>Batafsil</h4>
                            </Col>
                            <Col md={1} className={"text-right"}>
                                <Link to={"/admin/transfer"} className={"deleteButton"}>
                                    <X />
                                </Link>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            <Col>
                                <span>Shartnoma raqami</span>
                                <h4 className={"form-control"}>{transferOne.contractNumber}</h4>
                                <br />
                                <span>Shartnoma vaqti</span>
                                <h4 className={"form-control"}>{moment(transferOne.contractTime).format('DD-MM-YYYY, HH:mm')}</h4>
                                <br/>
                                <Row>
                                    <Col>
                                        <span>Jami summa</span>
                                        <h4 className={"form-control"}>{transferOne.allAmount}</h4>
                                    </Col>
                                    <Col className={"pt-4"}>
                                        <input type={"checkbox"} className={"custom-chechbox"} checked={transferOne ? transferOne.accepted : false} readOnly />
                                        <span> Qabul qilingan</span>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <span>Javobgar shaxs</span>
                                <h4 className={"form-control"}>{transferOne.user ? transferOne.user.fullName : ''}</h4>
                                <br />
                                <span>Olingan ombor</span>
                                <h4 className={"form-control"}>{transferOne.fromWarehouse ? transferOne.fromWarehouse.name : ''}</h4>
                                <br />
                                <span>Qoyilgan Ombor</span>
                                <h4 className={"form-control"}>{transferOne.toWarehouse ? transferOne.toWarehouse.name : ''}</h4>
                            </Col>
                        </Row>
                        <br />
                        <Table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nomi</th>
                                <th>Soni</th>
                                <th>Qoldiq</th>
                                <th>Narx</th>
                                <th>Bozor narx</th>
                            </tr>
                            </thead>
                            <tbody>
                            {transferOne.transferProducts ? transferOne.transferProducts.map((item, i) =>
                                <tr>
                                    <td>{i + 1}</td>
                                    <td>{item.productTemplate ? item.productTemplate.name : ''}</td>
                                    <td>{item.count}</td>
                                    <td>{item.leftover}</td>
                                    <td>{item.price}</td>
                                    <td>{item.retailPrice}</td>
                                </tr>
                            ) : ''}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>

        );
    }
}

TransferGet.propTypes = {};

export default connect(
    ({
         app: {
             openModal, currentItem, editModal, deleteModal, transferOne
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        currentItem,
        transferOne
    })
)(TransferGet);
