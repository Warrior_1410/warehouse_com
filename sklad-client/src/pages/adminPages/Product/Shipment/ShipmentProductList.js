import React, {Component} from 'react';
import {Table} from "reactstrap";
import {connect} from "react-redux";
import { getShipmentAction
} from "../../../../redux/actions/AppActions";
import "../Product.css";
import {Link} from "react-router-dom";
import {EyeButton} from "../../../../component/Icons";

class ShipmentProductList extends Component {
    componentDidMount() {
        this.props.dispatch(getShipmentAction())
        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {

            let {shipment} = this.props;

        return (
            <Table>
                <thead>
                <tr>
                    <td>#</td>
                    <td>Shartnoma raqami</td>
                    <td>Ombor</td>
                    {/*<td>Receiver</td>*/}
                    {/*<td>Responsible</td>*/}
                    {/*<td>Supplier</td>*/}
                    {/*<td>P Amount</td>*/}
                    {/*<td>P Total</td>*/}
                    {/*<td>P Sum Per</td>*/}
                    {/*<td>Currency</td>*/}
                    <td>Jami Summa</td>
                    <td>Izoh</td>
                    <td>Amal</td>
                </tr>
                </thead>
                <tbody>
                {shipment ? shipment.map((item, i) =>
                    <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{item.contractNumber}</td>
                        <td>{item.warehouse.name}</td>
                        {/*<td>{item.receiverUser.fullName}</td>*/}
                        {/*<td>{item.responsibleUser.fullName}</td>*/}
                        {/*<td>{item.supplierUser.fullName}</td>*/}
                        <td>{item.allSum}</td>
                        <td>{item.description}</td>
                        <td>
                            <Link to={"/admin/shipment/" + item.contractNumber}>
                                <div className={"viewButton"}>
                                    <EyeButton />
                                </div>
                            </Link>
                        </td>
                    </tr>) : ''}
                </tbody>
            </Table>
        );
    }
}


export default connect(
    ({
         app: {
             openModal, currentItem, editModal, deleteModal, shipment
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        currentItem,
        shipment,
    })
)(ShipmentProductList);
