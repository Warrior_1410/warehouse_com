import React, {Component} from 'react';
import {
    Row,
    Button,
    Table,
    Input,
    Container,
    Col,
} from "reactstrap";
import {connect} from "react-redux";
import {
    getSupplierAction,
    getAgentAction, getCountAction, saveShipmentAction, getShipmentAction, searchShipmentAction
} from "../../../../redux/actions/AppActions";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import "../Product.css";
import {v4 as uuidv4} from 'uuid';
import {getWarehousesAction} from "../../../../redux/actions/WarehouseAction";
import {getProductTemplatesAction} from "../../../../redux/actions/ProductTemplateAction";
import {toast} from "react-toastify";
import ShipmentProductList from "./ShipmentProductList";
import Pagination from "react-js-pagination";
import Select from "react-select";
import Header from "../../../../component/Header/Header";
import {AddButton, DeleteButton, EditButton, X} from "../../../../component/Icons";

class ShipmentProduct extends Component {
    componentDidMount() {
        this.props.dispatch(getWarehousesAction());
        this.props.dispatch(getSupplierAction());
        this.props.dispatch(getAgentAction());
        this.props.dispatch(getProductTemplatesAction());
        this.props.dispatch(getShipmentAction({page: 0, size: this.props.size}))
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getShipmentAction({page: (pageNumber - 1), size: this.props.size}))
    }

    constructor(props) {
        super(props);
        this.state = {
            supplier : '',
            responsible : '',
            receiver : '',
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false,
            openViewPage: false,
            inputs: [
                {
                    id: uuidv4(),
                    productTemplateId: '',
                    allCount: '',
                    count: '',
                    retailPrice: '',
                    allPrice: '',
                    status: false
                }
            ]
        }
    }

    render() {

        const {warehouses, suppliers, agents, productTemplates, shipment,productTemplatesSearch,
            count,page,size,totalElements,totalPages} = this.props;

        const getCountByWarehouse = (e) => {
            this.props.dispatch(getCountAction(e.target.value))
        }

        const addTr = () => {
            let inputs = this.state.inputs
            inputs.push({id: uuidv4(), status: false})
            this.setState({inputs})
        }

        const saveTr = (id) => {

            let name = document.getElementById("name/" + id).innerText
            let pId = document.getElementById("templateId/" + id).value

            let allCount = document.getElementById("allCount/" + id).value * 1
            let count = document.getElementById("count/" + id).value * 1
            let allPrice = document.getElementById("allPrice/" + id).value
            let retailPrice = document.getElementById("retailPrice/" + id).value

            // console.log("allCount : " + allCount)
            // console.log("count : " + count)


            if (allCount === count || allCount > count) {
                let index = this.state.inputs.findIndex(index => (
                    index.id === id
                ))
                let inputs = [...this.state.inputs]
                let tr = this.state.inputs[index]

                tr.productTemplateName = name
                tr.productTemplateId = pId
                tr.allCount = allCount
                tr.count = count
                tr.allPrice = allPrice
                tr.retailPrice = retailPrice
                tr.status = true

                inputs[index] = tr

                this.setState({inputs})

                let itemAllSum = 0;
                this.state.inputs.map(item => item ? itemAllSum = itemAllSum + item.allPrice * 1 : '')
                document.getElementById("allA").value = itemAllSum
            } else {
                toast.error("Maksimal miqdor togri emas")
            }
        }
        const editTr = (id) => {
            let index = this.state.inputs.findIndex(index => (
                index.id === id
            ))
            let inputs = [...this.state.inputs]
            let tr = this.state.inputs[index]
            tr.status = false
            inputs[index] = tr
            this.setState({inputs})
        }

        const deleteTr = (index) => {
            let inputs = this.state.inputs
            inputs.splice(index, 1)
            this.setState({inputs})
        }

        const calc = (id, e) => {
            let son = document.getElementById("count/" + id).value
            document.getElementById("allPrice/" + id).value = son * e.target.value
        }

        const getCount = (e, id) => {
            if (e.value === 0) {
                console.log(e.value)
            } else {

                let index = this.state.inputs.findIndex(index => (
                    index.id === id
                ))
                let inputs = [...this.state.inputs]
                let tr = this.state.inputs[index]

                count.map(item => item.productTemplateId === e.value ? tr.allCount = item.count : '')

                inputs[index] = tr
                this.setState({inputs})

                document.getElementById(`templateId/${id}`).value = e.value
            }
        }

        const saveForm = (e, v) => {
            v.orderProductDto = this.state.inputs
            let b = 0;
            v.orderProductDto.map(item => item ? b = b + item.allPrice * 1 : '')
            v.allSum = b
            v.paymentTotal = v.allSum
            v.paymentSumPercent = v.paymentAmount / (b / 100)

            v.responsibleUserId = this.state.responsible
            v.receiverUserId = this.state.receiver
            v.supplierUserId = this.state.supplier

            this.props.dispatch(saveShipmentAction(v))
        }


        const calcl = (e) => {
            let b = 0;
            this.state.inputs.map(item => item ? b = b + item.allPrice * 1 : '')

            if (b === 0) {
                toast.error("Hali malumot kiritilmagan!")
            }

            let percent = (e.target.value / (b / 100)).toString()
            if (percent.indexOf('.') > 0) {
                percent = percent.substring(percent.indexOf(0), percent.indexOf('.') + 2);
            }
            document.getElementById("percent").value = percent + " %"
        }
        const openViewPageFun = () => {
            this.setState({openViewPage: !this.state.openViewPage})
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getShipmentAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(searchShipmentAction(e.target.value))

            }
        }
        const getRole = (e,role) => {
            if (role === "supplier"){
                this.setState({supplier : e.value})
            } else if (role === "receiver"){
                this.setState({receiver : e.value})
            } else if(role === "responsible"){
                this.setState({responsible : e.value})
            }else {

            }
        }


        return (
            <div>
                <div>
                    {this.state.openViewPage ?
                        <Container className={"pt-3"}>
                            <h3 className={"text-center"}>Chiqim qo'shish</h3>
                            <span onClick={openViewPageFun} className={"deleteButton"}><X /></span>
                            <AvForm method={"POST"} onValidSubmit={saveForm}>
                                <Row className={'mt-5'}>
                                    <Col md={3}>
                                        <AvField type={"number"} label={"Hujjat nomeri:"} className={"firstInput"}
                                                 name={"contractNumber"}
                                                 placeholder="e.g. 5562235"/>
                                    </Col>
                                    <Col md={3}>
                                        <AvField label={"Sana:"} type={"datetime-local"} className={"secondInput"}
                                                 name={"contractTime"}/>
                                    </Col>
                                    <Col md={6}>
                                        <AvField label={"Olingan ombor:"} type="select" name="warehouseId"
                                                 id="exampleSelect" onChange={getCountByWarehouse}>
                                            <option selected>Omborni tanlang</option>
                                            {warehouses ? warehouses.map((item, i) =>
                                                <option value={item.id}>{item.name}</option>
                                            ) : ""}
                                        </AvField>
                                    </Col>
                                </Row>

                                <div className="col-2">
                                    <div className={"viewButton"} onClick={addTr}>
                                        <AddButton />
                                    </div>
                                </div>

                                <Table className={"inComeTable mt-5"}>
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Nomi</th>
                                        <th>Max miqdor</th>
                                        <th>son</th>
                                        <th>narx</th>
                                        <th>Summa</th>
                                        <th>Amal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.inputs.map((item, i) =>
                                        item.status ?
                                            <tr key={item.id}>
                                                <th scope="row">{i + 1}</th>
                                                <td><Input type={"text"} value={item.productTemplateName} disabled/>
                                                </td>
                                                <td><Input type={"text"} value={item.allCount} disabled/></td>
                                                <td><Input type={"number"} value={item.count} disabled/></td>
                                                <td><Input type={"number"} value={item.retailPrice} disabled/></td>
                                                <td><Input type={"number"} value={item.allPrice} disabled/></td>
                                                <td>
                                                    <span className={"editButton"} onClick={() => editTr(item.id)}>
                                                        <EditButton />
                                                    </span>
                                                    <span className={"deleteButton"} onClick={() => deleteTr(i)}>
                                                        <DeleteButton />
                                                    </span>
                                                </td>
                                            </tr>
                                            :
                                            <tr key={item.id}>
                                                <th scope="row">{i + 1}</th>
                                                <td>
                                                    <Select
                                                        id={`name/${item.id}`}
                                                        defaultValue={{label: item.productTemplateName, value: item.productTemplateId}}
                                                        selectedValue={{label: item.productTemplateName, value: item.productTemplateId}}
                                                        onChange={(e) => getCount(e, item.id)}
                                                        placeholder={"Mahsulot"}
                                                        isSearchable={true}
                                                        options={productTemplatesSearch}
                                                        classNamePrefix={"select"}
                                                        className={"product_input"}
                                                    />
                                                    <input id={`templateId/${item.id}`} type={"hidden"}/>
                                                </td>
                                                <td><Input id={`allCount/${item.id}`} type={"number"}
                                                           defaultValue={item.allCount}
                                                           disabled/>
                                                </td>
                                                <td><Input id={`count/${item.id}`} max={item.allCount} type={"number"}
                                                           defaultValue={item.count}/></td>
                                                <td><Input id={`retailPrice/${item.id}`} type={"number"}
                                                           defaultValue={item.retailPrice}
                                                           onChange={(e) => calc(item.id, e)}/></td>
                                                <td><Input id={`allPrice/${item.id}`} type={"number"}
                                                           defaultValue={item.allPrice}
                                                           disabled/></td>
                                                <td>
                                                    <span className={"viewButton"} onClick={() => saveTr(item.id)}>
                                                    <AddButton />
                                                </span>
                                                    <span className={"deleteButton"} onClick={() => deleteTr(i)}>
                                                    <DeleteButton />
                                                </span>
                                                </td>
                                            </tr>
                                    )}
                                    <tr>
                                        <td colSpan={2}>
                                            <AvField type="number" name="currency" placeholder={"Valyuta kursi"}/>
                                        </td>
                                        <td colSpan={2}>
                                            <AvField type="number" name="paymentAmount" placeholder={"To'langan summa"} onChange={calcl}/>
                                        </td>
                                        <td>
                                            <Input id={"percent"} placeholder={"... %"} disabled/>
                                        </td>
                                        <td><Input id={"allA"} disabled placeholder={"Jami summa : "}/></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </Table>
                                <Row className={"mt-4"}>
                                    <Col md={4}>
                                        <span>Yetkazib beruvchi shaxs</span>
                                        <Select
                                            defaultValue={"0"}
                                            placeholder={"Yetkazib beruvchi shaxsni tanlang"}
                                            onChange={(e) => getRole(e,"supplier")}
                                            isSearchable={true}
                                            options={suppliers}
                                            classNamePrefix={"select"}
                                        />
                                    </Col>
                                    <Col md={4}>
                                        <span>Javobgar shaxs</span>
                                        <Select
                                            defaultValue={"0"}
                                            placeholder={"Javobgar shaxsni tanlang"}
                                            onChange={(e) => getRole(e,"responsible")}
                                            isSearchable={true}
                                            options={agents}
                                            classNamePrefix={"select"}
                                        />
                                    </Col>
                                    <Col md={4}>
                                        <span>Qabul qiluvchi shaxs</span>
                                        <Select
                                            defaultValue={"0"}
                                            placeholder={"Qabul qilivchi shaxsni tanlang"}
                                            onChange={(e) => getRole(e,"receiver")}
                                            isSearchable={true}
                                            options={agents}
                                            classNamePrefix={"select"}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <Button className={"btnSave mt-3"} type={"submit"}>
                                            Saqlash
                                        </Button>
                                    </Col> </Row>
                            </AvForm>
                        </Container>
                        :
                        <Container>
                            <Header
                                title={"Chiqim ro'yhati"}
                                addModal={openViewPageFun}
                                search={search}
                                calendar={""}
                            />
                            <ShipmentProductList />
                            <Pagination
                                activePage={page + 1}
                                itemsCountPerPage={size}
                                totalItemsCount={totalElements}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Container>
                    }
                </div>
            </div>
        );
    }
}

ShipmentProduct.propTypes = {};

export default connect(
    ({
         app: {
             showModal, suppliers, agents, openModal, warehouses, currentItem, count,productTemplatesSearch,
             editModal, deleteModal, productTemplates, currentObject, shipment,page,size,totalElements,totalPages
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        warehouses,
        currentObject,
        currentItem,
        suppliers,
        agents,
        productTemplates,
        count,
        productTemplatesSearch,
        shipment,page,size,totalElements,totalPages
    })
)(ShipmentProduct);
