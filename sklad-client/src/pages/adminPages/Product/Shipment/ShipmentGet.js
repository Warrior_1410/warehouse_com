import React, {Component} from 'react';
import {
    Row,
    Table,
    Container,
    Col,
} from "reactstrap";
import {connect} from "react-redux";
import {
    getShipmentGetAction
} from "../../../../redux/actions/AppActions";
import "../Product.css";
import {Link} from "react-router-dom";
import moment from "moment";
import {X} from "../../../../component/Icons";

class ShipmentGet extends Component {
    componentDidMount() {
        this.props.dispatch(getShipmentGetAction(this.props.id))
        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {

        const {shipmentOne} = this.props;
        console.log(shipmentOne)

        const get = (number) => {
            if (number){
                let p = number.toString()

                if (p.indexOf('.') > 0) {
                    p = p.substring(p.indexOf(0), p.indexOf('.') + 2);
                }
                return p
            }
        }
        return (
            <Container className={"pt-5"}>
                <Row>
                    <Col>
                        <Row>
                            <Col md={11}>
                                <h4>Batafsil</h4>
                            </Col>
                            <Col md={1} className={"text-right"}>
                                <Link to={"/admin/shipment"} className={"deleteButton"}>
                                    <X />
                                </Link>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            <Col>
                                <Row>
                                    <Col>
                                        <span>Shartnoma raqami</span>
                                        <h4 className={"form-control"}>{shipmentOne.contractNumber}</h4>
                                    </Col>
                                    <Col>
                                        <span>Olingan ombor</span>
                                        <h4 className={"form-control"}>{shipmentOne.warehouse ? shipmentOne.warehouse.name : ''}</h4>
                                    </Col>
                                </Row>
                                <br />
                                <span>Shartnoma vaqti</span>
                                <h4 className={"form-control"}>{moment(shipmentOne.contractTime).format('DD-MM-YYYY, HH:mm')}</h4>
                                <br />
                                <Row>
                                    <Col>
                                        <span>Valyuta</span>
                                        <h4 className={"form-control"}>{shipmentOne.currency}</h4>
                                    </Col>
                                    <Col>
                                        <span>Jami summa</span>
                                        <h4 className={"form-control"}>{shipmentOne.allSum}</h4>
                                    </Col>
                                </Row>
                                <br />
                                <Row>
                                    <Col>
                                        <span>To'langan summa</span>
                                        <h4 className={"form-control"}>{shipmentOne.paymentAmount}</h4>
                                    </Col>
                                    <Col>
                                        <span>To'langan summa %</span>
                                        <h4 className={"form-control"}>{get(shipmentOne.paymentSumPercent) + " %"}</h4>
                                    </Col>
                                    <Col>
                                        <span>To'lanuvchi summa</span>
                                        <h4 className={"form-control"}>{shipmentOne.paymentTotal}</h4>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                    <span>Javobgar shaxs</span>
                                    <h4 className={"form-control"}>{shipmentOne.responsibleUser ? shipmentOne.responsibleUser.fullName : ''}</h4>
                                    <br />
                                    <span>Yetkazib beruvchi shaxs</span>
                                    <h4 className={"form-control"}>{shipmentOne.supplierUser ? shipmentOne.supplierUser.fullName : ''}</h4>
                                    <br />
                                    <span>Qabul qiluvchi shaxs</span>
                                    <h4 className={"form-control"}>{shipmentOne.receiverUser ? shipmentOne.receiverUser.fullName : ''}</h4>

                            </Col>
                        </Row>
                        <br />
                        <Table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nomi</th>
                                <th>soni</th>
                                <th>Bozor narx</th>
                                <th>Jami narx</th>
                                <th>Izoh</th>
                            </tr>
                            </thead>
                            <tbody>
                            {shipmentOne.products ? shipmentOne.products.map((item, i) =>
                                <tr>
                                    <td>{i + 1}</td>
                                    <td>{item.product ? item.product[0].productTemplate.name : ''}</td>
                                    <td>{item.count}</td>
                                    <td>{item.retailPrice}</td>
                                    <td>{item.allPrice}</td>
                                    <td>{item.description}</td>
                                </tr>
                            ) : ''}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>

        );
    }
}

ShipmentGet.propTypes = {};

export default connect(
    ({
         app: {
             openModal, currentItem, editModal, deleteModal, shipmentOne
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        currentItem,
        shipmentOne
    })
)(ShipmentGet);
