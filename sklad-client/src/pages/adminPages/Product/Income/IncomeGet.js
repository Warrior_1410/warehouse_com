import React, {Component} from 'react';
import {
    Row,
    Button,
    Table,
    Container,
    Col,
} from "reactstrap";
import {connect} from "react-redux";
import {getIncomeGetAction} from "../../../../redux/actions/AppActions";
import "../Product.css";
import {Link} from "react-router-dom";
import moment from "moment";
import {X} from "../../../../component/Icons";

class IncomeGet extends Component {
    componentDidMount() {
        this.props.dispatch(getIncomeGetAction(this.props.id))
        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {

        const {incomeOne} = this.props;

        console.log(incomeOne)
        console.log(moment(incomeOne.contractTime).format('DD-MM-YYYY') === '13-02-2021')

        return (
            <Container className={"content pt-4 mr-5"}>
                <Row>
                    <Col>
                        <Row>
                            <Col md={11}>
                                <h4>Batafsil</h4>
                            </Col>
                            <Col md={1} className={"text-right"}>
                                <Link to={"/admin/income"} className={"deleteButton"}>
                                    <X />
                                </Link>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            <Col>
                                <span>Shartnoma raqami</span>
                                <h4 className={"form-control"}>{incomeOne.contractNumber}</h4>
                                <br/>
                                <span>Shartnoma vaqti</span>
                                <h4 className={"form-control"}>{moment(incomeOne.contractTime).format('DD-MM-YYYY, HH:mm')}</h4>
                                <br/>
                                <Row>
                                    <Col>
                                        <span>Ombor</span>
                                        <h4 className={"form-control"}>{incomeOne.warehouse ? incomeOne.warehouse.name : ''}</h4>
                                    </Col>
                                    <Col>
                                        <span>Jami summa</span>
                                        <h4 className={"form-control"}>{incomeOne.amount}</h4>
                                    </Col>
                                </Row>
                            </Col>

                            <Col>
                                <span>Javobgar shaxs</span>
                                <h4 className={"form-control"}>{incomeOne.responsibleUser ? incomeOne.responsibleUser.fullName : ''}</h4>
                                <br/>
                                <span>Yetkazib beruvchi shaxs</span>
                                <h4 className={"form-control"}>{incomeOne.supplierUser ? incomeOne.supplierUser.fullName : ''}</h4>
                                <br/>
                                <span>Qabul qiluvchi shaxs</span>
                                <h4 className={"form-control"}>{incomeOne.receiverUser ? incomeOne.receiverUser.fullName : ''}</h4>
                            </Col>
                        </Row>
                        <br />

                        <Table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nomi</th>
                                <th>Soni</th>
                                <th>Qoldiq</th>
                                <th>Narx</th>
                                <th>Bozor narx</th>
                                <th>Ombor</th>
                                <th>Izoh</th>
                            </tr>
                            </thead>
                            <tbody>
                            {incomeOne.products ? incomeOne.products.map((item, i) =>
                                <tr>
                                    <td>{i + 1}</td>
                                    <td>{item.productTemplate.name}</td>
                                    <td>{item.count}</td>
                                    <td>{item.leftover === 0 ?
                                        <span className={"text-danger"}>{item.leftover}</span> : item.leftover}</td>
                                    <td>{item.price}</td>
                                    <td>{item.retailPrice}</td>
                                    <td>{item.warehouse ? item.warehouse.name : ''}</td>
                                    <td>{item.description}</td>
                                </tr>
                            ) : ''}
                            </tbody>
                        </Table>
                    </Col>
                </Row>

            </Container>

        );
    }
}

IncomeGet.propTypes = {};

export default connect(
    ({
         app: {
             openModal, currentItem, editModal, deleteModal, incomeOne
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        currentItem,
        incomeOne
    })
)(IncomeGet);
