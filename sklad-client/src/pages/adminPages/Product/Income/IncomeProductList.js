import React, {Component} from 'react';
import {
    Button,
    Table,
} from "reactstrap";
import {connect} from "react-redux";
import { getIncomeAction
} from "../../../../redux/actions/AppActions";
import "../Product.css";
import {Link} from "react-router-dom";
import {EyeButton} from "../../../../component/Icons";

class IncomeProductList extends Component {
    componentDidMount() {
        this.props.dispatch(getIncomeAction())
        console.clear()
    }

    constructor(props) {
        super(props);
        this.state = {
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false
        }
    }

    render() {

        let {income} = this.props;

        return (
            <Table>
                <thead>
                <tr>
                    <td>#</td>
                    <td>Shartnoma raqami</td>
                    <td>Ombor</td>
                    <td>Izoh</td>
                    <td>Amal</td>
                    {/*<td>Receiver</td>*/}
                    {/*<td>Responsible</td>*/}
                    {/*<td>Supplier</td>*/}
                    {/*<td>Status</td>*/}
                </tr>
                </thead>
                <tbody>
                {income ? income.map((item, i) =>
                    <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{item.contractNumber}</td>
                        <td>{item.warehouse.name}</td>
                        <td>{item.description}</td>
                        {/*<td>{item.receiverUser.fullName}</td>*/}
                        {/*<td>{item.responsibleUser.fullName}</td>*/}
                        {/*<td>{item.supplierUser.fullName}</td>*/}
                        {/*<td>{item.statusEnum}</td>*/}
                        <td>
                            <Link to={"/admin/income/" + item.contractNumber}>
                                <div className={"viewButton"}>
                                    <EyeButton />
                                </div>
                            </Link>
                        </td>
                    </tr>
                ) : ''}
                </tbody>
            </Table>
        );
    }
}

IncomeProductList.propTypes = {};

export default connect(
    ({
         app: {
             openModal, currentItem, editModal, deleteModal, income
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        currentItem,
        income,
    })
)(IncomeProductList);
