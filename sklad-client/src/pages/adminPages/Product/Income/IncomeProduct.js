import React, {Component} from 'react';
import {
    Row,
    Button,
    Table,
    Input,
    Container,
    Col, Label,
} from "reactstrap";
import {connect} from "react-redux";
import {
    getSupplierAction,
    getAgentAction,
    getPriceAction,
    saveIncomeAction,
    getIncomeAction,
    getIncomeGetAction,
    searchIncomeAction,
} from "../../../../redux/actions/AppActions";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import "../Product.css";
import {v4 as uuidv4} from 'uuid';
import {getWarehousesAction,} from "../../../../redux/actions/WarehouseAction";
import {getProductTemplatesAction} from "../../../../redux/actions/ProductTemplateAction";
import IncomeProductList from "./IncomeProductList";
import Pagination from "react-js-pagination";
import Select from "react-select";
import Header from "../../../../component/Header/Header";
import {AddButton, DeleteButton, EditButton, X} from "../../../../component/Icons";

class IncomeProduct extends Component {
    componentDidMount() {
        this.props.dispatch(getWarehousesAction());
        this.props.dispatch(getSupplierAction());
        this.props.dispatch(getIncomeAction({page: 0, size: this.props.size}));
        this.props.dispatch(getAgentAction());
        this.props.dispatch(getProductTemplatesAction())
        this.props.dispatch(getPriceAction())
        this.props.dispatch(getIncomeGetAction())
        console.clear()
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getIncomeAction({page: (pageNumber - 1), size: this.props.size}))
    }


    constructor(props) {
        super(props);
        this.state = {
            supplier : '',
            responsible : '',
            receiver : '',
            currentItem: '',
            currentObject: {},
            openModal: false,
            editModal: false,
            deleteModal: false,
            openViewPage: true,
            openViewDetail: false,
            inputs: [
                {
                    id: uuidv4(),
                    productTemplateId: '',
                    oldPrice: '',
                    count: 0,
                    price: 0,
                    allPrice: 0,
                    retailPrice: 0,
                    status: false
                }
            ]
        }
    }

    render() {

        const {warehouses, suppliers, agents, productTemplates, price, income,productTemplatesSearch,page,size,totalElements,totalPages} = this.props;
        const {currentItem, openModal, editModal, deleteModal, currentObject} = this.state;
        const addTr = () => {
            let inputs = this.state.inputs
            inputs.push({id: uuidv4(), status: false})
            this.setState({inputs})
        }

        const saveTr = (id) => {

            let name = document.getElementById("name/" + id).innerText
            let pId = document.getElementById("templateId/" + id).value


            let oldPrice = document.getElementById("oldPrice/" + id).value
            let count = document.getElementById("count/" + id).value
            let price = document.getElementById("price/" + id).value
            let allSum = document.getElementById("allSum/" + id).value
            let retailPrice = document.getElementById("retailPrice/" + id).value

            let index = this.state.inputs.findIndex(index => (
                index.id === id
            ))
            let inputs = [...this.state.inputs]
            let tr = this.state.inputs[index]

            tr.productTemplateName = name
            tr.productTemplateId = pId
            tr.oldPrice = oldPrice
            tr.count = count
            tr.price = price
            tr.allPrice = allSum
            tr.retailPrice = retailPrice
            tr.status = true

            inputs[index] = tr

            this.setState({inputs})

            let itemAllSum = 0;
            this.state.inputs.map(item => item ? itemAllSum = itemAllSum + item.allPrice * 1 : '')
            document.getElementById("allA").value = itemAllSum
        }
        const editTr = (id) => {
            let index = this.state.inputs.findIndex(index => (
                index.id === id
            ))
            let inputs = [...this.state.inputs]
            let tr = this.state.inputs[index]
            tr.status = false
            inputs[index] = tr
            this.setState({inputs})
        }
        const deleteTr = (index) => {
            let inputs = this.state.inputs
            inputs.splice(index, 1)
            this.setState({inputs})
        }
        const calc = (id, e) => {
            let son = document.getElementById("count/" + id).value
            document.getElementById("allSum/" + id).value = son * e.target.value
        }
        const getPrice = (e, id) => {
            if (e.value === 0) {
                console.log(e.value)
            } else {

                let index = this.state.inputs.findIndex(index => (
                    index.id === id
                ))
                let inputs = [...this.state.inputs]
                let tr = this.state.inputs[index]

                price.map(item => item.productTemplateId === e.value ? tr.oldPrice = item.price : '')
                inputs[index] = tr
                this.setState({inputs})

                document.getElementById(`templateId/${id}`).value = e.value
            }
        }
        const saveForm = (e, v) => {
            console.log(this.state.inputs)

            v.responsibleUserId = this.state.responsible
            v.receiverUserId = this.state.receiver
            v.supplierUserId = this.state.supplier

            v.productDtos = this.state.inputs
            let b = 0;
            v.productDtos.map(item => item ? b = b + item.allPrice * 1 : '')
            v.amount = b
            this.props.dispatch(saveIncomeAction(v))
        }
        const openViewPageFun = () => {
            this.setState({openViewPage: !this.state.openViewPage})
        }

        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getIncomeAction({page: 0, size: this.props.size}))
            } else {
                this.props.dispatch(searchIncomeAction(e.target.value))

            }
        }

        const getRole = (e,role) => {
            if (role === "supplier"){
                this.setState({supplier : e.value})
            } else if (role === "receiver"){
                this.setState({receiver : e.value})
            } else if(role === "responsible"){
                this.setState({responsible : e.value})
            }else {

            }
        }

        const getDate = (e) => {
            console.log(e.target.value)
            let formatted = e.target.value;
            // this.props.dispatch(getIncomeByDateAction({page: 0, size: this.props.size, filterDate: formatted}));
        }


        return (
            <div>
                {this.state.openViewPage ?
                    <Container>
                        <Header
                            title={"Kirimlar ro'yhati"}
                            addModal={openViewPageFun}
                            search={search}
                            calendar={getDate}
                        />


                        <IncomeProductList/>
                        <Pagination
                            activePage={page + 1}
                            itemsCountPerPage={size}
                            totalItemsCount={totalElements}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                            linkClass="page-link"
                        />
                    </Container>
                    :
                    <Container className={"pt-4"}>
                        <h3 className={"text-center"}>Kirim malumotlari qoshish</h3>
                        <span onClick={openViewPageFun} className={"deleteButton"}><X /></span>
                        <AvForm method={"POST"} onValidSubmit={saveForm}>
                            <Row className={"mt-5"}>
                                <Col md={3}>
                                    <AvField type={"number"} label={"Hujjat nomeri:"} className={"firstInput"}
                                             name={"contractNumber"}
                                             placeholder="e.g. 5562235"/>
                                </Col>
                                <Col md={3}>
                                    <AvField label={"Sana:"} type={"datetime-local"} className={"secondInput"}
                                             name={"contractTime"}/>
                                </Col>
                                <Col md={6}>
                                    <AvField label={"Qo'yiluvchi ombor:"} type="select" name="warehouseId"
                                             id="exampleSelect">
                                        <option selected>Omborni tanlang</option>
                                        {warehouses ? warehouses.map((item, i) =>
                                            <option value={item.id}>{item.name}</option>
                                        ) : ""}
                                    </AvField>
                                </Col>
                            </Row>
                            <Row className={"mt-4"}>
                                <Col md={4}>
                                    <span>Yetkazib beruvchi shaxs</span>
                                    <Select
                                        defaultValue={"0"}
                                        placeholder={"Yetkazib beruvchi shaxsni tanlang"}
                                        onChange={(e) => getRole(e,"supplier")}
                                        isSearchable={true}
                                        options={suppliers}
                                        classNamePrefix={"select"}
                                    />
                                </Col>
                                <Col md={4}>
                                    <span>Javobgar shaxs</span>
                                    <Select
                                        defaultValue={"0"}
                                        placeholder={"Javobgar shaxsni tanlang"}
                                        onChange={(e) => getRole(e,"responsible")}
                                        isSearchable={true}
                                        options={agents}
                                        classNamePrefix={"select"}
                                    />
                                </Col>
                                <Col md={4}>
                                    <span>Qabul qiluvchi shaxs</span>
                                    <Select
                                        defaultValue={"0"}
                                        placeholder={"Qabul qilivchi shaxsni tanlang"}
                                        onChange={(e) => getRole(e,"receiver")}
                                        isSearchable={true}
                                        options={agents}
                                        classNamePrefix={"select"}
                                    />
                                </Col>
                            </Row>

                            <div className="col-2 addUser ">
                                <div className={"viewButton"} onClick={addTr}>
                                    <AddButton />
                                </div>
                            </div>

                            <Table className={"inComeTable mt-5"}>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Nomi</th>
                                    <th>Eski narx</th>
                                    <th>Soni</th>
                                    <th>Narx</th>
                                    <th>Summa</th>
                                    <th>Bozor narx</th>
                                    <th>Amal</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.inputs.map((item, i) =>
                                    item.status ?
                                        <tr key={item.id}>
                                            <th scope="row">{i + 1}</th>
                                            <td><Input type={"text"} value={item.productTemplateName} disabled/>
                                            </td>
                                            <td><Input type={"number"} disabled value={item.oldPrice}/></td>
                                            <td><Input type={"number"} value={item.count} disabled/></td>
                                            <td><Input type={"number"} value={item.price} disabled/></td>
                                            <td><Input type={"number"} value={item.allPrice} disabled/></td>
                                            <td><Input type={"number"} value={item.retailPrice} disabled/></td>
                                            <td>
                                                <span className={"editButton"} onClick={() => editTr(item.id)}>
                                                        <EditButton />
                                                    </span>
                                                <span className={"deleteButton"} onClick={() => deleteTr(i)}>
                                                        <DeleteButton />
                                                    </span>
                                            </td>
                                        </tr>
                                        :
                                        <tr key={item.id}>
                                            <th scope="row">{i + 1}</th>
                                            <td>
                                                <Select
                                                    id={`name/${item.id}`}
                                                    defaultValue={{label: item.productTemplateName, value: item.productTemplateId}}
                                                    selectedValue={{label: item.productTemplateName, value: item.productTemplateId}}
                                                    onChange={(e) => getPrice(e, item.id)}
                                                    placeholder={"Mahsulot"}
                                                    isSearchable={true}
                                                    options={productTemplatesSearch}
                                                    classNamePrefix={"select"}
                                                    className={"product_input"}
                                                />
                                                <input id={`templateId/${item.id}`} type={"hidden"} />
                                            </td>
                                            <td><Input id={`oldPrice/${item.id}`} type={"number"}
                                                       defaultValue={item.oldPrice}
                                                       disabled/>
                                            </td>
                                            <td><Input id={`count/${item.id}`} type={"number"}
                                                       defaultValue={item.count}/></td>
                                            <td><Input id={`price/${item.id}`} type={"number"}
                                                       defaultValue={item.price}
                                                       onChange={(e) => calc(item.id, e)}/></td>
                                            <td><Input id={`allSum/${item.id}`} type={"number"}
                                                       defaultValue={item.allPrice}
                                                       disabled/></td>
                                            <td><Input id={`retailPrice/${item.id}`} type={"number"}
                                                       defaultValue={item.retailPrice}/></td>
                                            <td>
                                                <span className={"viewButton"} onClick={() => editTr(item.id)}>
                                                    <AddButton />
                                                </span>
                                                <span className={"deleteButton"} onClick={() => deleteTr(i)}>
                                                    <DeleteButton />
                                                </span>
                                            </td>
                                        </tr>
                                )}
                                </tbody>
                                <tr>
                                    <td colSpan={5}>&nbsp;</td>
                                    <td><Input id={"allA"} placeholder={"Jami summa :"} disabled/></td>
                                    <td></td>
                                </tr>
                            </Table>
                            <Row>
                                <Col md={12}>
                                    <Button className={"btnSave"} type={"submit"}>
                                        Saqlash
                                    </Button>
                                </Col>
                            </Row>
                        </AvForm>
                    </Container>
                }
            </div>
        )
    }
}

IncomeProduct.propTypes = {};

export default connect(
    ({
         app: {
             showModal, suppliers, agents, openModal, warehouses, currentItem, price,
             editModal, deleteModal, productTemplates, currentObject, income,productTemplatesSearch
             ,page,size,totalElements,totalPages
         },
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        openModal,
        deleteModal,
        editModal,
        warehouses,
        currentObject,
        currentItem,
        suppliers,
        agents,
        productTemplates,
        price, income,
        productTemplatesSearch,
        page,size,totalElements,totalPages
    })
)(IncomeProduct);
