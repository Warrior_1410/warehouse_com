import React, {Component} from 'react';
import {Input, Table} from "reactstrap";
import {connect} from "react-redux";
import {
    saveMeasureAction,
    getMeasuresAction,
    deleteMeasureAction,
    updateMeasureAction,
    patchMeasureAction, getMeasureSearchAction
} from "../../redux/actions/MeasureAction";
import {AvField} from 'availity-reactstrap-validation';
import Header from "../../component/Header/Header";
import {DeleteButton, EditButton} from "../../component/Icons";
import AddModal from "../../component/Modal/AddModal";
import EditModal from "../../component/Modal/EditModal";
import DeleteModal from "../../component/Modal/DeleteModal";
import StatusModal from "../../component/Modal/StatusModal";


class CurrencyName extends Component {
    componentDidMount() {
        this.props.dispatch(getMeasuresAction());
        console.clear()
    }


    render() {
        const {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
            page, size, totalElements, totalPages,measures} = this.props;

        const showHideAddModal = () => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    addModal: !addModal
                }
            })
        }
        const showHideEditModal = (item) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentObject : item,
                    editModal: !editModal
                }
            })
        }
        const showHideDeleteModal = (id) => {
            if (id){
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : id,
                        deleteModal: !deleteModal
                    }
                })
            }else {
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        currentItem : '',
                        deleteModal: !deleteModal
                    }
                })
            }
        }

        const saveItem = (e, v) => {
            this.props.dispatch(saveMeasureAction(v))
        }
        const deleteItem = () => {
            this.props.dispatch(deleteMeasureAction(currentItem))
        }
        const editItem = (e, v) => {
            this.props.dispatch(updateMeasureAction(v))
        }

        const showHideStatusModal = (id) => {
            this.props.dispatch({
                type: "updateState",
                payload: {
                    currentItem : id,
                    statusModal: !statusModal
                }
            })
            if (id === 0){
                this.props.dispatch({
                    type: "updateState",
                    payload: {
                        measures: null
                    }
                })
                this.props.dispatch(getMeasuresAction())
            }
        }
        const updateStatus = () => {
            this.props.dispatch(patchMeasureAction(currentItem))
        }
        const search = (e) => {
            if (e.target.value === "") {
                this.props.dispatch(getMeasuresAction())
            } else {
                this.props.dispatch(getMeasureSearchAction(e.target.value))
            }
        }
        return (
            <div>

                <Header title={"O'lchov birliklari' ro'yhati"} addModal={showHideAddModal} search={search} />

                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nomi</th>
                            <th>Izoh</th>
                            <th>Faol</th>
                            <th className={'text-center'}>Amal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {measures ? measures.map((item, i) =>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>
                                    <Input type={"checkbox"} onChange={() => showHideStatusModal(item.id)} checked={item.active}/>
                                </td>
                                <td className={'text-center'}>
                                    <div className={"editButton"} onClick={() => showHideEditModal(item)}>
                                        <EditButton />
                                    </div>
                                    <div className={"deleteButton"} onClick={() => showHideDeleteModal(item.id)}>
                                        <DeleteButton />
                                    </div>
                                </td>
                            </tr>
                        ) : <tr>
                            <td colSpan={5}>Malumot topilmadi</td>
                        </tr>}
                        </tbody>
                    </Table>

                <AddModal isOpen={addModal} toggle={showHideAddModal}
                          close={showHideAddModal} onValidSubmit={saveItem}>
                    <span>Nomi</span>
                    <AvField type="search" name="name" required/>

                    <span>Izoh</span>
                    <AvField type="text" name="description"/>

                    <AvField type="checkbox" name="active" label={"Faol"}/>
                </AddModal>

                <EditModal isOpen={editModal} toggle={showHideEditModal}
                           close={showHideEditModal} onValidSubmit={editItem}>
                    <AvField type={"hidden"} name={"id"}
                             defaultValue={currentObject ? currentObject.id : ''}/>
                    <span>Nomi</span>
                    <AvField type="search" name="name"
                             defaultValue={currentObject ? currentObject.name : ''}
                             required/>
                    <span>Izoh</span>
                    <AvField type="search" name="description"
                             defaultValue={currentObject ? currentObject.description : ''}/>

                    <AvField type="checkbox" name="active" label={"Faol"} defaultChecked={currentObject ? currentObject.active : false}/>
                </EditModal>

                <DeleteModal isOpen={deleteModal} close={showHideDeleteModal} del={deleteItem}/>

                <StatusModal isOpen={statusModal} close={() => showHideStatusModal(0)} update={updateStatus}/>
            </div>
        );
    }
}

CurrencyName.propTypes = {};

export default connect(
    ({
         app: {addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
             page, size, totalElements, totalPages, measures},
         auth: {isAdmin, isSuperAdmin, currentUser}
     }) => ({
        addModal, editModal, deleteModal, statusModal, currentItem, currentObject,
        page, size, totalElements, totalPages,
        measures
    })
)(CurrencyName);
