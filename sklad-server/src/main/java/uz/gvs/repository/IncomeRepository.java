package uz.gvs.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Income;
import uz.gvs.entity.Warehouse;

import javax.swing.text.html.Option;
import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IncomeRepository extends JpaRepository<Income, UUID> {

    @Query(nativeQuery = true, value = "select coalesce(max(contract_number),'0') from income")
    String findMaxContractNumber();

    @Query(nativeQuery = true, value = "SELECT * FROM income WHERE to_char(contract_time, 'YYYY-MM-DD')")
    List<Income> findAllByDate();

    Optional<Income> findByContractNumber(String contractNumber);

    @Query(nativeQuery = true, value = "select * from income where contract_number like %:contractNumber%")
    List<Income> getAllIncomeSearch(String contractNumber);
}
