package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.gvs.entity.OrderProduct;


import java.util.UUID;

public interface OrderProductRepository extends JpaRepository<OrderProduct, UUID> {
}
