package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Bank;
import uz.gvs.entity.MobilePayment;

import java.util.UUID;

public interface MobilePaymentRepository extends JpaRepository<MobilePayment, UUID> {

    @Query(nativeQuery = true, value = "select coalesce(max(contract_number),'0') from mobile_payment")
    String findMaxContractNumber();
}
