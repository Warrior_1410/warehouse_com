package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Bank;
import uz.gvs.entity.CashName;
import uz.gvs.entity.Warehouse;

import java.util.List;

public interface CashNameRepository extends JpaRepository<CashName,Integer> {

    boolean existsByNameEqualsIgnoreCase(String name);

    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    @Query(nativeQuery = true, value = "select * from cash_name where name like %:name%")
    List<CashName> getAllCashNameSearch(String name);
}
