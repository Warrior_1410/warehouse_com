package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.GrantedAuthority;
import uz.gvs.entity.Role;
import uz.gvs.entity.User;
import uz.gvs.entity.enums.RoleName;

import java.util.*;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String phoneNumber);

    @Query(nativeQuery = true, value = "select * from users where id = (id)")
    Optional<UUID> findUserIdByRoleName(@Param("id") UUID id);

    List<User> findUsersByRoles(Role role);
}
