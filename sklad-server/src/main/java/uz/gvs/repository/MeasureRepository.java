package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Measure;
import uz.gvs.entity.Warehouse;

import java.util.List;

public interface MeasureRepository extends JpaRepository<Measure,Integer> {
    boolean existsByNameEqualsIgnoreCase(String name);
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    @Query(nativeQuery = true, value = "select * from measure where name like %:name%")
    List<Measure> getAllMeasureSearch(String name);
}
