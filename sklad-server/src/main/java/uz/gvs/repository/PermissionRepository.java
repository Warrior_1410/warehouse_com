package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.gvs.entity.Permission;
import uz.gvs.entity.Role;
import uz.gvs.entity.enums.PermissionName;
import uz.gvs.entity.enums.RoleName;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PermissionRepository extends JpaRepository<Permission, Integer> {
    Set<Permission> findAllByPermissionName(PermissionName permissionName);

    boolean existsByPermissionName(PermissionName permissionName);

    @Query(value = "select * from permission where id in(select permission_id from permission_role where role_id=(select id from role where role_name=:roleName))", nativeQuery = true)
    List<Permission> findAllByRoleName(@Param("roleName") String roleName);

}
