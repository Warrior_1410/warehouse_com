package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.gvs.entity.Region;
import uz.gvs.entity.Warehouse;

import java.util.List;
import java.util.Optional;

public interface RegionRepository extends JpaRepository<Region,Integer> {

    @Query(nativeQuery = true, value = "select * from region where region_id=null and lower(name)=lower(:regionName)")
    boolean regionIdnull();
    //
    @Query(nativeQuery = true, value = "delete from region where region_id = ?id")
    boolean deleteRegionByRegionId(@Param("id") Integer id);
    // ota yoq add
    @Query(nativeQuery = true,value = "select * from region where lower(name)=lower(:regionName) and region_id isNULL")
    Optional<Region> existRegion(@Param("regionName")String regionName);

    @Query(nativeQuery = true, value = "select * from region where name like %:name%")
    List<Region> getAllRegionSearch(String name);

    //add region ota si bosa
    boolean existsByNameEqualsIgnoreCaseAndRegion_Id(String name, Integer region_id);
    boolean existsByNameEqualsIgnoreCaseAndRegion_IdIsNull(String name);

    // update otasi bosaa
    boolean existsByNameEqualsIgnoreCaseAndIdNotAndRegion_Id(String name, Integer id, Integer region_id);

    //update yoq otasi bosa
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    //
    boolean deleteByRegion_RegionId(Integer region_region_id);

    boolean existsByNameEqualsIgnoreCase(String name);
}

