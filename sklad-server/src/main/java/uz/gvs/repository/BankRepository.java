package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Bank;
import uz.gvs.entity.Warehouse;

import java.util.List;
import java.util.Optional;

public interface BankRepository extends JpaRepository<Bank,Integer> {

    boolean existsByNameEqualsIgnoreCase(String name);
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    @Query(nativeQuery = true, value = "select * from bank where name like %:name%")
    List<Bank> getAllBankSearch(String name);
}
