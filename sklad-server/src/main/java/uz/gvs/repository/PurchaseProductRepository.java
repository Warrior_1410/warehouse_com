package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Brand;
import uz.gvs.entity.ProductCategory;
import uz.gvs.entity.PurchaseProduct;
import uz.gvs.entity.Warehouse;

import java.util.List;
import java.util.UUID;

public interface PurchaseProductRepository extends JpaRepository<PurchaseProduct, UUID> {

    @Query(nativeQuery = true, value = "select * from product_category where name like %:name%")
    List<ProductCategory> getAllProductCategorySearch(String name);

}
