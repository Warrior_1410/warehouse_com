package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Income;
import uz.gvs.entity.Shipment;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ShipmentRepository extends JpaRepository<Shipment, UUID> {

    @Query(nativeQuery = true, value = "select coalesce(max(contract_number),'0') from shipment")
    String findMaxContractNumber();

    Optional<Shipment> findByContractNumber(String contractNumber);

    @Query(nativeQuery = true, value = "select * from shipment where contract_number like %:contractNumber%")
    List<Shipment> getAllShipmentSearch(String contractNumber);
}
