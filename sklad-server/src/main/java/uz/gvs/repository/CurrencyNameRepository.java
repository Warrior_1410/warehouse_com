package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.CurrencyName;
import uz.gvs.entity.Warehouse;

import java.util.List;

public interface CurrencyNameRepository extends JpaRepository<CurrencyName,Integer> {
    boolean existsByNameEqualsIgnoreCase(String name);
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    @Query(nativeQuery = true, value = "select * from currency_name where name like %:name%")
    List<CurrencyName> getAllCurrencyNameSearch(String name);
}
