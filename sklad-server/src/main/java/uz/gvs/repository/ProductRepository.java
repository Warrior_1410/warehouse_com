package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.gvs.entity.Product;
import uz.gvs.entity.Warehouse;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    @Query(nativeQuery = true, value = "select * from product p where p.product_template_id=:productTemplateId and p.warehouse_id = :warehouseId and leftover>0 order by (select contract_time from income where id=(select income_id from income_product where product_id=p.id))")
    List<Product> getAllProductLeftOver(UUID productTemplateId,Integer warehouseId);

    @Query(nativeQuery = true, value = "select coalesce(sum(p.leftover),0) from product p where p.product_template_id=:productTemplateId and p.leftover>0")
    Double getAllLeftOver(UUID productTemplateId);

    @Query(nativeQuery = true,value = "select price from product where product_template_id =:id order by created_at desc limit 1")
    Optional<Double> getAllByAllPrice(@Param("id") UUID id);

    @Query(nativeQuery = true, value = "select coalesce(sum(p.leftover),0) from product p where p.product_template_id=:productTemplateId and p.leftover>0 and warehouse_id = :warehouseId")
    Double getAllByWarehouseId(@Param("warehouseId") Integer warehouseId,@Param("productTemplateId") UUID productTemplateId);

    @Query(nativeQuery = true, value = "select coalesce(sum(p.leftover),0) from product p where p.product_template_id=:productTemplateId")
    Double getAllProductTemplate(@Param("productTemplateId") UUID productTemplateId);

}
