package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.gvs.entity.UserCategory;

public interface UserCategoryRepository extends JpaRepository<UserCategory, Integer> {

}
