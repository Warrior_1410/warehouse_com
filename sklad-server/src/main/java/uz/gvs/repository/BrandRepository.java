package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Brand;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Warehouse;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BrandRepository extends JpaRepository<Brand,Integer> {
    boolean existsByNameEqualsIgnoreCase(String name);
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    @Query(nativeQuery = true, value = "select * from brand where name like %:name%")
    List<Brand> getAllBrandSearch(String name);

    Optional<Brand> findByAttachment_Id(UUID id);
}
