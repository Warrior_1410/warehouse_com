package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.gvs.entity.Brand;
import uz.gvs.entity.ProductCategory;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Region;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory,Integer> {
    boolean existsByNameEqualsIgnoreCase(String name);
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);
    boolean existsByNameEqualsIgnoreCaseAndIdNotAndCategory_Id(String name, Integer id, Integer category_id);
    // ota yoq add
    @Query(nativeQuery = true,value = "select * from product_category where lower(name)=lower(:productCategoryName) and category_id isNULL")
    Optional<ProductCategory> productCategory(@Param("productCategoryName")String productCategoryName);


    @Query(nativeQuery = true, value = "select * from product_category where name like %:name%")
    List<ProductCategory> getAllProductCategorySearch(String name);

    Optional<ProductCategory> findByAttachment_Id(UUID id);

}
