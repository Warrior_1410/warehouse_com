package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.gvs.entity.Currency;
import uz.gvs.entity.Kassa;

import java.util.UUID;

public interface KassaRepository extends JpaRepository<Kassa, UUID> {

}
