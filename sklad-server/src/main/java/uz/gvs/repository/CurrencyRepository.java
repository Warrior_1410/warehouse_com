package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.gvs.entity.CashName;
import uz.gvs.entity.Currency;

import java.util.UUID;

public interface CurrencyRepository extends JpaRepository<Currency, UUID> {
}
