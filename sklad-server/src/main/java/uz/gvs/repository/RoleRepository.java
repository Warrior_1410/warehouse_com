package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.gvs.entity.Role;
import uz.gvs.entity.enums.RoleName;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Set<Role> findAllByRoleName(RoleName roleName);

    Set<Role> findAllByRoleNameIn(Collection<RoleName> roleName);
    Optional<Role> findByRoleName(RoleName roleName);

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @Override
    void deleteById(Integer integer);
}
