package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Bank;
import uz.gvs.entity.KassaPaymentType;
import uz.gvs.entity.Warehouse;

import java.util.List;

public interface KassaPaymentTypeRepository extends JpaRepository<KassaPaymentType,Integer> {

    boolean existsByNameEqualsIgnoreCase(String name);

    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, Integer id);

    @Query(nativeQuery = true, value = "select * from kassa_payment_type where name like %:name%")
    List<KassaPaymentType> getAllKassaPaymentTypeSearch(String name);
}
