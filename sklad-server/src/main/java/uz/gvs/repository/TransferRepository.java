package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Income;
import uz.gvs.entity.Shipment;
import uz.gvs.entity.Transfer;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransferRepository extends JpaRepository<Transfer, UUID> {
    @Query(nativeQuery = true, value = "select coalesce(max(contract_number),'0') from transfer")
    String findMaxContractNumber();

    Optional<Transfer> findByContractNumber(String contractNumber);

    @Query(nativeQuery = true, value = "select * from transfer where contract_number like %:contractNumber%")
    List<Transfer> getAllTransferSearch(String contractNumber);
}
