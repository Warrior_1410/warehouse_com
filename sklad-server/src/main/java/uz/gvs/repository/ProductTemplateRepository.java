package uz.gvs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.gvs.entity.Brand;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Warehouse;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductTemplateRepository extends JpaRepository<ProductTemplate, UUID> {
    boolean existsByNameEqualsIgnoreCaseAndIdNot(String name, UUID id);
    boolean existsByNameEqualsIgnoreCase(String name);

    @Query(nativeQuery = true, value = "select * from product_template where name like %:name%")
    List<ProductTemplate> getAllProductTemplateSearch(String name);

    Optional<ProductTemplate> findByAttachment_Id(UUID id);

}
