package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Currency extends AbsEntity {
    @ManyToOne
    private CurrencyName currencyName;//dollar
    private double amount;// 10 500 so'm, faqat so'mdagi qiymat yoziladi
    private double unity;// 1$
}
