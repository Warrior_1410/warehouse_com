package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ProductTemplate extends AbsEntity {
    private String kod;
    @Column(nullable = false)
    private String name;
    private String barcode;
    private Integer minCount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Brand brand;
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCategory productCategory;
    @ManyToOne(fetch = FetchType.LAZY)
    private Measure measure;
    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment attachment;


    private boolean active;
    private boolean mobile;
}
