package uz.gvs.entity.enums;

public enum ReturnType {
    RETURN_FROM_BUYERS,
    RETURN_TO_SUPPLIER,
}
