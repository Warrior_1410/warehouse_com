package uz.gvs.entity.enums;

public enum RoleName {
    ADMIN, USER, SUPPLIER, CLIENT, AGENT, FOUNDER, SUPER_ADMIN
}
