package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductTemplate productTemplate;
    private double count;
    private double leftover;
    private double price;
    private Double retailPrice;
    @ManyToOne
    private Warehouse warehouse;
    //transfer uchun
    @ManyToOne
    private Product product;
    private Double allPrice;
    private boolean transfer;
}
