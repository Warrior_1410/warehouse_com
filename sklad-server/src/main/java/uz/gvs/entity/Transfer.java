package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Transfer extends AbsEntity {
    private String contractNumber;
    private Timestamp contractTime;
    @ManyToOne
    private Warehouse fromWarehouse;
    @ManyToOne
    private Warehouse toWarehouse;
    @ManyToOne
    private User user;
    private double allAmount;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "transfer_products",
            joinColumns = {@JoinColumn(name = "transfer_id")},
            inverseJoinColumns = {@JoinColumn(name = "product_id")})
    private List<Product> transferProduct;
    private boolean accepted;
}
