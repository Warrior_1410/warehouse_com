package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.enums.ReturnType;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PurchaseProduct extends AbsEntity {
    private String contractNumber;
    private String contractTime;
    @ManyToOne
    private Warehouse warehouse;

    @ManyToOne
    private User responsibleUser;
    @ManyToOne
    private User receiverUser;
    @ManyToOne
    private User clientUser;

    @Enumerated(EnumType.STRING)
    private ReturnType returnType;

    private double allPrice;
}
