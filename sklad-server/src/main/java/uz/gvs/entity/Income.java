package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.enums.StatusEnum;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Income extends AbsEntity {
    private String contractNumber;
    private Timestamp contractTime;

    @ManyToMany
    @JoinTable(name = "income_product",
            joinColumns = {@JoinColumn(name = "income_id")},
            inverseJoinColumns = {@JoinColumn(name = "product_id")})
    private List<Product> products;

    @ManyToOne
    private User responsibleUser;
    @ManyToOne
    private User receiverUser;
    @ManyToOne
    private User supplierUser;
    @ManyToOne
    private Warehouse warehouse;

    private Double amount;
    @Enumerated(EnumType.STRING)
    private StatusEnum statusEnum;
}
