package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Shipment extends AbsEntity {
    private String contractNumber;
    private Timestamp contractTime;
    @ManyToOne
    private Warehouse warehouse;
    @ManyToOne
    private User responsibleUser;
    @ManyToOne
    private User receiverUser;
    @ManyToOne
    private User supplierUser;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sell_product",
            joinColumns = {@JoinColumn(name = "shipment_id")},
            inverseJoinColumns = {@JoinColumn(name = "order_product_id")})
    private List<OrderProduct> orderProducts;
    private double allSum; //kirimni umumuiy narxi 3 300 000 yoki 340$
    private double paymentSumPercent;//to'lovning necha foizi to'langani

    private double currency; // dollar bo'lsa 10500, 1.00
    private double paymentAmount; //dollar bo'lsa 100$, so'm 1 200 000
    private double paymentTotal;// dollar bo'lsa, 1 050 000, so'm - 1 200 000
    private boolean active;
}
