package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MobileOrder extends AbsEntity {

    private String contractNumber;
    private Timestamp contractTime;
    @ManyToOne
    private User client;
    @ManyToOne
    private User agent;

    @ManyToMany
    @JoinTable(name = "sell_product",
            joinColumns = {@JoinColumn(name = "shipment_id")},
            inverseJoinColumns = {@JoinColumn(name = "order_product_id")})
    private List<OrderProduct> products;

    private double allPrice;
    private boolean accepted;
}
