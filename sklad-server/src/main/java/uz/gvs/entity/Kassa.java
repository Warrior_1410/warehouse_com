package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Kassa extends AbsEntity {//kassa kirim
    private Timestamp paymentTime;
    @ManyToOne
    private CashName cashName;//dollar, sum kassasi

    private double amount;//1$, 1 so'm
    private double unity;// 10600 so'm, 1 so'm
    private double allSum;//200$
    @ManyToOne
    private KassaPaymentType kassaPaymentType;

    @ManyToOne
    private User user;//to'lov kim uchunligi
}
