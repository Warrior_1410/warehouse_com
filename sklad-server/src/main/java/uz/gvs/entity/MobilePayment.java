package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MobilePayment extends AbsEntity {
    private String contractNumber;
    private Timestamp contractTime;
    @ManyToOne
    private User client;
    @ManyToOne
    private User agent;
    @OneToMany
    private List<Kassa> kassa;
    private boolean accepted;
    private double allPrice;
}
