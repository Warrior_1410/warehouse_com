package uz.gvs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.gvs.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ReturnProduct extends AbsEntity {
    @ManyToOne
    private OrderProduct orderProduct;
    @ManyToOne
    private Product product;
    //    private String contractNumber;
    private double count;
    private double price;
    private double allPrice;
}
