package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.CurrencyName;

import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyDto {
    private UUID id;
    private Integer currencyNameId;
    private double amount;
    private double unity;
    private CurrencyName currencyName;

    public CurrencyDto(Integer currencyNameId,double amount, double unity)//Req
    {
        this.currencyNameId = currencyNameId;
        this.amount = amount;
        this.unity = unity;
    }

    public CurrencyDto(UUID id, double amount, double unity, CurrencyName currencyName)
    {
        this.id = id;
        this.amount = amount;
        this.unity = unity;
        this.currencyName = currencyName;

    }
}
