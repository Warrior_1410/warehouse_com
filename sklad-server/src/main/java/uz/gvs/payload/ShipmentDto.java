package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.OrderProduct;
import uz.gvs.entity.User;
import uz.gvs.entity.Warehouse;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShipmentDto {
    private UUID id;

    private String contractNumber;
    private LocalDateTime contractTime;

    //REQ
    private Integer warehouseId;
    private UUID responsibleUserId;
    private UUID receiverUserId;
    private UUID supplierUserId;

    //RES
    private Warehouse warehouse;
    private User responsibleUser;
    private User receiverUser;
    private User supplierUser;

    private List<OrderProductDto> orderProductDto;
    private List<UUID> orderProductId;
    private List<OrderProduct> products;
    private double allSum;
    private double paymentSumPercent;
    private double currency;
    private double paymentAmount;
    private double paymentTotal;

    //REQ
    public ShipmentDto(String contractNumber,LocalDateTime contractTime,Integer warehouseId, UUID responsibleUserId, UUID receiverUserId, UUID supplierUserId, List<OrderProductDto> orderProductDto, double allSum, double paymentSumPercent, double currency, double paymentAmount, double paymentTotal) {
        this.contractNumber = contractNumber;
        this.contractTime = contractTime;
        this.warehouseId = warehouseId;
        this.responsibleUserId = responsibleUserId;
        this.receiverUserId = receiverUserId;
        this.supplierUserId = supplierUserId;
        this.orderProductDto = orderProductDto;
        this.allSum = allSum;
        this.paymentSumPercent = paymentSumPercent;
        this.currency = currency;
        this.paymentAmount = paymentAmount;
        this.paymentTotal = paymentTotal;
    }
    //RES

    public ShipmentDto(UUID id, String contractNumber, LocalDateTime contractTime, Warehouse warehouse, User responsibleUser, User receiverUser, User supplierUser, List<OrderProduct> products, double allSum, double paymentSumPercent, double currency, double paymentAmount, double paymentTotal) {
        this.id = id;
        this.contractNumber = contractNumber;
        this.contractTime = contractTime;
        this.warehouse = warehouse;
        this.responsibleUser = responsibleUser;
        this.receiverUser = receiverUser;
        this.supplierUser = supplierUser;
        this.products = products;
        this.allSum = allSum;
        this.paymentSumPercent = paymentSumPercent;
        this.currency = currency;
        this.paymentAmount = paymentAmount;
        this.paymentTotal = paymentTotal;
    }

    public ShipmentDto(String contractNumber, Warehouse warehouse, User responsibleUser, User receiverUser, User supplierUser, double allSum) {
        this.contractNumber = contractNumber;
        this.warehouse = warehouse;
        this.responsibleUser = responsibleUser;
        this.receiverUser = receiverUser;
        this.supplierUser = supplierUser;
        this.allSum = allSum;
    }
}
