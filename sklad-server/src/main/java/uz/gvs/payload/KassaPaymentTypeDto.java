package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class KassaPaymentTypeDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;

    KassaPaymentTypeDto(String name, String description, boolean active) //Req
    {
        this.name = name;
        this.description = description;
        this.active = active;

    }
}
