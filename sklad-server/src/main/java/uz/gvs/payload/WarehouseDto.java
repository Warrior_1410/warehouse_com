package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class WarehouseDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;

    WarehouseDto(String name, String description, boolean active){
        this.name = name;
        this.description = description;
        this.active = active;

    }
}
