package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.*;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private UUID id;
    private String fullName;
    private String roleName;
    @NotNull
    private String phoneNumber;
    private String password;
    private String description;
    private String address;
    private String phoneNumber2;
    private Region region;
    private Integer regionId;
    private Contact contact;
    private UUID contactId;
    private Integer bankId;
    private BankDto bankDto;
    private Bank bank;
    private String account;

    private Role roles;

    public UserDto(UUID id, String fullName, @NotNull String phoneNumber, String password, String description, String address, String phoneNumber2, Region region, Contact contact, Bank bank) {
        this.id = id;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.description = description;
        this.address = address;
        this.phoneNumber2 = phoneNumber2;
        this.region = region;
        this.contact = contact;
        this.bank = bank;
//        this.roles = roles;
    }

    public UserDto(UUID id, String fullName){
        this.id = id;
        this.fullName = fullName;
    }

    public UserDto(UUID id, String fullName, @NotNull String phoneNumber, String description, String address, String phoneNumber2, Region region, Contact contact, Bank bank, String account) {
        this.id = id;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.address = address;
        this.phoneNumber2 = phoneNumber2;
        this.region = region;
        this.contact = contact;
        this.bank = bank;
        this.account = account;
    }
}
