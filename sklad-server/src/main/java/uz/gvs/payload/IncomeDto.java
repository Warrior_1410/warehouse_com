package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Product;
import uz.gvs.entity.User;
import uz.gvs.entity.Warehouse;
import uz.gvs.entity.enums.StatusEnum;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncomeDto {
    private UUID id;
    private String contractNumber;
    private LocalDateTime contractTime;

    private List<ProductDto> productDtos;
    private List<UUID> productDtosId;

    private List<Product> products;
    private List<UUID> productsId;
    private String description;

    private User responsibleUser;
    private UUID responsibleUserId;

    private User receiverUser;
    private UUID receiverUserId;

    private User supplierUser;
    private UUID supplierUserId;

    private Warehouse warehouse;
    private Integer warehouseId;

    private double amount;
    private StatusEnum statusEnum;

    //Req
    public IncomeDto(List<ProductDto> productDtos, UUID responsibleUserId, UUID receiverUserId, UUID supplierUserId, Integer warehouseId, double amount,String contractNumber,LocalDateTime contractTime){
        this.productDtos = productDtos;
        this.responsibleUserId = responsibleUserId;
        this.receiverUserId = receiverUserId;
        this.supplierUserId = supplierUserId;
        this.warehouseId = warehouseId;
        this.amount = amount;
        this.contractNumber = contractNumber;
        this.contractTime = contractTime;
    }

    public IncomeDto(String contractNumber, User responsibleUser, User receiverUser, User supplierUser, Warehouse warehouse, StatusEnum statusEnum,String description) {
        this.contractNumber = contractNumber;
        this.responsibleUser = responsibleUser;
        this.receiverUser = receiverUser;
        this.supplierUser = supplierUser;
        this.warehouse = warehouse;
        this.statusEnum = statusEnum;
        this.description = description;
    }

    public IncomeDto(UUID id,String contractNumber, LocalDateTime contractTime, List<Product> products, String description, User responsibleUser, User receiverUser, User supplierUser, Warehouse warehouse, double amount, StatusEnum statusEnum) {
        this.id = id;
        this.contractNumber = contractNumber;
        this.contractTime = contractTime;
        this.products = products;
        this.description = description;
        this.responsibleUser = responsibleUser;
        this.receiverUser = receiverUser;
        this.supplierUser = supplierUser;
        this.warehouse = warehouse;
        this.amount = amount;
        this.statusEnum = statusEnum;
    }
}
