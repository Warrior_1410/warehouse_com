package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Product;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferProductDto {
    private UUID id;

    private ProductDto productDto;
    private Product product;
    private UUID productId;

    private UUID productTemplateId;
    private double count;
    private double price;
    private double allPrice;
}
