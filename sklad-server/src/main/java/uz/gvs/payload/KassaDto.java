package uz.gvs.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.CashName;
import uz.gvs.entity.KassaPaymentType;
import uz.gvs.entity.User;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KassaDto {
    private UUID id;
    private CashName cashName;
    private Integer cashNameId;
    private double amount;
    private double unity;
    private double allSum;
    private KassaPaymentType kassaPaymentType;
    private Integer kassaPaymentTypeId;
    private User user;
    private UUID userId;
    private Timestamp paymentTime;
    private String description;

    public KassaDto (UUID id, CashName cashName, double amount,double unity,double allSum, Timestamp paymentTime,KassaPaymentType kassaPaymentType, User user,String description){
        this.id = id;
        this.cashName = cashName;
        this.amount = amount;
        this.unity = unity;
        this.allSum = allSum;
        this.user = user;
        this.paymentTime=paymentTime;
        this.kassaPaymentType = kassaPaymentType;
        this.description = description;
    }
    public KassaDto(Integer cashNameId,double amount, double unity,double allSum, Integer kassaPaymentTypeId,Timestamp paymentTime,String description)//Req
    {
        this.cashNameId = cashNameId;
        this.amount = amount;
        this.unity = unity;
        this.allSum = allSum;
        this.kassaPaymentTypeId = kassaPaymentTypeId;
        this.description=description;
    }
}
