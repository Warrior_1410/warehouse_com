package uz.gvs.payload;

import lombok.Data;

@Data
public class PachDto {
    private boolean active;
    private boolean mobile;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }
}
