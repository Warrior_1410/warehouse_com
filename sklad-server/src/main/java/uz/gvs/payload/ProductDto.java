package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Warehouse;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private UUID id;
    private UUID productTemplateId;
    private ProductTemplate productTemplate;
    private double count;
    private double leftover;
    private double price;
    private double retailPrice;
    private double allPrice;
    private UUID productId;
    private Warehouse warehouse;
    private Integer warehouseId;

    public ProductDto(UUID productTemplateId, double count, double price) {
        this.productTemplateId = productTemplateId;
        this.count = count;
        this.price = price;
    }

    public ProductDto(UUID productTemplateId, double count, double price, double retailPrice, Integer warehouseId) {
        this.productTemplateId = productTemplateId;
        this.count = count;
        this.price = price;
        this.retailPrice = retailPrice;
        this.warehouseId = warehouseId;
    }

    public ProductDto(UUID productTemplateId, double count) {
        this.productTemplateId = productTemplateId;
        this.count = count;
    }

    public ProductDto(UUID id, ProductTemplate productTemplate, double count, double leftover, double price, double retailPrice, Warehouse warehouse) {
        this.id = id;
        this.productTemplate = productTemplate;
        this.count = count;
        this.leftover = leftover;
        this.price = price;
        this.retailPrice = retailPrice;
        this.warehouse = warehouse;

    }
    public ProductDto(double price,UUID productTemplateId) {
        this.price = price;
        this.productTemplateId = productTemplateId;

    }
}
