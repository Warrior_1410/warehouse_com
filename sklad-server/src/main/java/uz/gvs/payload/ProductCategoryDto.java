package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Attachment;
import uz.gvs.entity.ProductCategory;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCategoryDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;
    private boolean mobile;
    //REQ
    private UUID attachmentId;
    private Integer categoryId;

    //RES
    private Attachment attachment;
    private ProductCategory category;

    //REQ
    public ProductCategoryDto(String name, String description, boolean active, boolean mobile, UUID attachmentId, Integer categoryId) {
        this.name = name;
        this.description = description;
        this.active = active;
        this.mobile = mobile;
        this.attachmentId = attachmentId;
        this.categoryId = categoryId;
    }
    //RES
    public ProductCategoryDto(Integer id, String name, String description, boolean active, boolean mobile, Attachment attachment, ProductCategory category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.mobile = mobile;
        this.attachment = attachment;
        this.category = category;
    }
}

