package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.CurrencyName;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CashNameDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;
    private Integer currencyNameId;
    private CurrencyName currencyName;

    CashNameDto(String name, String description, boolean active, Integer currencyNameId)//Req
    {
        this.name = name;
        this.description = description;
        this.active = active;
        this.currencyNameId = currencyNameId;

    }

    public CashNameDto(Integer id, String name, String description, boolean active, CurrencyName currencyName){
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.currencyName = currencyName;

    }
}
