package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Product;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderProductDto {
    //edit
    private UUID id;
    //req
    private UUID productTemplateId;
    private double count;
    private double retailPrice;
    private double allPrice;
    //res
    private ProductDto productDto;
    //RES
    private Product product;
}
