package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Attachment;
import uz.gvs.entity.User;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BrandDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;
    private boolean mobile;

    //req
    private UUID attachmentId;
    private UUID userId;
    //res
    private String attachment;//bu frontend uchun rasm format jo'natishni o'rniga
    private User supplierUser;

    //ReqBrand
    public BrandDto(String name, String description, boolean active, boolean mobile, UUID attachmentId, UUID userId) {
        this.name = name;
        this.description = description;
        this.active = active;
        this.mobile = mobile;
        this.attachmentId = attachmentId;
        this.userId = userId;
    }
    //ResBrand
    public BrandDto(Integer id, String name, String description, boolean active, boolean mobile, String attachment,UUID attachmentId, User supplierUser) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.mobile = mobile;
        this.attachment = attachment;
        this.attachmentId = attachmentId;
        this.supplierUser = supplierUser;
    }
}
