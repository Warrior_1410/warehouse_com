package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeasureDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;

    public MeasureDto(String name, String description, boolean active) {
        this.name = name;
        this.description = description;
        this.active = active;
    }
}
