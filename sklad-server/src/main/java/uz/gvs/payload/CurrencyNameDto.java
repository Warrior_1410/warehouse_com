package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyNameDto {
    private Integer id;
    private String name;
    private String description;
    private boolean active;

    CurrencyNameDto(String name, String description, boolean active)//Req
    {
        this.name = name;
        this.description = description;
        this.active = active;

    }
}
