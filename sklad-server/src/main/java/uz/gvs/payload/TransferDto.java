package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Product;
import uz.gvs.entity.User;
import uz.gvs.entity.Warehouse;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferDto {
    private UUID id;

    private String contractNumber;
    private LocalDateTime contractTime;

    private Integer fromWarehouseId;
    private Integer toWarehouseId;
    private UUID userId;

    private Warehouse fromWarehouse;
    private Warehouse toWarehouse;

    private User user;
//    private List<TransferProductDto> transferProductDto;
    private List<ProductDto> transferProductDto;
    private List<UUID> transferProductId;
    private List<Product> transferProducts;
    private double allAmount;
    private boolean accepted;

    //REQ

    public TransferDto(UUID id, String contractNumber, LocalDateTime contractTime, Warehouse fromWarehouse, Warehouse toWarehouse, User user, List<Product> transferProducts, double allAmount, boolean accepted) {
        this.id = id;
        this.contractNumber = contractNumber;
        this.contractTime = contractTime;
        this.fromWarehouse = fromWarehouse;
        this.toWarehouse = toWarehouse;
        this.user = user;
        this.transferProducts = transferProducts;
        this.allAmount = allAmount;
        this.accepted = accepted;
    }

    public TransferDto(Integer fromWarehouseId, Integer toWarehouseId, UUID userId, List<ProductDto> transferProductDto, double allAmount, boolean accepted) {
        this.fromWarehouseId = fromWarehouseId;
        this.toWarehouseId = toWarehouseId;
        this.userId = userId;
        this.transferProductDto = transferProductDto;
        this.allAmount = allAmount;
        this.accepted = accepted;
    }

    public TransferDto(UUID id,String contractNumber, Warehouse fromWarehouse, Warehouse toWarehouse, User user, double allAmount, boolean accepted) {
        this.id = id;
        this.contractNumber = contractNumber;
        this.fromWarehouse = fromWarehouse;
        this.toWarehouse = toWarehouse;
        this.user = user;
        this.allAmount = allAmount;
        this.accepted = accepted;
    }
}
