package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Attachment;
import uz.gvs.entity.Brand;
import uz.gvs.entity.Measure;
import uz.gvs.entity.ProductCategory;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductTemplateDto {
    private UUID id;
    private String kod;
    private String name;
    private String barcode;
    private Integer minCount;
    private boolean active;
    private boolean mobile;
    //REQ
    private Integer brandId;
    private Integer productCategoryId;
    private Integer measureId;
    private UUID attachmentId;
    //RES
    private Brand brand;
    private ProductCategory productCategory;
    private Measure measure;
    private Attachment attachment;
    private String attachmentContent;

    private Double count;

    public ProductTemplateDto(String kod, String name, String barcode, Integer minCount, boolean active, boolean mobile, Integer brandId, Integer productCategoryId, Integer measureId, UUID attachmentId) {
        this.kod = kod;
        this.name = name;
        this.barcode = barcode;
        this.minCount = minCount;
        this.active = active;
        this.mobile = mobile;
        this.brandId = brandId;
        this.productCategoryId = productCategoryId;
        this.measureId = measureId;
        this.attachmentId = attachmentId;
    }

    public ProductTemplateDto(UUID id, String kod, String name, String barcode, Integer minCount, boolean active, boolean mobile, Brand brand, ProductCategory productCategory, Measure measure, UUID attachmentId) {
        this.id = id;
        this.kod = kod;
        this.name = name;
        this.barcode = barcode;
        this.minCount = minCount;
        this.active = active;
        this.mobile = mobile;
        this.brand = brand;
        this.productCategory = productCategory;
        this.measure = measure;
        this.attachmentId = attachmentId;
    }


    public ProductTemplateDto(UUID id, String kod, String name, Brand brand, ProductCategory productCategory, Measure measure, Double count) {
        this.id = id;
        this.kod = kod;
        this.name = name;
        this.brand = brand;
        this.productCategory = productCategory;
        this.measure = measure;
        this.count = count;
    }
}
