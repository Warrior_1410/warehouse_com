package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Kassa;
import uz.gvs.entity.User;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MobilePaymentDto {
    private UUID id;
    private String contractNumber;
    private Timestamp contractTime;
    private User client;
    private UUID clientId;
    private User agent;
    private UUID agentId;
    private List<Kassa> kassa;
    private Set<UUID> kassaId;
    private boolean accepted;
    private double allPrice;

    public MobilePaymentDto(UUID clientId, UUID agentId, Set<UUID> kassaId, boolean accepted, double allPrice){
        this.clientId = clientId;
        this.agentId = agentId;
        this.kassaId = kassaId;
        this.accepted = accepted;
        this.allPrice = allPrice;
    }

    public MobilePaymentDto(UUID id, String contractNumber, Timestamp contractTime, User client, User agent, List<Kassa> kassa, boolean accepted, double allPrice){
        this.id = id;
        this.contractNumber = contractNumber;
        this.contractTime = contractTime;
        this.client = client;
        this.agent = agent;
        this.kassa = kassa;
        this.accepted = accepted;
        this.allPrice = allPrice;
    }

}
