package uz.gvs.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.gvs.entity.Region;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionDto {

    private Integer id;
    private String name;
    private String description;
    private boolean active;

    //REQ
    private Integer regionId;

    //RES
    private Region region;

    public RegionDto(String name, String description, boolean active) {
        this.name = name;
        this.description = description;
        this.active = active;
    }

    //REQ
    public RegionDto(String name, String description, boolean active, Integer regionId) {
        this.name = name;
        this.description = description;
        this.active = active;
        this.regionId = regionId;
    }
    //RES

    public RegionDto(Integer id, String name, String description, boolean active, Region region) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.region = region;
    }

    public RegionDto(Integer id, String name, String description, boolean active, Integer regionId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.regionId = regionId;
    }
}
