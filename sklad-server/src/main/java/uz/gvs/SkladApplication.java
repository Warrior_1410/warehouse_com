package uz.gvs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkladApplication {
    //بسم الله الرحمن الرحيم
    public static void main(String[] args) {
        SpringApplication.run(SkladApplication.class, args);
    }

}
