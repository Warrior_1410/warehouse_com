package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.PatchLoad;
import uz.gvs.payload.ProductCategoryDto;
import uz.gvs.payload.ProductTemplateDto;
import uz.gvs.repository.ProductRepository;
import uz.gvs.repository.ProductTemplateRepository;
import uz.gvs.service.ProductTemplateService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/productTemplate")
public class ProductTemplateController {
    @Autowired
    ProductTemplateService productTemplateService;
    @Autowired
    ProductTemplateRepository productTemplateRepository;
    @Autowired
    ProductRepository productRepository;
    @PostMapping
    public HttpEntity<?> create(@RequestBody ProductTemplateDto productTemplateDto) {
        ApiResponse apiResponse = productTemplateService.create(productTemplateDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }
    //GETONE
    @GetMapping("/{id}")
    public HttpEntity<?> getOne(@PathVariable UUID id){
        ProductTemplate productTemplate = productTemplateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getProTemp"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,productTemplateService.getOne(productTemplate)));
    }
    //  GETLIST
    @GetMapping
    public HttpEntity<?> getList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                       @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, productTemplateService.getList(page, size)));
    }
    //  UPDATE
    @PutMapping("/{id}")
    public HttpEntity<?> update(@PathVariable UUID id, @RequestBody ProductTemplateDto productTemplateDto){
        ApiResponse apiResponse = productTemplateService.update(id, productTemplateDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }


    @PatchMapping("/{id}")
    public HttpEntity<?> updateBoolean(@PathVariable UUID id, @RequestBody PatchLoad patchLoad){
        ApiResponse apiResponse = productTemplateService.updateBoolean(id,patchLoad);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    //  DELETE
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id){
        ApiResponse apiResponse = productTemplateService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/byWarehouse/{id}")
    public HttpEntity<?> getByWarehouse(@PathVariable Integer id){
        List<ProductTemplateDto> byWarehouse = productTemplateService.getByWarehouse(id);
        return ResponseEntity.ok(byWarehouse);
    }

    @GetMapping("/searchBy/{name}")
    public HttpEntity<?> getProductTemplateAllByName(@PathVariable String name,@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                                          @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size){
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,productTemplateService.getAllByName(name,page,size)));
    }
    @GetMapping("/all")
    public HttpEntity<?> getProductTemplateAll(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                               @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size){
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,productTemplateService.getAll(page,size)));
    }

    @GetMapping("/search/{name}")
    public List<ProductTemplate> getProductTemplateSearch(@PathVariable String name){
        return productTemplateService.getProductTemplateSearch(name);
    }
}
