package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Region;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.RegionDto;
import uz.gvs.repository.RegionRepository;
import uz.gvs.service.RegionService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/region")
public class RegionController {
    @Autowired
    RegionService regionService;
    @Autowired
    RegionRepository regionRepository;

    //  CREATE
    @PostMapping
    public HttpEntity<?> create(@RequestBody RegionDto regionDto) {
        ApiResponse apiResponse = regionService.create(regionDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    //      GETONE
    @GetMapping("/{id}")
    public HttpEntity<?> getRegion(@PathVariable Integer id) {
        Region region = regionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getRegion"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, regionService.getRegion(region)));
    }

    //  GETLIST
    @GetMapping
    public HttpEntity<?> getRegionList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                       @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, regionService.getRegionList(page, size)));
    }

    //      UPDATE
    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id, @RequestBody RegionDto regionDto) {
        ApiResponse apiResponse = regionService.edit(id, regionDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @PatchMapping("/{id}")
    public HttpEntity<?> update(@PathVariable Integer id) {
        ApiResponse apiResponse = regionService.update(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    //  DELETE
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id) {
        ApiResponse apiResponse = regionService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/search/{name}")
    public List<Region> getRegionSearch(@PathVariable String name){
        return regionService.getRegionSearch(name);
    }

}
