package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Income;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.IncomeDto;
import uz.gvs.repository.IncomeRepository;
import uz.gvs.service.IncomeService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/income")
public class IncomeController {
    @Autowired
    IncomeService incomeService;
    @Autowired
    IncomeRepository incomeRepository;

    @PostMapping
    public HttpEntity<?> addIncome(@RequestBody IncomeDto incomeDto) {
        ApiResponse apiResponse = incomeService.addIncome(incomeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getIncome(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                   @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, incomeService.getIncomeList(page, size)));
    }

    @GetMapping("/byDate")
    public HttpEntity<?> getIncomeDate(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                       @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size,
                                       @RequestParam(value = "date", defaultValue = "") String date) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, incomeService.getIncomeListByDate(page, size, date)));
    }

    @GetMapping("/{id}")
    public IncomeDto getIncomeOne(@PathVariable String id) {
        try {
            Income get_income = incomeRepository.findByContractNumber(id).orElseThrow(() -> new ResourceNotFoundException("get Income"));
            return incomeService.getIncome(get_income);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/search/{contractNumber}")
    public List<IncomeDto> getIncomeSearch(@PathVariable Integer contractNumber) {
        return incomeService.searchIncome(contractNumber.toString());
    }


//    @GetMapping("/byDate/{date}")
//    public List<IncomeDto> getIncomeDate(@PathVariable Date date) {
//        return incomeService.searchIncomeByDate(date);
//    }

}