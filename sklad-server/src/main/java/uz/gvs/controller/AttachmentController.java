package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.gvs.payload.ApiResponse;
import uz.gvs.service.AttachmentService;
import uz.gvs.utils.MessageConst;

import java.util.UUID;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {
    @Autowired
    AttachmentService attachmentService;

    @PostMapping("/upload")
    public HttpEntity<?> uploadFile(MultipartHttpServletRequest request) {
        return ResponseEntity.ok(attachmentService.uploadFile(request));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id) {
        return attachmentService.getFile(id);
    }

    @GetMapping("getFile/{id}")
    public HttpEntity<?> getFileCode(@PathVariable UUID id) {
        String attachmentContent = attachmentService.getAttachmentContent(id);
                return ResponseEntity.ok(attachmentContent);
    }

    @DeleteMapping("/all/{id}")
    public HttpEntity<?> deleteFile(@PathVariable UUID id){
        return ResponseEntity.ok(attachmentService.deleteAttachment("all",id));
    }

    @DeleteMapping("/byBrand/{id}")
    public HttpEntity<?> deleteFileByBrand(@PathVariable UUID id){
        return ResponseEntity.ok(attachmentService.deleteAttachment("brand",id));
    }
    @DeleteMapping("/byCategory/{id}")
    public HttpEntity<?> deleteFileByCategory(@PathVariable UUID id){
        return ResponseEntity.ok(attachmentService.deleteAttachment("category",id));
    }
    @DeleteMapping("/byTemplate/{id}")
    public HttpEntity<?> deleteFileByTemplate(@PathVariable UUID id){
        return ResponseEntity.ok(attachmentService.deleteAttachment("template",id));
    }
}
