package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Bank;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.repository.BankRepository;
import uz.gvs.service.BankService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/bank")
public class BankController {
    @Autowired
    BankService bankService;
    @Autowired
    BankRepository bankRepository;

    @PostMapping
    public HttpEntity<?> addBank(@RequestBody BankDto bankDto) {
        ApiResponse apiResponse = bankService.addBank(bankDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getBankList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, bankService.getBankList(page, size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getBank(@PathVariable Integer id) {
        Bank bank = bankRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getBank"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, bankService.getBank(bank)));
    }
    @GetMapping("/search/{name}")
    public List<Bank> getBankSearch(@PathVariable String name){
        return bankService.getBankSearch(name);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editBank(@PathVariable Integer id, @RequestBody BankDto bankDto) {
        ApiResponse apiResponse = bankService.editBank(id, bankDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @PatchMapping("/{id}")
    public HttpEntity<?> updateBank(@PathVariable Integer id) {
        ApiResponse apiResponse = bankService.updateBank(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteBank(@PathVariable Integer id) {
        ApiResponse apiResponse = bankService.deleteBank(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
