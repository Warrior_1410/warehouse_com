package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Bank;
import uz.gvs.entity.KassaPaymentType;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.payload.KassaPaymentTypeDto;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.KassaPaymentTypeRepository;
import uz.gvs.service.BankService;
import uz.gvs.service.KassaPaymentTypeService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/kassaPaymentType")
public class KassaPaymentTypeController {
    @Autowired
    KassaPaymentTypeService kassaPaymentTypeService;
    @Autowired
    KassaPaymentTypeRepository kassaPaymentTypeRepository;

    @PostMapping
    public HttpEntity<?> addKassaPaymentType(@RequestBody KassaPaymentTypeDto kassaPaymentTypeDto){
        ApiResponse apiResponse = kassaPaymentTypeService.addKassaPaymentType(kassaPaymentTypeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getKassaPaymentTypeList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,kassaPaymentTypeService.getKassaPaymentTypeList(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getKassaPaymentType(@PathVariable Integer id){
        KassaPaymentType kassaPaymentType = kassaPaymentTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getKassaPaymentType"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,kassaPaymentTypeService.getKassaPaymentType(kassaPaymentType)));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editKassaPaymentType(@PathVariable Integer id, @RequestBody KassaPaymentTypeDto kassaPaymentTypeDto){
        ApiResponse apiResponse = kassaPaymentTypeService.editKassaPaymentType(id, kassaPaymentTypeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @PatchMapping("/{id}")
    public HttpEntity<?> updateKassaPaymentType(@PathVariable Integer id){
        ApiResponse apiResponse = kassaPaymentTypeService.updateKassaPaymentType(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteKassaPaymentType(@PathVariable Integer id){
        ApiResponse apiResponse = kassaPaymentTypeService.deleteKassaPaymentType(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/search/{name}")
    public List<KassaPaymentType> getKassaPaymentTypeSearch(@PathVariable String name){
        return kassaPaymentTypeService.getKassaPaymentTypeSearch(name);
    }
}
