package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.payload.ProductDto;
import uz.gvs.repository.WarehouseRepository;
import uz.gvs.service.ProductService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/{id}")
    public Double lastPrice(@PathVariable UUID id){
        return productService.getPrice(id);
    }

    @GetMapping("/allPrice")
    public HttpEntity<?> getAllPrice(){
        List<ProductDto> allPrice = productService.getAllPrice();
        return ResponseEntity.ok(allPrice);
    }

    @GetMapping("/allCount/{id}")
    public HttpEntity<?> getAllCount(@PathVariable Integer id){
        List<ProductDto> allCount = productService.getAllCount(id);
        return ResponseEntity.ok(allCount);
    }
}
