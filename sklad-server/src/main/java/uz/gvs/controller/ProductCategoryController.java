package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.ProductCategory;
import uz.gvs.entity.Region;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.PatchLoad;
import uz.gvs.payload.ProductCategoryDto;
import uz.gvs.payload.RegionDto;
import uz.gvs.repository.ProductCategoryRepository;
import uz.gvs.repository.RegionRepository;
import uz.gvs.service.ProductCategoryService;
import uz.gvs.service.RegionService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/productCategory")
public class ProductCategoryController {
    @Autowired
    ProductCategoryService productCategoryService;
    @Autowired
    ProductCategoryRepository productCategoryRepository;

    //  CREATE
    @PostMapping
    public HttpEntity<?> create(@RequestBody ProductCategoryDto productCategoryDto) {
        ApiResponse apiResponse = productCategoryService.create(productCategoryDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    //  GETONE
    @GetMapping("/{id}")
    public HttpEntity<?> getRegion(@PathVariable Integer id) {
        ProductCategory productCategory = productCategoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getProduct"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, productCategoryService.getProductCategory(productCategory)));
    }

    //  GETLIST
    @GetMapping
    public HttpEntity<?> getRegionList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                       @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, productCategoryService.getProductCategoryList(page, size)));
    }
    //  UPDATE
    @PutMapping("/{id}")
    public HttpEntity<?> update(@PathVariable Integer id, @RequestBody ProductCategoryDto productCategoryDto){
        ApiResponse apiResponse = productCategoryService.update(id, productCategoryDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }
    //
    @PatchMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id, @RequestBody PatchLoad patchLoad){
        ApiResponse apiResponse = productCategoryService.updateBoolean(id, patchLoad);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }
    //  DELETE
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id){
        ApiResponse apiResponse = productCategoryService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/search/{name}")
    public List<ProductCategory> getProductCategorySearch(@PathVariable String name){
        return productCategoryService.getProductCategorySearch(name);
    }
}
