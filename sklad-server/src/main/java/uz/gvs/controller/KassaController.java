package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Kassa;
import uz.gvs.entity.KassaPaymentType;
import uz.gvs.entity.User;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.KassaDto;
import uz.gvs.payload.KassaPaymentTypeDto;
import uz.gvs.repository.KassaPaymentTypeRepository;
import uz.gvs.repository.KassaRepository;
import uz.gvs.security.CurrentUser;
import uz.gvs.service.KassaPaymentTypeService;
import uz.gvs.service.KassaService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.UUID;

@RestController
@RequestMapping("/api/kassa")
public class KassaController {
    @Autowired
    KassaService kassaService;
    @Autowired
    KassaRepository kassaRepository;

    @PostMapping
    public HttpEntity<?> addKassa(@RequestBody KassaDto kassaDto, @CurrentUser User user){
        ApiResponse apiResponse = kassaService.addKassa(kassaDto,user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getKassaList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,kassaService.getKassaList(page,size)));

    }

    @GetMapping("/{id}")
    public HttpEntity<?> getKassa(@PathVariable UUID id){
        Kassa kassa = kassaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getKassa"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,kassaService.getKassa(kassa)));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editKassa(@PathVariable UUID id, @RequestBody KassaDto kassaDto){
        ApiResponse apiResponse = kassaService.editKassa(id, kassaDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteKassa(@PathVariable UUID id){
        ApiResponse apiResponse = kassaService.deleteKassa(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
