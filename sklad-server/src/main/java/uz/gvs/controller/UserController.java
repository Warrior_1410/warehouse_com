package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Role;
import uz.gvs.entity.User;
import uz.gvs.entity.enums.RoleName;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.UserDto;
import uz.gvs.repository.RoleRepository;
import uz.gvs.repository.UserRepository;
import uz.gvs.security.CurrentUser;
import uz.gvs.service.ApiResponseService;
import uz.gvs.service.AuthService;
import uz.gvs.service.UserService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;


@RestController
@RequestMapping("/api/auth")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    RoleRepository roleRepository;
    @PostMapping
    public HttpEntity<?> addUser(@RequestBody UserDto userDto, @CurrentUser User user) {
        ApiResponse apiResponse = userService.addUser(userDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }
    @GetMapping("/byRole/{name}")
    public HttpEntity<?> getUser(@PathVariable String name){
        Role role = null;
        if (name.equals("supplier")){
            role = roleRepository.findByRoleName(RoleName.SUPPLIER).orElseThrow(() -> new ResourceNotFoundException("getRole"));
        }
        else if (name.equals("agent")){
            role = roleRepository.findByRoleName(RoleName.AGENT).orElseThrow(() -> new ResourceNotFoundException("getRole"));
        }
        else if (name.equals("client")){
            role = roleRepository.findByRoleName(RoleName.CLIENT).orElseThrow(() -> new ResourceNotFoundException("getRole"));
        }
        else if (name.equals("user")){
            role = roleRepository.findByRoleName(RoleName.USER).orElseThrow(() -> new ResourceNotFoundException("getRole"));
        }

        return ResponseEntity.ok(userService.getUserRole(role));
    }

    @GetMapping("/all")
    public HttpEntity<?> getAllUsers(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size){
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,userService.getAllUser(page,size)));
    }
}
