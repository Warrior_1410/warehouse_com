package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Transfer;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.IncomeDto;
import uz.gvs.payload.TransferDto;
import uz.gvs.repository.TransferRepository;
import uz.gvs.service.TranferService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/transfer")
public class TransferController {
    @Autowired
    TranferService tranferService;
    @Autowired
    TransferRepository transferRepository;
    @PostMapping
    public HttpEntity<?> addTransfer(@RequestBody TransferDto transferDto){
        ApiResponse apiResponse = tranferService.addTranser(transferDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOne(@PathVariable String id){
        Transfer transfer = transferRepository.findByContractNumber(id).orElseThrow(() -> new ResourceNotFoundException("geOne"));
        return ResponseEntity.ok(tranferService.getTransfer(transfer));
    }

    @GetMapping
    public HttpEntity<?> getTransferList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                   @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, tranferService.getTransferList(page, size)));
    }

    @GetMapping("/search/{contractNumber}")
    public List<TransferDto> getTransferSearch(@PathVariable Integer contractNumber){
        return tranferService.searchTransfer(contractNumber.toString());
    }
}
