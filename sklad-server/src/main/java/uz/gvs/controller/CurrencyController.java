package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.CashName;
import uz.gvs.entity.Currency;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.CashNameDto;
import uz.gvs.payload.CurrencyDto;
import uz.gvs.repository.CashNameRepository;
import uz.gvs.repository.CurrencyRepository;
import uz.gvs.service.CashNameService;
import uz.gvs.service.CurrencyService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.UUID;

@RestController
@RequestMapping("/api/currency")
public class CurrencyController {
    @Autowired
    CurrencyService currencyService;
    @Autowired
    CurrencyRepository currencyRepository;

    @PostMapping
    public HttpEntity<?> addCurrency(@RequestBody CurrencyDto currencyDto){
        ApiResponse apiResponse = currencyService.addCurrency(currencyDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getCurrencyList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,currencyService.getCurrencyList(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCurrency(@PathVariable UUID id){
        Currency currency = currencyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getCashName"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,currencyService.getCurrency(currency)));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCurrency(@PathVariable UUID id, @RequestBody CurrencyDto currencyDto){
        ApiResponse apiResponse = currencyService.editCurrency(id, currencyDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCurrency(@PathVariable UUID id){
        ApiResponse apiResponse = currencyService.deleteCurrency(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

}
