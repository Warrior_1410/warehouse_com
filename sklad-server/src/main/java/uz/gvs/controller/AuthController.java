package uz.gvs.controller;

import org.aspectj.bridge.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.User;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.JwtToken;
import uz.gvs.payload.ReqLogin;
import uz.gvs.repository.UserRepository;
import uz.gvs.security.CurrentUser;
import uz.gvs.security.JwtTokenProvider;
import uz.gvs.service.AuthService;
import uz.gvs.utils.MessageConst;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    private HttpEntity<?> login(@Valid @RequestBody ReqLogin reqLogin) {
        try {
            Authentication authentication = authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(reqLogin.getPhoneNumber(), reqLogin.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User user = (User) authentication.getPrincipal();
            String token = jwtTokenProvider.generateToken(user);
            return ResponseEntity
                    .status(201)
                    .body(new ApiResponse(MessageConst.LOGIN_SUCCESS, true, new JwtToken(token)));
        } catch (Exception e) {
            return ResponseEntity.status(401).body(new ApiResponse(MessageConst.LOGIN_ERROR, false));
        }
    }
    @GetMapping("/me")
    public HttpEntity<?> getUserMe(@CurrentUser User user) {
        return ResponseEntity.ok(user);
    }
}
