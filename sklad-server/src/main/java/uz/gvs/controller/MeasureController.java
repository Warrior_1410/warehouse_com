package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Measure;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.MeasureDto;
import uz.gvs.repository.MeasureRepository;
import uz.gvs.service.ApiResponseService;
import uz.gvs.service.MeasureService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/measure")
public class MeasureController {
    @Autowired
    MeasureService measureService;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    MeasureRepository measureRepository;

    // CREATE MEASURE
    @PostMapping
    public HttpEntity<?> addMeasure(@RequestBody MeasureDto measureDto) {
        ApiResponse apiResponse = measureService.addMeasure(measureDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    // GetList MEASURE
    @GetMapping
    public HttpEntity<?> getMeasureList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                        @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {

        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, measureService.getMeasureList(page, size)));
    }

    // GetOne MEASURE
    @GetMapping("/{id}")
    public HttpEntity<?> getMeasure(@PathVariable Integer id) {
        Measure measure = measureRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getMeasure"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, measureService.getMeasure(measure)));
    }

    // UPDATE MEASURE
    @PutMapping("/{id}")
    public HttpEntity<?> editMeasure(@PathVariable Integer id, @RequestBody MeasureDto measureDto) {
        ApiResponse apiResponse = measureService.editMeasure(id, measureDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @PatchMapping("/{id}")
    public HttpEntity<?> updateMeasure(@PathVariable Integer id) {
        ApiResponse apiResponse = measureService.updateMeasure(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    // DELETE MEASURE
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteMeasure(@PathVariable Integer id) {
        ApiResponse apiResponse = measureService.deleteMeasure(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/search/{name}")
    public List<Measure> getMeasureSearch(@PathVariable String name){
        return measureService.getMeasureSearch(name);
    }
}
