package uz.gvs.controller;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Income;
import uz.gvs.entity.Shipment;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.IncomeDto;
import uz.gvs.payload.RegionDto;
import uz.gvs.payload.ShipmentDto;
import uz.gvs.repository.ShipmentRepository;
import uz.gvs.service.ApiResponseService;
import uz.gvs.service.ShipmentService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/shipment")
public class ShipmentController {
    @Autowired
    ShipmentService shipmentService;
    @Autowired
    ShipmentRepository shipmentRepository;
    @Autowired
    ApiResponseService apiResponseService;

    @PostMapping
    public HttpEntity<?> addShipment(@RequestBody ShipmentDto shipmentDto) {
        ApiResponse apiResponse = shipmentService.addShipment(shipmentDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/{id}")
    public ShipmentDto getShipmentOne(@PathVariable String id) {
        try {
            Shipment get_shipment = shipmentRepository.findByContractNumber(id).orElseThrow(() -> new ResourceNotFoundException("get Shipment"));
            return shipmentService.getShipment(get_shipment);

        } catch (Exception e) {
            return null;
        }
    }

//    @GetMapping
//    public HttpEntity<?> getShipmentList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
//                                       @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
//        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, shipmentService.getShipmentList(page, size)));
//    }

    @GetMapping
    public HttpEntity<?> getAllShipment(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                        @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, shipmentService.getShipmentList(page, size)));
    }


    @GetMapping("/search/{contractNumber}")
    public List<ShipmentDto> getShipmentSearch(@PathVariable Integer contractNumber){
        return shipmentService.searchShipment(contractNumber.toString());
    }
//    //      UPDATE
//    @PutMapping("/{id}")
//    public HttpEntity<?> update(@PathVariable Integer id, @RequestBody RegionDto regionDto) {
//        ApiResponse apiResponse = shipmentService.update(id, regionDto);
//        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
//    }

    //  DELETE
//    @DeleteMapping("/{id}")
//    public HttpEntity<?> delete(@PathVariable UUID id) {
//        ApiResponse apiResponse = shipmentService.deleteShipment(id);
//        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
//    }
}
