package uz.gvs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Brand;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BrandDto;
import uz.gvs.payload.PatchLoad;
import uz.gvs.repository.BrandRepository;
import uz.gvs.service.BrandService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/brand")
public class    BrandController {
    @Autowired
    BrandService brandService;
    @Autowired
    BrandRepository brandRepository;

    // CREATE
    @PostMapping
    public HttpEntity<?> addBrand(@RequestBody BrandDto brandDto) {
        ApiResponse apiResponse = brandService.addBrand(brandDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    // GetList Brand
    @GetMapping
    public HttpEntity<?> getBrandList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                      @RequestParam(value = "size", defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, brandService.getBrandList(page, size)));
    }

    // GET ONE

    /**
     * bu ketmon uchun
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getBrand(@PathVariable Integer id) {
        Brand brand = brandRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getBrand"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS, true, brandService.getBrand(brand)));
    }

    // UPDATE

    /**
     *
     * @param id
     * @param brandDto
     * @return
     */
    @PutMapping("/{id}")
    public HttpEntity<?> updateBrand(@PathVariable Integer id, @RequestBody BrandDto brandDto) {
        ApiResponse apiResponse = brandService.updateBrand(id, brandDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }
    
    @PatchMapping("/{id}")
    public HttpEntity<?> editBoolean(@PathVariable Integer id,@RequestBody PatchLoad patchLoad){
        ApiResponse apiResponse = brandService.updateBoolean(id, patchLoad);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }
    // DELETE

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteBrand(@PathVariable Integer id) {
        ApiResponse apiResponse = brandService.deleteBrand(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }


    @GetMapping("/search/{name}")
    public List<Brand> getBrandSearch(@PathVariable String name){
        return brandService.getBrandSearch(name);
    }
}
