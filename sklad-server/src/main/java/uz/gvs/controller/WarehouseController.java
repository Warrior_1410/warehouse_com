package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.PatchLoad;
import uz.gvs.payload.PatchLoad;
import uz.gvs.payload.WarehouseDto;
import uz.gvs.repository.WarehouseRepository;
import uz.gvs.service.WarehouseService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import javax.sound.midi.Patch;
import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;
    @Autowired
    WarehouseRepository warehouseRepository;

    @PostMapping
    public HttpEntity<?> addWarehouse(@RequestBody WarehouseDto warehouseDto){
        ApiResponse apiResponse = warehouseService.addWarehouse(warehouseDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getWarehouseList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,warehouseService.getWarehouseList(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getWarehouse(@PathVariable Integer id){
        Warehouse warehouse = warehouseRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getWarehouse"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,warehouseService.getWarehouse(warehouse)));
    }

    @GetMapping("/search/{name}")
    public List<Warehouse> getWarehouseSearch(@PathVariable String name){
        return warehouseService.getWarehouseSearch(name);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editWarehouse(@PathVariable Integer id, @RequestBody WarehouseDto warehouseDto){
        ApiResponse apiResponse = warehouseService.editWarehouse(id, warehouseDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }
    @PatchMapping("/{id}")
    public HttpEntity<?> updateWarehouse(@PathVariable Integer id){
        ApiResponse apiResponse = warehouseService.updateWarehouse(id);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteBank(@PathVariable Integer id){
        ApiResponse apiResponse = warehouseService.deleteWarehouse(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
