package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Kassa;
import uz.gvs.entity.MobilePayment;
import uz.gvs.entity.User;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.KassaDto;
import uz.gvs.payload.MobilePaymentDto;
import uz.gvs.repository.KassaRepository;
import uz.gvs.repository.MobilePaymentRepository;
import uz.gvs.security.CurrentUser;
import uz.gvs.service.KassaService;
import uz.gvs.service.MobilePaymentService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.UUID;

@RestController
@RequestMapping("/api/mobilePayment")
public class MobilePaymentController {
    @Autowired
    MobilePaymentRepository mobilePaymentRepository;
    @Autowired
    MobilePaymentService mobilePaymentService;

    @PostMapping("/add")
    public HttpEntity<?> addMobilePayment(@RequestBody MobilePaymentDto mobilePaymentDto, @CurrentUser User user){
        ApiResponse apiResponse = mobilePaymentService.addMobilePayment(mobilePaymentDto,user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getMobilePaymentList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.SUCCESS_MESSAGE,true,mobilePaymentService.getMobilePaymentList(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getMobilePayment(@PathVariable UUID id){
        MobilePayment mobilePayment = mobilePaymentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getMobilePayment"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.SUCCESS_MESSAGE,true,mobilePaymentService.getMobilePayment(mobilePayment)));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editMobilePayment(@PathVariable UUID id, @RequestBody MobilePaymentDto mobilePaymentDto){
        ApiResponse apiResponse = mobilePaymentService.editMobilePayment(id, mobilePaymentDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteMobilePayment(@PathVariable UUID id){
        ApiResponse apiResponse = mobilePaymentService.deleteMobilePayment(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }
}
