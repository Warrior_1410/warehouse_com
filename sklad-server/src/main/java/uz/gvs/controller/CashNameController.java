package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.Bank;
import uz.gvs.entity.CashName;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.payload.CashNameDto;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.CashNameRepository;
import uz.gvs.service.BankService;
import uz.gvs.service.CashNameService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/cashName")
public class CashNameController {
    @Autowired
    CashNameService cashNameService;
    @Autowired
    CashNameRepository cashNameRepository;

    @PostMapping
    public HttpEntity<?> addCashName(@RequestBody CashNameDto cashNameDto){
        ApiResponse apiResponse = cashNameService.addCashName(cashNameDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getCashNameList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,cashNameService.getCashNameList(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCashName(@PathVariable Integer id){
        CashName cashName = cashNameRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getCashName"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,cashNameService.getCashName(cashName)));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCashName(@PathVariable Integer id, @RequestBody CashNameDto cashNameDto){
        ApiResponse apiResponse = cashNameService.editCashName(id, cashNameDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }
    @PatchMapping("/{id}")
    public HttpEntity<?> updateCashName(@PathVariable Integer id){
        ApiResponse apiResponse = cashNameService.updateCashName(id);
        return ResponseEntity.status(apiResponse.isSuccess()?202:409).body(apiResponse);
    }
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCashName(@PathVariable Integer id){
        ApiResponse apiResponse = cashNameService.deleteCashName(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/search/{name}")
    public List<CashName> getCashNameSearch(@PathVariable String name){
        return cashNameService.getCashNameSearch(name);
    }
}
