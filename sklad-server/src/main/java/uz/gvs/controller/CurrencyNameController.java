package uz.gvs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.gvs.entity.CurrencyName;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.CurrencyNameDto;
import uz.gvs.repository.CurrencyNameRepository;
import uz.gvs.service.CurrencyNameService;
import uz.gvs.utils.AppConstant;
import uz.gvs.utils.MessageConst;

import java.util.List;

@RestController
@RequestMapping("/api/currencyName")
public class CurrencyNameController {
    @Autowired
    CurrencyNameRepository currencyNameRepository;
    @Autowired
    CurrencyNameService currencyNameService;
    @PostMapping
    public HttpEntity<?> addCurrencyName(@RequestBody CurrencyNameDto currencyNameDto){
        ApiResponse apiResponse = currencyNameService.addCurrencyName(currencyNameDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<?> getCurrencyNameList(@RequestParam(value = "page", defaultValue = AppConstant.DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size",defaultValue = AppConstant.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,currencyNameService.getCurrencyNameList(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCurrencyName(@PathVariable Integer id){
        CurrencyName currencyName = currencyNameRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getCurrencyName"));
        return ResponseEntity.ok(new ApiResponse(MessageConst.GET_SUCCESS,true,currencyNameService.getCurrencyName(currencyName)));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCurrenct(@PathVariable Integer id, @RequestBody CurrencyNameDto currencyNameDto){
        ApiResponse apiResponse = currencyNameService.editCurrencyName(id, currencyNameDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteNameCurrency(@PathVariable Integer id){
        ApiResponse apiResponse = currencyNameService.deleteCurrencyName(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 204 : 409).body(apiResponse);
    }

    @GetMapping("/search/{name}")
    public List<CurrencyName> getCurrencyNameSearch(@PathVariable String name){
        return currencyNameService.getCurrencyNameSearch(name);
    }
}
