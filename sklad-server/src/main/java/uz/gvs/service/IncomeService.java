package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import uz.gvs.entity.Income;
import uz.gvs.entity.Product;
import uz.gvs.entity.Warehouse;
import uz.gvs.entity.enums.StatusEnum;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.IncomeDto;
import uz.gvs.payload.ProductDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.*;
import uz.gvs.utils.AppConstant;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IncomeService {
    @Autowired
    IncomeRepository incomeRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductService productService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ApiResponseService apiResponseService;

    public ApiResponse addIncome(IncomeDto incomeDto) {
        try {

            String maxContractNumber = incomeRepository.findMaxContractNumber();
            Integer integer = Integer.valueOf(maxContractNumber);

            Income income = new Income();
//            if (integer == 0) {
//                income.setContractNumber("1");
//            }
//            income.setContractNumber(String.valueOf(integer + 1));
//            income.setContractTime(Timestamp.valueOf(LocalDateTime.now()));
            income.setContractNumber(incomeDto.getContractNumber());
            income.setContractTime(Timestamp.valueOf(incomeDto.getContractTime()));

            List<Product> productList = new ArrayList<>();
            for (ProductDto productDto : incomeDto.getProductDtos()) {
                productDto.setWarehouseId(incomeDto.getWarehouseId());
                Product product = productService.addProduct(productDto);
                productList.add(product);
            }
            income.setProducts(productList);

            income.setResponsibleUser(userRepository.findById(incomeDto.getResponsibleUserId()).orElseThrow(() -> new ResourceNotFoundException("getResponsibleUser")));
            income.setReceiverUser(userRepository.findById(incomeDto.getReceiverUserId()).orElseThrow(() -> new ResourceNotFoundException("getReceiverUser")));
            income.setSupplierUser(userRepository.findById(incomeDto.getSupplierUserId()).orElseThrow(() -> new ResourceNotFoundException("getSupplierUser")));
            income.setWarehouse(warehouseRepository.findById(incomeDto.getWarehouseId()).orElseThrow(() -> new ResourceNotFoundException("getWarehouse")));
            income.setAmount(incomeDto.getAmount());
            income.setStatusEnum(StatusEnum.COUNTED);

            incomeRepository.save(income);

            return apiResponseService.savedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getIncomeList(int page, int size) {
        try {
            Page<Income> all = incomeRepository.findAll(PageRequest.of(page, size));
            return new ResPageable(
                    all.getTotalPages(),
                    all.getTotalElements(),
                    all.getNumber(),
                    all.getSize(),
                    all.get().map(this::getIncomeSearch).collect(Collectors.toList())
            );
        } catch (Exception e) {
            return null;
        }
    }

    public ResPageable getIncomeListByDate(int page, int size, String date) {
        try {
//            Page<Income> all = incomeRepository.findAllByDate(PageRequest.of(page, size));
            List<Income> all = incomeRepository.findAllByDate();
            for (int i = 0; i < all.toArray().length; i++) {
                System.out.println(all);
            }
            return null;
//            return new ResPageable(
//                    all.getTotalPages(),
//                    all.getTotalElements(),
//                    all.getNumber(),
//                    all.getSize(),
//                    all.get().map(this::getIncomeSearch).collect(Collectors.toList())
//            );
        } catch (Exception e) {
            return null;
        }
    }

    public IncomeDto getIncomeSearch(Income income) {
        return new IncomeDto(
                income.getContractNumber(),
                income.getResponsibleUser(),
                income.getReceiverUser(),
                income.getSupplierUser(),
                income.getWarehouse(),
                income.getStatusEnum(),
                income.getDescription()
        );
    }


    public IncomeDto getIncome(Income income) {
        try {
            return new IncomeDto(
                    income.getId(),
                    income.getContractNumber(),
                    income.getContractTime().toLocalDateTime(),
                    income.getProducts(),
                    income.getDescription(),
                    income.getResponsibleUser(),
                    income.getReceiverUser(),
                    income.getSupplierUser(),
                    income.getWarehouse(),
                    income.getAmount(),
                    income.getStatusEnum()
            );
        } catch (Exception e) {
            return null;
        }
    }

    public List<IncomeDto> searchIncome(String contractNumber) {
        List<Income> allIncomeSearch = incomeRepository.getAllIncomeSearch(contractNumber);

        List<IncomeDto> incomeDtoList = new ArrayList<>();
        for (Income incomeSearch : allIncomeSearch) {
            incomeDtoList.add(getIncomeSearch(incomeSearch));
        }
        return incomeDtoList;
    }


//    public List<IncomeDto> searchIncomeByDate(Date date) {
//
//        List<Income> allIncomeSearch = incomeRepository.getAllIncomeSearch("");
//
//        List<IncomeDto> incomeDtoList = new ArrayList<>();
//        for (Income incomeSearch : allIncomeSearch) {
//            incomeDtoList.add(getIncomeSearch(incomeSearch));
//        }
//        return incomeDtoList;
//    }


}
