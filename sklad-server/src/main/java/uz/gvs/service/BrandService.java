package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.Brand;
import uz.gvs.entity.Measure;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.payload.*;
import uz.gvs.repository.AttachmentContentRepository;
import uz.gvs.repository.AttachmentRepository;
import uz.gvs.repository.BrandRepository;
import uz.gvs.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BrandService {
    @Autowired
    BrandRepository brandRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    /**
     * Create Brand
     *
     * @return ApiResponse
     */
    public ApiResponse addBrand(BrandDto brandDto) {
        if (!brandRepository.existsByNameEqualsIgnoreCase(brandDto.getName())) {
            Brand brand = new Brand();
            brand.setName(brandDto.getName());
            brand.setDescription(brandDto.getDescription());
            brand.setActive(brandDto.isActive());
            brand.setMobile(brandDto.isMobile());
            brand.setAttachment(attachmentRepository.findById(brandDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
            brand.setUser(userRepository.findById(brandDto.getUserId()).orElseThrow(() -> new ResourceNotFoundException("getUser")));
            brandRepository.save(brand);
            return apiResponseService.savedResponse();
        }
        return apiResponseService.existsResponse();
    }
    // GetOne   Brand

    /**
     * Get Brand One
     *
     * @return getBrand
     */
    public BrandDto getBrand(Brand brand) {
        return new BrandDto(
                brand.getId(),
                brand.getName(),
                brand.getDescription(),
                brand.isActive(),
                brand.isMobile(),
                "",
                (brand.getAttachment() == null ? null : brand.getAttachment().getId()),
                brand.getUser());
    }

    //GET Brand List
    public ResPageable getBrandList(int page, int size) {
        try {
            PageRequest pageable = PageRequest.of(page, size);
            Page<Brand> brandPage = brandRepository.findAll(pageable);
            List<Brand> content = brandPage.getContent();
            List<BrandDto> brandDtoList = new ArrayList<>();
            for (int i = 0; i < brandPage.getContent().size(); i++) {
                brandDtoList.add(getBrand(brandPage.getContent().get(i)));
            }
            return new ResPageable(
                    brandPage.getTotalPages(),
                    brandPage.getTotalElements(),
                    page,
                    size,
                    brandPage.getContent().stream().map(this::getBrand));
        } catch (Exception e) {
            return null;
        }
    }

    //UPDATE

    /**
     * UPDATE
     * @return ApiResponse
     */
    public ApiResponse updateBrand(Integer id, BrandDto brandDto) {
        try {
            Optional<Brand> optionalBrand = brandRepository.findById(id);
            if (optionalBrand.isPresent()) {
                Brand brand = optionalBrand.get();
                if (!brandRepository.existsByNameEqualsIgnoreCaseAndIdNot(brandDto.getName(), id)) {
                    brand.setName(brandDto.getName());
                    brand.setDescription(brandDto.getDescription());
                    brand.setActive(brandDto.isActive());
                    if (brandDto.getAttachmentId() != null) {
                        brand.setAttachment(attachmentRepository.findById(brandDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttach")));
                    }
                    brand.setMobile(brandDto.isMobile());
                    if (brandDto.getUserId() != null) {
                        brand.setUser(userRepository.findById(brandDto.getUserId()).orElseThrow(() -> new ResourceNotFoundException("getUser")));
                    }
                    brandRepository.save(brand);
                    return apiResponseService.savedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.errorResponse();
        }
    }


    public ApiResponse updateBoolean(Integer id, PatchLoad patchLoad){
        try {
            Optional<Brand> optionalBrand = brandRepository.findById(id);
            if (optionalBrand.isPresent()){
                Brand brand = optionalBrand.get();
                if (patchLoad.getRequest().equals("active")) {
                    brand.setActive(!brand.isActive());
                    brandRepository.save(brand);
                } else if (patchLoad.getRequest().equals("mobile")) {
                    brand.setMobile(!brand.isMobile());
                    brandRepository.save(brand);
                }
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.existsResponse();
        }catch (Exception e){
            return  apiResponseService.tryErrorResponse();
        }
    }
    //DELETE
    public ApiResponse deleteBrand(Integer id) {
        try {
            brandRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }

    public List<Brand> getBrandSearch(String name){
        try {
            return brandRepository.getAllBrandSearch(name);
        }catch (Exception e){
            return null;
        }
    }

}
