package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Currency;
import uz.gvs.entity.Kassa;
import uz.gvs.entity.User;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.CurrencyDto;
import uz.gvs.payload.KassaDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.CashNameRepository;
import uz.gvs.repository.KassaPaymentTypeRepository;
import uz.gvs.repository.KassaRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class KassaService {
    @Autowired
    CashNameRepository cashNameRepository;
    @Autowired
    KassaPaymentTypeRepository kassaPaymentTypeRepository;
    @Autowired
    KassaRepository kassaRepository;
    @Autowired
    ApiResponseService apiResponseService;

    public ApiResponse addKassa(KassaDto kassaDto, User user) {
        try {
            Kassa kassa = new Kassa();
            kassa.setPaymentTime(Timestamp.valueOf(LocalDateTime.now()));
            kassa.setUnity(kassaDto.getUnity());
            kassa.setDescription(kassaDto.getDescription());
            kassa.setAmount(kassaDto.getAmount());
            kassa.setAllSum(kassaDto.getAllSum());
            kassa.setCashName(cashNameRepository.findById(kassaDto.getCashNameId()).orElseThrow(() -> new ResourceNotFoundException("getCashName")));
            kassa.setKassaPaymentType(kassaPaymentTypeRepository.findById(kassaDto.getKassaPaymentTypeId()).orElseThrow(() -> new ResourceNotFoundException("getKassaPaymentType")));
            kassa.setUser(user);
            kassaRepository.save(kassa);
            return apiResponseService.savedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getKassaList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Kassa> allKassa = kassaRepository.findAll(pageable);

            List<KassaDto> kassaDtoList = new ArrayList<>();
            for (int i = 0; i < allKassa.getContent().size(); i++) {
                kassaDtoList.add(
                        getKassa(allKassa.getContent().get(i))
                );
            }
            return new ResPageable(
                    allKassa.getTotalPages(),
                    allKassa.getTotalElements(),
                    page,
                    size,
                    allKassa.getContent().stream().map(this::getKassa));
        } catch (Exception e) {
            return null;
        }
    }

    public KassaDto getKassa(Kassa kassa) {
        return new KassaDto(
                kassa.getId(),
                kassa.getCashName(),
                kassa.getAmount(),
                kassa.getUnity(),
                kassa.getAllSum(),
                kassa.getPaymentTime(),
                kassa.getKassaPaymentType(),
                kassa.getUser(),
                kassa.getDescription()
        );
    }

    public ApiResponse editKassa(UUID id, KassaDto kassaDto) {
        try {
            Optional<Kassa> optionalKassa = kassaRepository.findById(id);
            if (optionalKassa.isPresent()) {
                Kassa kassa = optionalKassa .get();
                kassa.setAmount(kassaDto.getAmount());
                kassa.setUnity(kassa.getUnity());
                kassa.setAllSum(kassaDto.getAllSum());
                if (kassaDto.getCashNameId() != null) {
                    kassa.setCashName(
                            cashNameRepository.findById(kassaDto.getCashNameId()).orElseThrow(() -> new ResourceNotFoundException("getCashName"))
                    );
                }
                if (kassaDto.getKassaPaymentTypeId() != null) {
                    kassa.setKassaPaymentType(
                            kassaPaymentTypeRepository.findById(kassaDto.getKassaPaymentTypeId()).orElseThrow(() -> new ResourceNotFoundException("getKassaPaymentType"))
                    );
                }
                kassaRepository.save(kassa);

                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse deleteKassa(UUID id) {
        try {
            kassaRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }
}
