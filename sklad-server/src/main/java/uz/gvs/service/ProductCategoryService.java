package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.ProductCategory;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Region;
import uz.gvs.payload.*;
import uz.gvs.repository.AttachmentContentRepository;
import uz.gvs.repository.AttachmentRepository;
import uz.gvs.repository.ProductCategoryRepository;
import uz.gvs.repository.RegionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductCategoryService {
    @Autowired
    ProductCategoryRepository productCategoryRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    /**
     * @return ApiResponse
     */
    public ApiResponse create(ProductCategoryDto productCategoryDto) {
        try {
            if (productCategoryDto.getCategoryId() == null) {
                Optional<ProductCategory> optionalProductCategory = productCategoryRepository.productCategory(productCategoryDto.getName());
                if (optionalProductCategory.isPresent()) {
                    return apiResponseService.existsResponse();
                } else {
                    ProductCategory productCategory = new ProductCategory();
                    productCategory.setName(productCategoryDto.getName());
                    productCategory.setDescription(productCategoryDto.getDescription());
                    productCategory.setActive(productCategoryDto.isActive());
                    productCategory.setMobile(productCategoryDto.isMobile());
                    productCategory.setAttachment(attachmentRepository.findById(productCategoryDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
                    productCategoryRepository.save(productCategory);
                    return apiResponseService.savedResponse();
                }
            } else if (productCategoryDto.getCategoryId() != null) {
                if (productCategoryRepository.existsByNameEqualsIgnoreCaseAndIdNot(productCategoryDto.getName(), productCategoryDto.getCategoryId())) {
                    return apiResponseService.existsResponse();
                } else {
                    ProductCategory productCategory = new ProductCategory();
                    productCategory.setName(productCategoryDto.getName());
                    productCategory.setDescription(productCategoryDto.getDescription());
                    productCategory.setActive(productCategoryDto.isActive());
                    productCategory.setMobile(productCategoryDto.isMobile());
                    productCategory.setAttachment(attachmentRepository.findById(productCategoryDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
                    productCategory.setCategory(productCategoryRepository.findById(productCategoryDto.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getCategory")));
                    productCategoryRepository.save(productCategory);
                    return apiResponseService.savedResponse();
                }
            } else {
                return apiResponseService.errorResponse();
            }
        } catch (
                Exception e) {
            return apiResponseService.tryErrorResponse();
        }

    }

    /**
     * GETONE REGION
     * RES_Region
     *
     * @return ApiResponse
     */
    public ProductCategoryDto getProductCategory(ProductCategory productCategory) {
        return new ProductCategoryDto(
                productCategory.getId(),
                productCategory.getName(),
                productCategory.getDescription(),
                productCategory.isActive(),
                productCategory.isMobile(),
                productCategory.getAttachment(),
                productCategory.getCategory());
    }

    /**
     * GETLIST
     * RES_REGION
     */
    public ResPageable getProductCategoryList(int page, int size) {
        try {
            Page<ProductCategory> productCategoryPage = productCategoryRepository.findAll(PageRequest.of(page, size));
            List<ProductCategory> content = productCategoryPage.getContent();
            List<ProductCategoryDto> productCategoryDtoList = new ArrayList<>();
            for (int i = 0; i < productCategoryPage.getContent().size(); i++) {
                productCategoryDtoList.add(getProductCategory(productCategoryPage.getContent().get(i)));
            }
            return new ResPageable(
                    productCategoryPage.getTotalPages(),
                    productCategoryPage.getTotalElements(),
                    page,
                    size,
                    productCategoryPage.getContent().stream().map(this::getProductCategory));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * UPDATE
     * REQ_REGION REGIONDTO
     *
     * @return ApiResponse
     */
    public ApiResponse update(Integer id, ProductCategoryDto productCategoryDto) {
        try {
            Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(id);

            if (optionalProductCategory.isPresent()) {
                ProductCategory productCategory = optionalProductCategory.get();
                if (!productCategoryRepository.existsByNameEqualsIgnoreCaseAndIdNotAndCategory_Id(productCategoryDto.getName(), id, productCategoryDto.getCategoryId())) {
                    productCategory.setName(productCategoryDto.getName());
                    productCategory.setDescription(productCategoryDto.getDescription());
                    productCategory.setActive(productCategoryDto.isActive());
                    productCategory.setMobile(productCategoryDto.isMobile());
                    if (productCategoryDto.getAttachmentId() != null){
                        productCategory.setAttachment(attachmentRepository.findById(productCategoryDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
                    }

                    if (productCategory.getCategory() == null && productCategoryDto.getCategoryId() != null){
                        productCategory.setCategory(productCategoryRepository.findById(productCategoryDto.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getCategory")));
                    }

                    productCategoryRepository.save(productCategory);
                    return apiResponseService.savedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }




    public ApiResponse updateBoolean(Integer id, PatchLoad patchLoad){
        try {
            Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(id);
            if (optionalProductCategory.isPresent()){
                ProductCategory productCategory = optionalProductCategory.get();
                if (patchLoad.getRequest().equals("active")) {
                    productCategory.setActive(!productCategory.isActive());
                    productCategoryRepository.save(productCategory);
                } else if (patchLoad.getRequest().equals("mobile")) {
                    productCategory.setMobile(!productCategory.isMobile());
                    productCategoryRepository.save(productCategory);
                }
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.existsResponse();
        }catch (Exception e){
            return  apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse delete(Integer id) {
        try {
            productCategoryRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }


    public List<ProductCategory> getProductCategorySearch(String name){
        try {
            return productCategoryRepository.getAllProductCategorySearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
