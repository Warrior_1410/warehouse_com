package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import uz.gvs.entity.Role;
import uz.gvs.entity.User;
import uz.gvs.entity.Warehouse;
import uz.gvs.entity.enums.RoleName;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.ResPageable;
import uz.gvs.payload.UserDto;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.RegionRepository;
import uz.gvs.repository.RoleRepository;
import uz.gvs.repository.UserRepository;
import uz.gvs.security.CurrentUser;

import java.awt.print.Pageable;
import java.util.*;
import java.util.stream.Collectors;

import static uz.gvs.entity.enums.RoleName.*;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    BankRepository bankRepository;

    public ApiResponse addUser(@RequestBody UserDto userDto, @CurrentUser User currentUser) {
        try {
            Optional<User> byPhoneNumber = userRepository.findByPhoneNumber(userDto.getPhoneNumber());
            if (byPhoneNumber.isPresent() || userDto.getPhoneNumber().isEmpty()) {
                return apiResponseService.existsResponse();
            }
            if (userDto.getPassword().isEmpty()) {
                return apiResponseService.unEnoughData();
            }
            User user = makeUser(userDto);
            switch (RoleName.valueOf(userDto.getRoleName().toString())) {
                case CLIENT:
                    user.setRoles(roleRepository.findAllByRoleName(CLIENT));
                case SUPPLIER:
                    if (userDto.getBankId() != null) {
                        user.setBank(bankRepository.findById(userDto.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("get bank")));
                    }
                    user.setRegion(regionRepository.findById(userDto.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("get region")));
                    user.setRoles(roleRepository.findAllByRoleName(SUPPLIER));
                    break;
                case AGENT:
                    user.setRoles(roleRepository.findAllByRoleName(AGENT));
                    break;
                case FOUNDER:
                    user.setRoles(roleRepository.findAllByRoleName(FOUNDER));
                    break;
                case ADMIN:
                    user.setRoles(roleRepository.findAllByRoleName(ADMIN));
                    break;
                default:
                    return apiResponseService.notFoundResponse();
            }
            userRepository.save(user);
            return apiResponseService.savedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public Set<UserDto> getUserRole(Role role) {
        try {
            List<User> usersByRoles = userRepository.findUsersByRoles(role);
            if (!usersByRoles.isEmpty()) {
                Set<UserDto> userSet = new HashSet<>();
                for (User user : usersByRoles) {
                    UserDto userDto = new UserDto(
                            user.getId(),
                            user.getFullName()
                    );
                    userSet.add(userDto);
                }
                return userSet;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }


    public User makeUser(UserDto userDto) {
        User user = new User();
        user.setFullName(userDto.getFullName());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRoles(roleRepository.findAllByRoleName(CLIENT));
        user.setPhoneNumber(userDto.getPhoneNumber());
        if (userDto.getRegionId() != null && userDto.getRegionId() > 0)
            user.setRegion(regionRepository.findById(userDto.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("get Region")));
        return user;
    }


    public ResPageable getAllUser(int page, int size){
        try {
            Page<User> all = userRepository.findAll(PageRequest.of(page, size));
            return new ResPageable(
                    all.getTotalPages(),
                    all.getTotalElements(),
                    all.getNumber(),
                    all.getSize(),
                    all.get().map(this::getUser).collect(Collectors.toList())
            );
        } catch (Exception e) {
            return null;
        }
    }

    public UserDto getUser(User user){
        return new UserDto(
                user.getId(),
                user.getFullName(),
                user.getPhoneNumber(),
                user.getDescription(),
                user.getAddress(),
                user.getPhoneNumber2(),
                user.getRegion(),
                user.getContact(),
                user.getBank(),
                user.getAccount()
        );
    }
}
