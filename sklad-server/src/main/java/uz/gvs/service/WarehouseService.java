package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.*;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.WarehouseRepository;
import uz.gvs.utils.MessageConst;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WarehouseService {
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ApiResponseService apiResponseService;

    public ApiResponse addWarehouse(WarehouseDto warehouseDto) {
        try {
            boolean existsByName = warehouseRepository.existsByNameEqualsIgnoreCase(warehouseDto.getName());
            if (!existsByName) {
                Warehouse warehouse = new Warehouse();
                warehouse.setName(warehouseDto.getName());
                warehouse.setDescription(warehouseDto.getDescription());
                warehouse.setActive(warehouse.isActive());
                warehouseRepository.save(warehouse);
                return apiResponseService.savedResponse();
            } else {
                return apiResponseService.existsResponse();
            }
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getWarehouseList(int page, int size) {
        try {
            Page<Warehouse> all = warehouseRepository.findAll(PageRequest.of(page, size));
            return new ResPageable(
                            all.getTotalPages(),
                            all.getTotalElements(),
                            all.getNumber(),
                            all.getSize(),
                            all.get().map(this::getWarehouse).collect(Collectors.toList())
                    );
        } catch (Exception e) {
            return null;
        }
    }

    public WarehouseDto getWarehouse(Warehouse warehouse) {
        return new WarehouseDto(
                warehouse.getId(),
                warehouse.getName(),
                warehouse.getDescription(),
                warehouse.isActive()
        );
    }

    public ApiResponse editWarehouse(Integer id, WarehouseDto warehouseDto) {
        try {
            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);
            if (optionalWarehouse.isPresent()) {
                if (!warehouseRepository.existsByNameEqualsIgnoreCaseAndIdNot(warehouseDto.getName(), id)) {
                    Warehouse warehouse = optionalWarehouse.get();
                    warehouse.setName(warehouseDto.getName());
                    warehouse.setDescription(warehouseDto.getDescription());
                    warehouse.setActive(warehouseDto.isActive());
                    warehouseRepository.save(warehouse);
                    return apiResponseService.updatedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse updateWarehouse(Integer id){
        try {
            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(id);
            if (optionalWarehouse.isPresent()){
                Warehouse warehouse = optionalWarehouse.get();
                warehouse.setActive(!warehouse.isActive());
                warehouseRepository.save(warehouse);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.existsResponse();
        }catch (Exception e){
            return  apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse deleteWarehouse(Integer id) {
        try {
            warehouseRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public List<Warehouse> getWarehouseSearch(String name){
        try {
            return warehouseRepository.getAllWarehouseSearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
