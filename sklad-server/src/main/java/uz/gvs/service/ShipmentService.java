package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import uz.gvs.entity.*;
import uz.gvs.payload.*;
import uz.gvs.repository.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ShipmentService {
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderProductService orderProductService;
    @Autowired
    ShipmentRepository shipmentRepository;
    @Autowired
    ProductTemplateRepository productTemplateRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductService productService;

    //ADD
    public ApiResponse addShipment(ShipmentDto shipmentDto) {
        try {
            Shipment shipment = new Shipment();
//            String maxContractNumber = shipmentRepository.findMaxContractNumber();
//            Integer integer = Integer.valueOf(maxContractNumber);
//
//            if (integer == 0) {
//                shipment.setContractNumber("1");
//            }
//            shipment.setContractNumber(String.valueOf(integer + 1));
//            shipment.setContractTime(Timestamp.valueOf(LocalDateTime.now()));

            shipment.setContractNumber(shipmentDto.getContractNumber());
            shipment.setContractTime(Timestamp.valueOf(shipmentDto.getContractTime()));

            shipment.setWarehouse(warehouseRepository.findById(shipmentDto.getWarehouseId()).orElseThrow(() -> new ResourceNotFoundException("getWarehouse")));
            shipment.setResponsibleUser(userRepository.findById(shipmentDto.getResponsibleUserId()).orElseThrow(() -> new ResourceNotFoundException("getUserResponsible")));
            shipment.setReceiverUser(userRepository.findById(shipmentDto.getReceiverUserId()).orElseThrow(() -> new ResourceNotFoundException("getReceiverUser")));
            shipment.setSupplierUser(userRepository.findById(shipmentDto.getSupplierUserId()).orElseThrow(() -> new ResourceNotFoundException("getSupplierUser")));
            shipment.setAllSum(shipmentDto.getAllSum());
            shipment.setCurrency(shipmentDto.getCurrency());//
            shipment.setPaymentSumPercent(shipmentDto.getPaymentSumPercent());
            shipment.setPaymentAmount(shipmentDto.getPaymentAmount());
            shipment.setPaymentTotal(shipmentDto.getPaymentTotal());

            List<OrderProduct> orderProductList = new ArrayList<>();
            //mahsulot sonini yetarli ekanligini tekshirish uchun
            for (OrderProductDto orderProductDto : shipmentDto.getOrderProductDto()) {
                Double allLeftOver = productRepository.getAllLeftOver(orderProductDto.getProductTemplateId());
                if (orderProductDto.getCount() > allLeftOver) {
                    return apiResponseService.unEnoughProduct();
                }
            }
            // mahsulotni ombordan ayrish uchun
            for (OrderProductDto orderProductDto : shipmentDto.getOrderProductDto()) {
                OrderProduct orderProduct = new OrderProduct();

                List<Product> allProductLeftOver = productRepository.getAllProductLeftOver(orderProductDto.getProductTemplateId(), shipmentDto.getWarehouseId());
                List<Product> productList = new ArrayList<>();

                double needAmount = orderProductDto.getCount();
                for (Product product : allProductLeftOver) {
                    if (needAmount == 0) {
                        break;
                    }
                    if (product.getLeftover() > needAmount) {
                        product.setLeftover(product.getLeftover() - needAmount);
                        needAmount = 0;
                    } else if (product.getLeftover() == needAmount) {
                        product.setLeftover(0);
                        needAmount = 0;
                    } else {
                        needAmount = needAmount - product.getLeftover();
                        product.setLeftover(0);
                    }
                    productList.add(product);
                }
                List<Product> savedProductList = productRepository.saveAll(productList);

                orderProduct.setProduct(savedProductList);

                OrderProduct savedOrderProduct = orderProductService.addOrderProduct(orderProductDto, savedProductList);
                orderProductList.add(savedOrderProduct);
            }
            shipment.setOrderProducts(orderProductList);
            shipment.setActive(true);


            shipmentRepository.save(shipment);
            return apiResponseService.savedResponse();

        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    // DELETE MEASURE
    public ApiResponse deleteShipment(UUID id) {
        try {
            shipmentRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }

    public ShipmentDto getShipment(Shipment shipment) {
        try {
            return new ShipmentDto(
                    shipment.getId(),
                    shipment.getContractNumber(),
                    shipment.getContractTime().toLocalDateTime(),
                    shipment.getWarehouse(),
                    shipment.getResponsibleUser(),
                    shipment.getReceiverUser(),
                    shipment.getSupplierUser(),
                    shipment.getOrderProducts(),
                    shipment.getAllSum(),
                    shipment.getPaymentSumPercent(),
                    shipment.getCurrency(),
                    shipment.getPaymentAmount(),
                    shipment.getPaymentTotal()
            );
        } catch (Exception e) {
            return null;
        }
    }

    public ResPageable getShipmentList(int page, int size) {
        try {
            Page<Shipment> all = shipmentRepository.findAll(PageRequest.of(page, size));
            return new ResPageable(
                    all.getTotalPages(),
                    all.getTotalElements(),
                    all.getNumber(),
                    all.getSize(),
                    all.get().map(this::getShipmentSearch).collect(Collectors.toList())
            );
        } catch (Exception e) {
            return null;
        }
    }

    public ShipmentDto getShipmentSearch(Shipment shipment){
        return new ShipmentDto(
                shipment.getContractNumber(),
                shipment.getWarehouse(),
                shipment.getResponsibleUser(),
                shipment.getReceiverUser(),
                shipment.getSupplierUser(),
                shipment.getAllSum()
        );
    }

    public List<ShipmentDto> searchShipment(String contractNumber){
        List<Shipment> allShipmentSearch = shipmentRepository.getAllShipmentSearch(contractNumber);

        List<ShipmentDto> shipmentDtoList = new ArrayList<>();
        for (Shipment shipment : allShipmentSearch) {
            shipmentDtoList.add(getShipmentSearch(shipment));
        }
        return shipmentDtoList;
    }
}
