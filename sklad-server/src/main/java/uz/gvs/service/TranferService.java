package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Income;
import uz.gvs.entity.Product;
import uz.gvs.entity.Shipment;
import uz.gvs.entity.Transfer;
import uz.gvs.payload.*;
import uz.gvs.repository.ProductRepository;
import uz.gvs.repository.TransferRepository;
import uz.gvs.repository.UserRepository;
import uz.gvs.repository.WarehouseRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TranferService {
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    TransferRepository transferRepository;
    @Autowired
    ProductService productService;

    public ApiResponse addTranser(TransferDto transferDto) {
        Transfer transfer = new Transfer();

//        String maxContractNumber = transferRepository.findMaxContractNumber();
//        Integer integer = Integer.valueOf(maxContractNumber);
//
//        if (integer == 0) {
//            transfer.setContractNumber("1");
//        }
//        transfer.setContractNumber(String.valueOf(integer + 1));
//        transfer.setContractTime(Timestamp.valueOf(LocalDateTime.now()));

        transfer.setContractNumber(transferDto.getContractNumber());
        transfer.setContractTime(Timestamp.valueOf(transferDto.getContractTime()));

        transfer.setFromWarehouse(warehouseRepository.findById(transferDto.getFromWarehouseId()).orElseThrow(() -> new ResourceNotFoundException("getFromWarehouseId")));
        transfer.setToWarehouse(warehouseRepository.findById(transferDto.getToWarehouseId()).orElseThrow(() -> new ResourceNotFoundException("getToWarehouseId")));
        transfer.setUser(userRepository.findById(transferDto.getUserId()).orElseThrow(() -> new UsernameNotFoundException("getUser")));
        transfer.setAllAmount(transferDto.getAllAmount());
        transfer.setAccepted(transferDto.isAccepted());

        List<Product> transferProductList = new ArrayList<>();
        //mahsulot sonini yetarli ekanligini tekshirish uchun
        for (ProductDto transferProductDto : transferDto.getTransferProductDto()) {
            Double allLeftOver = productRepository.getAllLeftOver(transferProductDto.getProductTemplateId());
            if (transferProductDto.getCount() > allLeftOver) {
                apiResponseService.unEnoughProduct();
            }
        }

        for (ProductDto productDto : transferDto.getTransferProductDto()) {
            List<Product> allProductLeftOver = productRepository.getAllProductLeftOver(productDto.getProductTemplateId(), transferDto.getFromWarehouseId());
            List<Product> productList = new ArrayList<>();
            double needAmount = productDto.getCount();
            for (Product product : allProductLeftOver) {
                if (needAmount == 0) {
                    break;
                }
                if (product.getLeftover() > needAmount) {
                    product.setLeftover(product.getLeftover() - needAmount);
                    needAmount = 0;
                } else if (product.getLeftover() == needAmount) {
                    product.setLeftover(0);
                    needAmount = 0;
                } else {
                    needAmount = needAmount - product.getLeftover();
                    product.setLeftover(0);
                }
                productList.add(product);
            }
            productRepository.saveAll(productList);
        }
        List<Product> list = new ArrayList<>();
        for (ProductDto dto : transferDto.getTransferProductDto()) {
            Product product = productService.addTransferProduct(dto, transferDto.getToWarehouseId());
            list.add(product);
        }
        transfer.setTransferProduct(list);

        transferRepository.save(transfer);
        return apiResponseService.savedResponse();
    }

    public TransferDto getTransfer(Transfer transfer) {
        return new TransferDto(
                transfer.getId(),
                transfer.getContractNumber(),
                transfer.getContractTime().toLocalDateTime(),
                transfer.getFromWarehouse(),
                transfer.getToWarehouse(),
                transfer.getUser(),
                transfer.getTransferProduct(),
                transfer.getAllAmount(),
                transfer.isAccepted());
    }


    public ResPageable getTransferList(int page, int size) {
        try {
            Page<Transfer> all = transferRepository.findAll(PageRequest.of(page, size));
            return new ResPageable(
                    all.getTotalPages(),
                    all.getTotalElements(),
                    all.getNumber(),
                    all.getSize(),
                    all.get().map(this::getTransferSearch).collect(Collectors.toList())
            );
        } catch (Exception e) {
            return null;
        }
    }

    public TransferDto getTransferSearch(Transfer transfer){
        return new TransferDto(
                transfer.getId(),
                transfer.getContractNumber(),
                transfer.getFromWarehouse(),
                transfer.getToWarehouse(),
                transfer.getUser(),
                transfer.getAllAmount(),
                transfer.isAccepted()
        );
    }

    public List<TransferDto> searchTransfer(String contractNumber){
        List<Transfer> allTransferSearch = transferRepository.getAllTransferSearch(contractNumber);

        List<TransferDto> tranferServiceList = new ArrayList<>();
        for (Transfer transfer : allTransferSearch) {
            tranferServiceList.add(getTransferSearch(transfer));
        }
        return tranferServiceList;
    }

}
