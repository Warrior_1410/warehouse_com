package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Product;
import uz.gvs.entity.ProductCategory;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.*;
import uz.gvs.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductTemplateService {
    @Autowired
    ProductTemplateRepository productTemplateRepository;
    @Autowired
    BrandRepository brandRepository;
    @Autowired
    ProductCategoryRepository productCategoryRepository;
    @Autowired
    MeasureRepository measureRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    AttachmentService attachmentService;
    @Autowired
    ProductRepository productRepository;

    //  CREATE
    public ApiResponse create(ProductTemplateDto productTemplateDto) {
        if (!productTemplateRepository.existsByNameEqualsIgnoreCase(productTemplateDto.getName())) {
            ProductTemplate productTemplate = new ProductTemplate();
            productTemplate.setKod(productTemplateDto.getKod());
            productTemplate.setName(productTemplateDto.getName());
            productTemplate.setBarcode(productTemplateDto.getBarcode());
            productTemplate.setMinCount(productTemplateDto.getMinCount());
            productTemplate.setActive(productTemplateDto.isActive());
            productTemplate.setMobile(productTemplateDto.isMobile());
            productTemplate.setBrand(brandRepository.findById(productTemplateDto.getBrandId()).orElseThrow(() -> new ResourceNotFoundException("getBrand")));
            productTemplate.setProductCategory(productCategoryRepository.findById(productTemplateDto.getProductCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getProCat")));
            productTemplate.setMeasure(measureRepository.findById(productTemplateDto.getMeasureId()).orElseThrow(() -> new ResourceNotFoundException("getMeasure")));
            productTemplate.setAttachment(attachmentRepository.findById(productTemplateDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
            productTemplateRepository.save(productTemplate);
            return apiResponseService.savedResponse();
        }
        return apiResponseService.existsResponse();
    }

    // GETONE
    public ProductTemplateDto getOne(ProductTemplate productTemplate) {
        return new ProductTemplateDto(
                productTemplate.getId(),
                productTemplate.getKod(),
                productTemplate.getName(),
                productTemplate.getBarcode(),
                productTemplate.getMinCount(),
                productTemplate.isActive(),
                productTemplate.isMobile(),
                productTemplate.getBrand(),
                productTemplate.getProductCategory(),
                productTemplate.getMeasure(),
                productTemplate.getAttachment().getId()
        );
    }

    //LIST
    public ResPageable getList(int page, int size) {
        try {
            PageRequest pageRequest = PageRequest.of(page, size);
            Page<ProductTemplate> templatePage = productTemplateRepository.findAll(pageRequest);
            List<ProductTemplate> contentPA = templatePage.getContent();
            List<ProductTemplateDto> productTemplateDtoList = new ArrayList<>();
            for (int i = 0; i < templatePage.getContent().size(); i++) {
                productTemplateDtoList.add(getOne(templatePage.getContent().get(i)));
            }
            return new ResPageable(
                    templatePage.getTotalPages(),
                    templatePage.getTotalElements(),
                    page,
                    size,
                    templatePage.getContent().stream().map(this::getOne));
        } catch (Exception e) {
            return null;
        }
    }

    //UPDATE
    public ApiResponse update(UUID id, ProductTemplateDto productTemplateDto) {
        Optional<ProductTemplate> optionalProductTemplate = productTemplateRepository.findById(id);
        if (optionalProductTemplate.isPresent()) {
            ProductTemplate productTemplate = optionalProductTemplate.get();
            if (!productTemplateRepository.existsByNameEqualsIgnoreCaseAndIdNot(productTemplateDto.getName(), id)) {
                productTemplate.setKod(productTemplateDto.getKod());
                productTemplate.setName(productTemplateDto.getName());
                productTemplate.setBarcode(productTemplateDto.getBarcode());
                productTemplate.setMinCount(productTemplateDto.getMinCount());
                productTemplate.setActive(productTemplateDto.isActive());
                productTemplate.setMobile(productTemplateDto.isMobile());

                if (productTemplateDto.getBrandId() != null) {
                    productTemplate.setBrand(brandRepository.findById(productTemplateDto.getBrandId()).orElseThrow(() -> new ResourceNotFoundException("getBrand")));
                }
                if (productTemplateDto.getProductCategoryId() != null) {
                    productTemplate.setProductCategory(productCategoryRepository.findById(productTemplateDto.getProductCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getProCat")));
                }
                if (productTemplateDto.getMeasureId() != null) {
                    productTemplate.setMeasure(measureRepository.findById(productTemplateDto.getMeasureId()).orElseThrow(() -> new ResourceNotFoundException("getMeasure")));
                }
                if (productTemplateDto.getAttachmentId() != null){
                    productTemplate.setAttachment(attachmentRepository.findById(productTemplateDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment")));
                }

                productTemplateRepository.save(productTemplate);
                return apiResponseService.savedResponse();
            }
            return apiResponseService.existsResponse();
        }
        return apiResponseService.notFoundResponse();
    }

    //DELETE
    public ApiResponse delete(UUID id) {
        try {
            productTemplateRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }

    public ApiResponse updateBoolean(UUID id, PatchLoad patchLoad){
        try {
            Optional<ProductTemplate> byId = productTemplateRepository.findById(id);
            if (byId.isPresent()){
                ProductTemplate productTemplate = byId.get();
                if (patchLoad.getRequest().equals("active")) {
                    productTemplate.setActive(!productTemplate.isActive());
                    productTemplateRepository.save(productTemplate);
                } else if (patchLoad.getRequest().equals("mobile")) {
                    productTemplate.setMobile(!productTemplate.isMobile());
                    productTemplateRepository.save(productTemplate);
                }
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.existsResponse();
        }catch (Exception e){
            return  apiResponseService.tryErrorResponse();
        }
    }

    //by Warehouse
    public List<ProductTemplateDto> getByWarehouse(Integer id){
        try{
            List<ProductTemplate> all = productTemplateRepository.findAll();

            List<ProductTemplateDto> productTemplateDtoList = new ArrayList<>();
            for (ProductTemplate productTemplate : all) {
                Double allByWarehouseId = productRepository.getAllByWarehouseId(id, productTemplate.getId());

                ProductTemplateDto productTemplateDto = new ProductTemplateDto(
                        productTemplate.getId(),
                        productTemplate.getKod(),
                        productTemplate.getName(),
                        productTemplate.getBrand(),
                        productTemplate.getProductCategory(),
                        productTemplate.getMeasure(),
                        allByWarehouseId
                );
                productTemplateDtoList.add(productTemplateDto);
            }
            return productTemplateDtoList;
        }catch (Exception e){
            return null;
        }
    }

    public ResPageable getAllByName(String name,int page,int size){
        try{
            List<ProductTemplate> all = productTemplateRepository.getAllProductTemplateSearch(name);
            Page<ProductTemplate> templatePage = productTemplateRepository.findAll(PageRequest.of(page,size));
            List<ProductTemplateDto> productTemplateDtoList = new ArrayList<>();
            for (ProductTemplate productTemplate : all) {
                Double allByWarehouseId = productRepository.getAllProductTemplate(productTemplate.getId());

                ProductTemplateDto productTemplateDto = new ProductTemplateDto(
                        productTemplate.getId(),
                        productTemplate.getKod(),
                        productTemplate.getName(),
                        productTemplate.getBrand(),
                        productTemplate.getProductCategory(),
                        productTemplate.getMeasure(),
                        allByWarehouseId
                );
                productTemplateDtoList.add(productTemplateDto);
            }
            return new ResPageable(
                    templatePage.getTotalPages(),
                    templatePage.getTotalElements(),
                    page,
                    size,
                    productTemplateDtoList);
        }catch (Exception e){
            return null;
        }
    }
    public ResPageable getAll(int page,int size){
        try{
            List<ProductTemplate> all = productTemplateRepository.findAll();
            Page<ProductTemplate> templatePage = productTemplateRepository.findAll(PageRequest.of(page,size));
            List<ProductTemplateDto> productTemplateDtoList = new ArrayList<>();
            for (ProductTemplate productTemplate : all) {
                Double allByWarehouseId = productRepository.getAllProductTemplate(productTemplate.getId());
                ProductTemplateDto productTemplateDto = new ProductTemplateDto(
                        productTemplate.getId(),
                        productTemplate.getKod(),
                        productTemplate.getName(),
                        productTemplate.getBrand(),
                        productTemplate.getProductCategory(),
                        productTemplate.getMeasure(),
                        allByWarehouseId
                );
                productTemplateDtoList.add(productTemplateDto);
            }
            return new ResPageable(
                    templatePage.getTotalPages(),
                    templatePage.getTotalElements(),
                    page,
                    size,
                    productTemplateDtoList);
        }catch (Exception e){
            return null;
        }
    }

    public List<ProductTemplate> getProductTemplateSearch(String name){
        try {
            return productTemplateRepository.getAllProductTemplateSearch(name);
        }catch (Exception e){
            return null;
        }
    }

}
