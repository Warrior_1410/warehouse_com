package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Kassa;
import uz.gvs.entity.MobilePayment;
import uz.gvs.entity.User;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.KassaDto;
import uz.gvs.payload.MobilePaymentDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.KassaRepository;
import uz.gvs.repository.MobilePaymentRepository;
import uz.gvs.repository.UserRepository;

import java.sql.Timestamp;
import java.util.*;

@Service
public class MobilePaymentService {
    @Autowired
    MobilePaymentRepository mobilePaymentRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    KassaRepository kassaRepository;

    public ApiResponse addMobilePayment(MobilePaymentDto mobilePaymentDto, User user) {
        try {
            Integer size = mobilePaymentRepository.findAll().size();
            String maxContractNumber = mobilePaymentRepository.findMaxContractNumber();
            Integer integer = Integer.valueOf(maxContractNumber);

            MobilePayment mobilePayment = new MobilePayment();
            if (integer == 0){
                mobilePayment.setContractNumber("1");
            }
            mobilePayment.setContractNumber(String.valueOf(integer + 1));

            mobilePayment.setContractTime((Timestamp) new Date());
            mobilePayment.setClient(
                    userRepository.findById(mobilePaymentDto.getClientId()).orElseThrow(() -> new ResourceNotFoundException("getUser"))
            );
            if (user.getRoles().size() > 4) {
                mobilePayment.setAgent(
                        userRepository.findById(mobilePaymentDto.getClientId()).orElseThrow(() -> new ResourceNotFoundException("getUser"))
                );
            }
            mobilePayment.setAgent(
                    userRepository.findById(user.getId()).orElseThrow(() -> new ResourceNotFoundException("getUser"))
            );
            List<Kassa> kassaList = new ArrayList<>();
            for (UUID kassaListItem : mobilePaymentDto.getKassaId()) {
                Kassa kassa = kassaRepository.findById(kassaListItem).orElseThrow(() -> new ResourceNotFoundException("getKassa"));
                kassaList.add(kassa);
            }
            mobilePayment.setKassa(kassaList);
            mobilePayment.setAccepted(mobilePaymentDto.isAccepted());
            mobilePayment.setAllPrice(mobilePayment.getAllPrice());

            return apiResponseService.savedResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getMobilePaymentList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<MobilePayment> allMobilePayment = mobilePaymentRepository.findAll(pageable);

            List<MobilePaymentDto> mobilePaymentDtoList = new ArrayList<>();
            for (int i = 0; i < allMobilePayment.getContent().size(); i++) {
                mobilePaymentDtoList.add(
                        getMobilePayment(allMobilePayment.getContent().get(i))
                );
            }
            return new ResPageable(
                    allMobilePayment.getTotalPages(),
                    allMobilePayment.getTotalElements(),
                    page,
                    size,
                    allMobilePayment.getContent().stream().map(this::getMobilePayment));
        } catch (Exception e) {
            return null;
        }
    }

    public MobilePaymentDto getMobilePayment(MobilePayment mobilePayment) {
        return new MobilePaymentDto(
            mobilePayment.getId(),
                mobilePayment.getContractNumber(),
                mobilePayment.getContractTime(),
                mobilePayment.getClient(),
                mobilePayment.getAgent(),
                mobilePayment.getKassa(),
                mobilePayment.isAccepted(),
                mobilePayment.getAllPrice()
        );
    }

    public ApiResponse editMobilePayment(UUID id, MobilePaymentDto mobilePaymentDto) {
        try {
            Optional<MobilePayment> optionalMobilePayment = mobilePaymentRepository.findById(id);
            if (optionalMobilePayment.isPresent()) {
                MobilePayment mobilePayment = optionalMobilePayment.get();

                mobilePayment.setAccepted(mobilePaymentDto.isAccepted());
                mobilePayment.setAgent(mobilePaymentDto.getAgent());
                mobilePayment.setClient(mobilePaymentDto.getClient());
                mobilePayment.setKassa(mobilePayment.getKassa());
                mobilePayment.setAllPrice(mobilePaymentDto.getAllPrice());
                mobilePaymentRepository.save(mobilePayment);

                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse deleteMobilePayment(UUID id) {
        try {
            mobilePaymentRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }
}
