package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.CashName;
import uz.gvs.entity.Currency;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.*;
import uz.gvs.repository.CashNameRepository;
import uz.gvs.repository.CurrencyNameRepository;
import uz.gvs.repository.CurrencyRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class
CurrencyService {
    @Autowired
    CurrencyRepository currencyRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    CurrencyNameRepository currencyNameRepository;

    public ApiResponse addCurrency(CurrencyDto currencyDto) {
        try {
            Currency currency = new Currency();
            currency.setAmount(currencyDto.getAmount());
            currency.setUnity(currencyDto.getUnity());
            currency.setCurrencyName(currencyNameRepository.findById(currencyDto.getCurrencyNameId()).orElseThrow(() -> new ResourceNotFoundException("getCashName")));
            currencyRepository.save(currency);
            return apiResponseService.savedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }

    }

    public ResPageable getCurrencyList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Currency> allCurrency = currencyRepository.findAll(pageable);

            List<CurrencyDto> currencyDtoList = new ArrayList<>();
            for (int i = 0; i < allCurrency.getContent().size(); i++) {
                currencyDtoList.add(
                        getCurrency(allCurrency.getContent().get(i))
                );
            }
            return new ResPageable(
                    allCurrency.getTotalPages(),
                    allCurrency.getTotalElements(),
                    page,
                    size,
                    allCurrency.getContent().stream().map(this::getCurrency));
        } catch (Exception e) {
            return null;
        }
    }

    public CurrencyDto getCurrency(Currency currency) {
        return new CurrencyDto(
                currency.getId(),
                currency.getAmount(),
                currency.getUnity(),
                currency.getCurrencyName()
        );
    }

    public ApiResponse editCurrency(UUID id, CurrencyDto currencyDto) {
        try {
            Optional<Currency> optionalCurrency = currencyRepository.findById(id);
            if (optionalCurrency.isPresent()) {
                Currency currency = optionalCurrency.get();
                currency.setAmount(currencyDto.getAmount());
                currency.setUnity(currencyDto.getUnity());
                currency.setCurrencyName(
                   currencyNameRepository.findById(currencyDto.getCurrencyNameId()).orElseThrow(() -> new ResourceNotFoundException("getCurrencyName"))
                );
                currencyRepository.save(currency);

                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse deleteCurrency(UUID id) {
        try {
            currencyRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

}
