package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.Measure;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.MeasureDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.MeasureRepository;
import uz.gvs.utils.MessageConst;

import java.util.*;

@Service
public class MeasureService {
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    MeasureRepository measureRepository;

    // CREATE MEASURE
    public ApiResponse addMeasure(MeasureDto measureDto) {
        if (!measureRepository.existsByNameEqualsIgnoreCase(measureDto.getName())) {
            Measure measure = new Measure();
            measure.setName(measureDto.getName());
            measure.setDescription(measureDto.getDescription());
            measure.setActive(measureDto.isActive());
            measureRepository.save(measure);
            return apiResponseService.savedResponse();
        }
        return apiResponseService.existsResponse();
    }

    // GetList MEASURE
    public ResPageable getMeasureList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Measure> measurePage = measureRepository.findAll(pageable);
            List<Measure> content = measurePage.getContent();
            List<MeasureDto> measureDtoList = new ArrayList<>();
            for (int i = 0; i < measurePage.getContent().size(); i++) {
                measureDtoList.add(getMeasure(measurePage.getContent().get(i)));
            }
            return new ResPageable(
                    measurePage.getTotalPages(),
                    measurePage.getTotalElements(),
                    page,
                    size,
                    measurePage.getContent().stream().map(this::getMeasure));
        } catch (Exception e) {
            return null;
        }
    }

    // GetOne   MEASURE
    public MeasureDto getMeasure(Measure measure) {
        return new MeasureDto(
                measure.getId(),
                measure.getName(),
                measure.getDescription(),
                measure.isActive());
    }


    // UPDATE MEASURE
    public ApiResponse editMeasure(Integer id, MeasureDto measureDto) {
        try {
            Optional<Measure> byId = measureRepository.findById(id);
            if (byId.isPresent()) {
                Measure measure = byId.get();
                if (!measureRepository.existsByNameEqualsIgnoreCaseAndIdNot(measureDto.getName(), id)) {
                    measure.setName(measureDto.getName());
                    measure.setDescription(measureDto.getDescription());
                    measure.setActive(measureDto.isActive());
                    measureRepository.save(measure);
                    return apiResponseService.updatedResponse();
                }
                return apiResponseService.existsResponse();

            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.errorResponse();
        }
    }
    public ApiResponse updateMeasure(Integer id){
        try {
            Optional<Measure> optionalMeasure = measureRepository.findById(id);
            if (optionalMeasure.isPresent()){
                Measure measure = optionalMeasure.get();
                measure.setActive(!measure.isActive());
                measureRepository.save(measure);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }
    // DELETE MEASURE
    public ApiResponse deleteMeasure(Integer id) {
        try {
            measureRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }

    public List<Measure> getMeasureSearch(String name){
        try {
            return measureRepository.getAllMeasureSearch(name);
        }catch (Exception e){
            return null;
        }
    }


}
