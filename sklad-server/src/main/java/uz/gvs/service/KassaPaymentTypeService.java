package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.KassaPaymentType;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.payload.KassaPaymentTypeDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.KassaPaymentTypeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class KassaPaymentTypeService {
    @Autowired
    KassaPaymentTypeRepository kassaPaymentTypeRepository;
    @Autowired
    ApiResponseService apiResponseService;

    public ApiResponse addKassaPaymentType(KassaPaymentTypeDto kassaPaymentTypeDto) {
        try {
            boolean existsByName = kassaPaymentTypeRepository.existsByNameEqualsIgnoreCase(kassaPaymentTypeDto.getName());
            if (!existsByName) {
                KassaPaymentType kassaPaymentType = new KassaPaymentType();
                kassaPaymentType.setName(kassaPaymentTypeDto.getName());
                kassaPaymentType.setDescription(kassaPaymentTypeDto.getDescription());
                kassaPaymentType.setActive(kassaPaymentTypeDto.isActive());
                kassaPaymentTypeRepository.save(kassaPaymentType);
                return apiResponseService.savedResponse();
            } else {
                return apiResponseService.existsResponse();
            }

        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getKassaPaymentTypeList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<KassaPaymentType> allkassaPaymentTypes = kassaPaymentTypeRepository.findAll(pageable);

            List<KassaPaymentTypeDto> kassaPaymentTypeDtoList = new ArrayList<>();
            for (int i = 0; i < allkassaPaymentTypes.getContent().size(); i++) {
                kassaPaymentTypeDtoList.add(
                        getKassaPaymentType(allkassaPaymentTypes.getContent().get(i))
                );
            }
            return new ResPageable(
                    allkassaPaymentTypes.getTotalPages(),
                    allkassaPaymentTypes.getTotalElements(),
                    page,
                    size,
                    allkassaPaymentTypes.getContent().stream().map(this::getKassaPaymentType));
        } catch (Exception e) {
            return null;
        }
    }

    public KassaPaymentTypeDto getKassaPaymentType(KassaPaymentType kassaPaymentType) {
        return new KassaPaymentTypeDto(
                kassaPaymentType.getId(),
                kassaPaymentType.getName(),
                kassaPaymentType.getDescription(),
                kassaPaymentType.isActive()
        );
    }

    public ApiResponse editKassaPaymentType(Integer id, KassaPaymentTypeDto kassaPaymentTypeDto) {
        try {
            Optional<KassaPaymentType> optionalKassaPaymentType = kassaPaymentTypeRepository.findById(id);
            if (optionalKassaPaymentType.isPresent()) {
                if (!kassaPaymentTypeRepository.existsByNameEqualsIgnoreCaseAndIdNot(kassaPaymentTypeDto.getName(), id)) {
                    KassaPaymentType kassaPaymentType = optionalKassaPaymentType.get();
                    kassaPaymentType.setName(kassaPaymentTypeDto.getName());
                    kassaPaymentType.setDescription(kassaPaymentTypeDto.getDescription());
                    kassaPaymentType.setActive(kassaPaymentTypeDto.isActive());
                    kassaPaymentTypeRepository.save(kassaPaymentType);
                    return apiResponseService.updatedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }
    public ApiResponse updateKassaPaymentType(Integer id){
        try {
            Optional<KassaPaymentType> optionalKassaPaymentType = kassaPaymentTypeRepository.findById(id);
            if (optionalKassaPaymentType.isPresent()){
                KassaPaymentType kassaPaymentType = optionalKassaPaymentType.get();
                kassaPaymentType.setActive(!kassaPaymentType.isActive());
                kassaPaymentTypeRepository.save(kassaPaymentType);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }
    public ApiResponse deleteKassaPaymentType(Integer id) {
        try {
            kassaPaymentTypeRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public List<KassaPaymentType> getKassaPaymentTypeSearch(String name){
        try {
            return kassaPaymentTypeRepository.getAllKassaPaymentTypeSearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
