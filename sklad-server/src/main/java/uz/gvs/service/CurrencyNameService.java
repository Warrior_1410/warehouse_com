package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.CurrencyName;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.payload.CurrencyNameDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.CurrencyNameRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CurrencyNameService {
    @Autowired
    CurrencyNameRepository currencyNameRepository;
    @Autowired
    ApiResponseService apiResponseService;
    public ApiResponse addCurrencyName(CurrencyNameDto currencyNameDto) {
        try {
            boolean existsByName = currencyNameRepository.existsByNameEqualsIgnoreCase(currencyNameDto.getName());
            if (!existsByName) {
                CurrencyName currencyName = new CurrencyName();
                currencyName.setName(currencyNameDto.getName());
                currencyName.setDescription(currencyNameDto.getDescription());
                currencyName.setActive(currencyNameDto.isActive());

                currencyNameRepository.save(currencyName);

                return apiResponseService.savedResponse();
            } else {
                return apiResponseService.existsResponse();
            }

        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getCurrencyNameList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<CurrencyName> allCurrencyNameList = currencyNameRepository.findAll(pageable);

            List<CurrencyNameDto> currencyNameDtoList = new ArrayList<>();
            for (int i = 0; i < allCurrencyNameList.getContent().size(); i++) {
                currencyNameDtoList.add(
                        getCurrencyName(allCurrencyNameList.getContent().get(i))
                );
            }
            return new ResPageable(
                    allCurrencyNameList.getTotalPages(),
                    allCurrencyNameList.getTotalElements(),
                    page,
                    size,
                    allCurrencyNameList.getContent().stream().map(this::getCurrencyName));
        } catch (Exception e) {
            return null;
        }
    }

    public CurrencyNameDto getCurrencyName(CurrencyName currencyName) {
        return new CurrencyNameDto(
                currencyName.getId(),
                currencyName.getName(),
                currencyName.getDescription(),
                currencyName.isActive()
        );
    }

    public ApiResponse editCurrencyName(Integer id, CurrencyNameDto currencyNameDto) {
        try {
            Optional<CurrencyName> optionalCurrencyName = currencyNameRepository.findById(id);
            if (optionalCurrencyName.isPresent()) {
                CurrencyName currencyName = optionalCurrencyName.get();
                if (!currencyNameRepository.existsByNameEqualsIgnoreCaseAndIdNot(currencyNameDto.getName(),id)){
                currencyName.setName(currencyNameDto.getName());
                currencyName.setDescription(currencyNameDto.getDescription());
                currencyName.setActive(currencyNameDto.isActive());
                currencyNameRepository.save(currencyName);
                return apiResponseService.updatedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }

    }
    public ApiResponse updateCurrencyName(Integer id){
        try
        {
            Optional<CurrencyName> optionalCurrencyName = currencyNameRepository.findById(id);
            if (optionalCurrencyName.isPresent()){
                CurrencyName currencyName = optionalCurrencyName.get();
                currencyName.setActive(!currencyName.isActive());
                currencyNameRepository.save(currencyName);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }
    public ApiResponse deleteCurrencyName(Integer id) {
        try {
            currencyNameRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public List<CurrencyName> getCurrencyNameSearch(String name){
        try {
            return currencyNameRepository.getAllCurrencyNameSearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
