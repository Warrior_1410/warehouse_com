package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.Warehouse;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.BankRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BankService {
    @Autowired
    BankRepository bankRepository;
    @Autowired
    ApiResponseService apiResponseService;

    public ApiResponse addBank(BankDto bankDto) {
        try {
            boolean existsByName = bankRepository.existsByNameEqualsIgnoreCase(bankDto.getName());
            if (!existsByName) {
                Bank bank = new Bank();
                bank.setName(bankDto.getName());
                bank.setDescription(bankDto.getDescription());
                bank.setActive(bankDto.isActive());
                bankRepository.save(bank);
                return apiResponseService.savedResponse();
            } else {
                return apiResponseService.existsResponse();
            }
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getBankList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Bank> allBanks = bankRepository.findAll(pageable);

            List<BankDto> bankDtoList = new ArrayList<>();
            for (int i = 0; i < allBanks.getContent().size(); i++) {
                bankDtoList.add(
                        getBank(allBanks.getContent().get(i))
                );
            }
            return new ResPageable(
                    allBanks.getTotalPages(),
                    allBanks.getTotalElements(),
                    page,
                    size,
                    allBanks.getContent().stream().map(this::getBank));
        } catch (Exception e) {
            return null;
        }
    }

    public BankDto getBank(Bank bank) {
        return new BankDto(
                bank.getId(),
                bank.getName(),
                bank.getDescription(),
                bank.isActive()
        );
    }

    public ApiResponse editBank(Integer id, BankDto bankDto) {
        try {
            Optional<Bank> optionalBank = bankRepository.findById(id);
            if (optionalBank.isPresent()) {
                Bank bank = optionalBank.get();
                if (!bankRepository.existsByNameEqualsIgnoreCaseAndIdNot(bankDto.getName(), id)) {
                    bank.setName(bankDto.getName());
                    bank.setDescription(bankDto.getDescription());
                    bank.setActive(bankDto.isActive());
                    bankRepository.save(bank);
                    return apiResponseService.updatedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse updateBank(Integer id){
        try{
            Optional<Bank> optionalBank = bankRepository.findById(id);
            if (optionalBank.isPresent()){
                Bank bank = optionalBank.get();
                bank.setActive(!bank.isActive());
                bankRepository.save(bank);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }

    public ApiResponse deleteBank(Integer id) {
        try {
            bankRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }

    public List<Bank> getBankSearch(String name){
        try {
            return bankRepository.getAllBankSearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
