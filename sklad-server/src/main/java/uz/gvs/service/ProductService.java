package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Product;
import uz.gvs.entity.ProductTemplate;
import uz.gvs.payload.ProductDto;
import uz.gvs.repository.ProductRepository;
import uz.gvs.repository.ProductTemplateRepository;
import uz.gvs.repository.WarehouseRepository;

import java.util.*;

@Service
public class ProductService {
    @Autowired
    ProductTemplateRepository productTemplateRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    WarehouseRepository warehouseRepository;

    public Product addProduct(ProductDto productDto){
        Product product =new Product();
        product.setProductTemplate(productTemplateRepository.findById(productDto.getProductTemplateId()).orElseThrow(() -> new ResourceNotFoundException("getPTP")));
        product.setCount(productDto.getCount());
        product.setLeftover(productDto.getCount());
        product.setPrice(productDto.getPrice());
        product.setRetailPrice(productDto.getRetailPrice());
        product.setWarehouse(warehouseRepository.findById(productDto.getWarehouseId()).orElseThrow(() -> new ResourceNotFoundException("get Wareouse")));
        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }
    public Product addTransferProduct(ProductDto productDto,Integer warehouseId){
        Product product =new Product();
        product.setProductTemplate(productTemplateRepository.findById(productDto.getProductTemplateId()).orElseThrow(() -> new ResourceNotFoundException("getPTP")));
        product.setCount(productDto.getCount());
        product.setLeftover(productDto.getCount());
        product.setPrice(productDto.getPrice());
        product.setRetailPrice(productDto.getPrice());
        product.setWarehouse(warehouseRepository.findById(warehouseId).orElseThrow(() -> new ResourceNotFoundException("get Wareouse")));
        Product savedProduct = productRepository.save(product);
        return savedProduct;
    }

    public Double getPrice(UUID id){
        Optional<Double> allByAllPrice = productRepository.getAllByAllPrice(id);
        if (allByAllPrice.isPresent()){
            return allByAllPrice.get();
        }
        return null;
    }

    public List<ProductDto> getAllPrice(){
        try {
            List<ProductTemplate> allPrice = productTemplateRepository.findAll();
            List<ProductDto> productDtoList = new ArrayList<>();

            for (ProductTemplate productTemplate : allPrice) {
                Optional<Double> getPrice = productRepository.getAllByAllPrice(productTemplate.getId());
                ProductDto productDto = new ProductDto();

                productDto.setProductTemplateId(productTemplate.getId());
                if (getPrice.isPresent()){
                    Double price = getPrice.get();
                    productDto.setPrice(price);
                }
                else{
                    productDto.setPrice(0);
                }
                productDtoList.add(productDto);
            }
            return productDtoList;
        }catch (Exception e){
            return null;
        }
    }


    public List<ProductDto> getAllCount(Integer id){
        try {
            List<ProductTemplate> allCount = productTemplateRepository.findAll();
            List<ProductDto> productDtoList = new ArrayList<>();

            for (ProductTemplate productTemplate : allCount) {
                Double count = productRepository.getAllByWarehouseId(id,productTemplate.getId());
                ProductDto productDto = new ProductDto(productTemplate.getId(),count);
                productDtoList.add(productDto);
            }
            return productDtoList;
        }catch (Exception e){
            return null;
        }
    }

}
