package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.Bank;
import uz.gvs.entity.CashName;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.BankDto;
import uz.gvs.payload.CashNameDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.BankRepository;
import uz.gvs.repository.CashNameRepository;
import uz.gvs.repository.CurrencyNameRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CashNameService {
    @Autowired
    CashNameRepository cashNameRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    CurrencyNameRepository currencyNameRepository;

    public ApiResponse addCashName(CashNameDto cashNameDto) {
        try {
            boolean existsByName = cashNameRepository.existsByNameEqualsIgnoreCase(cashNameDto.getName());
            if (!existsByName) {
                CashName cashName = new CashName();
                cashName.setName(cashNameDto.getName());
                cashName.setDescription(cashNameDto.getDescription());
                cashName.setActive(cashNameDto.isActive());
                cashName.setCurrencyName(currencyNameRepository.findById(cashNameDto.getCurrencyNameId()).orElseThrow(() -> new ResourceNotFoundException("getCashName")));
                cashNameRepository.save(cashName);
                return apiResponseService.savedResponse();
            } else {
                return apiResponseService.existsResponse();
            }
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public ResPageable getCashNameList(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<CashName> allCashNames = cashNameRepository.findAll(pageable);

            List<CashNameDto> cashNameDtoList = new ArrayList<>();
            for (int i = 0; i < allCashNames.getContent().size(); i++) {
                cashNameDtoList.add(
                        getCashName(allCashNames.getContent().get(i))
                );
            }
            return new ResPageable(
                    allCashNames.getTotalPages(),
                    allCashNames.getTotalElements(),
                    page,
                    size,
                    allCashNames.getContent().stream().map(this::getCashName));
        } catch (Exception e) {
            return null;
        }
    }

    public CashNameDto getCashName(CashName cashName) {
        return new CashNameDto(
                cashName.getId(),
                cashName.getName(),
                cashName.getDescription(),
                cashName.isActive(),
                cashName.getCurrencyName()
        );
    }

    public ApiResponse editCashName(Integer id, CashNameDto cashNameDto) {
        try {
            Optional<CashName> optionalCashName = cashNameRepository.findById(id);
            if (optionalCashName.isPresent()) {
                CashName cashName = optionalCashName.get();
                if (!cashNameRepository.existsByNameEqualsIgnoreCaseAndIdNot(cashNameDto.getName(),id)) {
                    cashName.setName(cashNameDto.getName());
                    cashName.setDescription(cashNameDto.getDescription());
                    cashName.setActive(cashNameDto.isActive());
                    cashName.setCurrencyName(currencyNameRepository.findById(cashNameDto.getCurrencyNameId()).orElseThrow(() -> new ResourceNotFoundException("getCurrencyName")));
                    cashNameRepository.save(cashName);
                    return apiResponseService.updatedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.notFoundResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }
    public ApiResponse updateCashName(Integer id){
        try {
            Optional<CashName> optionalCashName = cashNameRepository.findById(id);
            if (optionalCashName.isPresent()){
                CashName cashName = optionalCashName.get();
                cashName.setActive(!cashName.isActive());
                cashNameRepository.save(cashName);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }
    public ApiResponse deleteCashName(Integer id) {
        try {
            cashNameRepository.deleteById(id);
            return apiResponseService.deletedResponse();
        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }

    public List<CashName> getCashNameSearch(String name){
        try {
            return cashNameRepository.getAllCashNameSearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
