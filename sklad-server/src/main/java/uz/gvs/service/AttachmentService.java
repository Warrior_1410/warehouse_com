package uz.gvs.service;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.gvs.entity.*;
import uz.gvs.payload.ApiResponse;
import uz.gvs.repository.*;

import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    BrandRepository brandRepository;
    @Autowired
    ProductCategoryRepository productCategoryRepository;
    @Autowired
    ProductTemplateRepository productTemplateRepository;

    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getFile"));
        AttachmentContent byAttachmentId = attachmentContentRepository.findByAttachmentId(id);
        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachement; filename:\"" + attachment.getName() + "\"")
                .body(byAttachmentId.getContent());
    }

    public UUID uploadFile(MultipartHttpServletRequest request) {
        try {
            Iterator<String> fileNames = request.getFileNames();
            while (fileNames.hasNext()) {
                MultipartFile file = request.getFile(fileNames.next());
                assert file != null;
                Attachment attachment = new Attachment(file.getOriginalFilename(), file.getContentType(), file.getSize());
                Attachment savedAttachment = attachmentRepository.save(attachment);
                AttachmentContent attachmentContent = new AttachmentContent(savedAttachment, file.getBytes());
                attachmentContentRepository.save(attachmentContent);
                return savedAttachment.getId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public HttpEntity<?> getFilePDF(UUID id, boolean download) {
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id);
        if (download) {
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_PDF)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\" codelari.pdf")
                    .body(Base64.decodeBase64(attachmentContent.getContent()));
        } else {
            HttpHeaders header = new HttpHeaders();
            header.setContentType(MediaType.APPLICATION_PDF);
            return new ResponseEntity<>(Base64.decodeBase64(attachmentContent.getContent()), header, HttpStatus.OK);
        }
    }

    public String getAttachmentContent(UUID id) {
        try {
            AttachmentContent byAttachmentId = attachmentContentRepository.findByAttachmentId(id);
            return java.util.Base64.getEncoder().encodeToString(byAttachmentId.getContent());
        } catch (Exception e) {
            return "";
        }

    }

    public ApiResponse deleteAttachment(String name,UUID id){
        try{
            Optional<Attachment> byId = attachmentRepository.findById(id);
            if (byId.isPresent()){
                Attachment attachment = byId.get();
                if (name.equals("all")){
                    if (brandRepository.findByAttachment_Id(attachment.getId()).isPresent() ||
                            productCategoryRepository.findByAttachment_Id(attachment.getId()).isPresent() ||
                            productTemplateRepository.findByAttachment_Id(attachment.getId()).isPresent()){
                    }else {
                        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId());
                        attachmentContentRepository.deleteById(attachmentContent.getId());
                    }
                }
                else if (name.equals("brand")){
                    AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId());
                    attachmentContentRepository.deleteById(attachmentContent.getId());
                    Optional<Brand> optionalBrand = brandRepository.findByAttachment_Id(attachment.getId());
                    if (optionalBrand.isPresent()){
                        Brand brand  = optionalBrand.get();
                        brand.setAttachment(null);
                        brandRepository.save(brand);
                    }
                }else if (name.equals("category")){
                    AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId());
                    attachmentContentRepository.deleteById(attachmentContent.getId());
                    Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findByAttachment_Id(attachment.getId());
                    if (optionalProductCategory.isPresent()){
                        ProductCategory productCategory  = optionalProductCategory.get();
                        productCategory.setAttachment(null);
                        productCategoryRepository.save(productCategory);
                    }
                } else if (name.equals("template")){
                    AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachment.getId());
                    attachmentContentRepository.deleteById(attachmentContent.getId());
                    Optional<ProductTemplate> optionalProductTemplate = productTemplateRepository.findByAttachment_Id(attachment.getId());
                    if (optionalProductTemplate.isPresent()){
                        ProductTemplate productTemplate = optionalProductTemplate.get();
                        productTemplate.setAttachment(null);
                        productTemplateRepository.save(productTemplate);
                    }
                }
                attachmentRepository.deleteById(attachment.getId());
                return apiResponseService.deletedResponse();
            }
            else
            {
                return apiResponseService.notFoundResponse();
            }
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }
    }


}
