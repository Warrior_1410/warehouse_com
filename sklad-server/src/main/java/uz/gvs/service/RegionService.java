package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.ProductCategory;
import uz.gvs.entity.Region;
import uz.gvs.payload.ApiResponse;
import uz.gvs.payload.RegionDto;
import uz.gvs.payload.ResPageable;
import uz.gvs.repository.RegionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RegionService {
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    ApiResponseService apiResponseService;

    /**
     * CREATE REGION
     * Req RegionDto
     *
     * @return ApiResponse
     */
    public ApiResponse create(RegionDto regionDto) {
        if (regionDto.getRegionId() != null) {
            if (regionRepository.existsByNameEqualsIgnoreCaseAndRegion_Id(regionDto.getName(), regionDto.getRegionId())) {
                return apiResponseService.existsResponse();
            }
//            else if (regionRepository.existsByNameEqualsIgnoreCaseAndRegion_IdIsNull(regionDto.getName()))
//                return apiResponseService.existsResponse();

            Region region = new Region();
            region.setName(regionDto.getName());
            region.setDescription(regionDto.getDescription());
            region.setActive(regionDto.isActive());
            region.setRegion(regionRepository.findById(regionDto.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("getRegion")));
            regionRepository.save(region);
            return apiResponseService.savedResponse();
        } else if (regionDto.getRegionId() == null) {
            Optional<Region> region1 = regionRepository.existRegion(regionDto.getName());
            if (region1.isPresent()) {
                return apiResponseService.existsResponse();
            } else {
                Region region = new Region();
                region.setName(regionDto.getName());
                region.setDescription(regionDto.getDescription());
                region.setActive(regionDto.isActive());
                regionRepository.save(region);
                return apiResponseService.savedResponse();
            }
        }
        return apiResponseService.notFoundResponse();
    }

    /**
     * GETONE REGION
     * RES_Region
     * ApiResponse
     */
    public RegionDto getRegion(Region region) {
        return new RegionDto(
                region.getId(), region.getName(), region.getDescription(), region.isActive(), region.getRegion()
        );

    }

    /**
     * GETLIST
     * RES_REGION
     */
    public ResPageable getRegionList(int page, int size) {
        try {
            PageRequest pageable = PageRequest.of(page, size);
            Page<Region> regionPage = regionRepository.findAll(pageable);
            List<Region> content = regionPage.getContent();
            List<RegionDto> regionDtoList = new ArrayList<>();
            for (int i = 0; i < regionPage.getContent().size(); i++) {
                regionDtoList.add(getRegion(regionPage.getContent().get(i)));
            }
            return new ResPageable(
                    regionPage.getTotalPages(),
                    regionPage.getTotalElements(),
                    page,
                    size,
                    regionPage.getContent().stream().map(this::getRegion));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * UPDATE
     * REQ_REGION REGIONDTO
     *
     * @return ApiResponse
     */
    public ApiResponse edit(Integer id, RegionDto regionDto) {
        try {
            Optional<Region> optionalRegion = regionRepository.findById(id);
            if (optionalRegion.isPresent()) {
                if (!regionRepository.existsByNameEqualsIgnoreCaseAndIdNotAndRegion_Id(regionDto.getName(), id, regionDto.getRegionId())) {
                    Region region = optionalRegion.get();
                    region.setName(regionDto.getName());
                    region.setDescription(regionDto.getDescription());
                    region.setActive(regionDto.isActive());
                    if (region.getRegion() == null && regionDto.getRegionId() != null) {
                        region.setRegion(regionRepository.findById(regionDto.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("getRegionId")));
                    }

                    regionRepository.save(region);
                    return apiResponseService.savedResponse();
                }
                return apiResponseService.existsResponse();
            }
            return apiResponseService.existsResponse();
        } catch (Exception e) {
            return apiResponseService.notFoundResponse();
        }
    }
    public ApiResponse update(Integer id){
        try{
            Optional<Region> optionalRegion = regionRepository.findById(id);
            if (optionalRegion.isPresent()){
                Region region = optionalRegion.get();
                region.setActive(!region.isActive());
                regionRepository.save(region);
                return apiResponseService.updatedResponse();
            }
            return apiResponseService.notFoundResponse();
        }catch (Exception e){
            return apiResponseService.tryErrorResponse();
        }

    }
    public ApiResponse delete(Integer id) {
        try {
            regionRepository.deleteById(id);
            return apiResponseService.deletedResponse();

        } catch (Exception e) {
            return apiResponseService.tryErrorResponse();
        }
    }


    public List<Region> getRegionSearch(String name){
        try {
            return regionRepository.getAllRegionSearch(name);
        }catch (Exception e){
            return null;
        }
    }
}
