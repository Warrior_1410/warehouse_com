package uz.gvs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.gvs.entity.OrderProduct;
import uz.gvs.entity.Product;
import uz.gvs.payload.OrderProductDto;
import uz.gvs.repository.OrderProductRepository;
import uz.gvs.repository.ProductRepository;

import java.util.List;

@Service
public class OrderProductService {
    @Autowired
    ApiResponseService apiResponseService;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    OrderProductRepository orderProductRepository;

    public OrderProduct addOrderProduct(OrderProductDto orderProductDto, List<Product> productList) {
        OrderProduct orderProduct = new OrderProduct();
        orderProduct.setProduct(productList);
        orderProduct.setCount(orderProductDto.getCount());
        orderProduct.setRetailPrice(orderProductDto.getRetailPrice());
        orderProduct.setAllPrice(orderProductDto.getCount() * orderProductDto.getRetailPrice());
        return orderProductRepository.save(orderProduct);
    }
}
